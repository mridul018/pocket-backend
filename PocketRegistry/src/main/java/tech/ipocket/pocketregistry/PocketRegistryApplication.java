package tech.ipocket.pocketregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class PocketRegistryApplication extends SpringBootServletInitializer {

    /**
     *
     * @apiNote used when run as jar
     */
    public static void main(String[] args) {
        SpringApplication.run(PocketRegistryApplication.class, args);
    }

    /**
     *
     * @apiNote used when run as war
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PocketRegistryApplication.class);
    }
}
