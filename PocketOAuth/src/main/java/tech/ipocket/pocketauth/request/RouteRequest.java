package tech.ipocket.pocketauth.request;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RouteRequest {

    private Integer id;

    @NotNull(message = "Route id cannot be null")
    @Size(max = 4, message = "Route id too large, max 4")
    private String routeId;

    @NotNull(message = "Route path cannot be null")
    private String routePath;

    @Nullable
    private String routeServiceId;

    @NotNull(message = "Route url cannot be null")
    private String routeUrl;

    @NotNull(message = "Route context path cannot be null")
    private String routeContextPath;

}
