package tech.ipocket.pocketauth.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MappingRequest {
    private Integer id;

    @NotNull(message = "Client id cannot be null")
    @Size(max = 4, message = "Client id too long, max length 4")
    private String clientId;

    @NotNull(message = "Route id cannot be null")
    @Size(max = 4, message = "Route id too long, max length 4")
    private String routeId;

    @NotNull(message = "Status cannot be null, it must ne true or false")
    private Boolean status;

    @NotNull(message = "Auth cannot be null, it must be true or false")
    private Boolean authRequire;
}
