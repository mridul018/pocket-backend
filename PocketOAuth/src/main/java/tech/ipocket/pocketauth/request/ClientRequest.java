package tech.ipocket.pocketauth.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientRequest {

    private Integer id;
    @NotNull(message = "Client id cannot be null")
    @Size(max = 4, message = "Client id too large, max length 4")
    private String clientId;
    @NotNull(message = "Client name cannot be null")
    private String clientName;
    @NotNull(message = "Client password cannot be null")
    private String clientPassword;
    @NotNull(message = "Client validity cannot be null")
    private Boolean clientValid;


}
