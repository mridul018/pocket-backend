package tech.ipocket.pocketauth.request;


import tech.ipocket.pocketauth.response.BaseResponseObject;

public abstract class BaseReqestObject {

    private String requestId;
    private String hardwareSignature;
    private String sessionToken;
    private String deviceName;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getHardwareSignature() {
        return hardwareSignature;
    }

    public void setHardwareSignature(String hardwareSignature) {
        this.hardwareSignature = hardwareSignature;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public abstract void validate(BaseResponseObject baseResponseObject);

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
