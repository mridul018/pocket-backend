package tech.ipocket.pocketauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketauth.entity.ClientRouteMap;

@Repository
public interface ClientRouteMapRepository extends JpaRepository<ClientRouteMap, Integer> {
}
