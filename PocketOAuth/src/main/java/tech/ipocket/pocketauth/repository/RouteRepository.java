package tech.ipocket.pocketauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketauth.entity.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Integer> {
}
