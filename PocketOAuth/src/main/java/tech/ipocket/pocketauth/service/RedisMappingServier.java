package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.model.RedisMapping;

public interface RedisMappingServier {

    void save(RedisMapping redisClientRouteMapping);
    void update(RedisMapping redisClientRouteMapping);
    void delete(Integer id);
}
