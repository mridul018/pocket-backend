package tech.ipocket.pocketauth.service;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.model.RedisClient;

@Service
public class RedisClientServiceImpl implements RedisClientService {

    private static final String KEY="CLIENT";
    private RedisTemplate<String, RedisClient> redisTemplate;
    private HashOperations hashOperations;

    public RedisClientServiceImpl(RedisTemplate<String, RedisClient> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @Override
    public void save(RedisClient client) {
        try {
            this.redisTemplate.boundHashOps(KEY).put(client.getClientId(), client);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update(RedisClient redisClient) {
        save(redisClient);
    }

    @Override
    public void delete(String clientId) {
        try {
            this.redisTemplate.boundHashOps(KEY).delete(clientId);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
