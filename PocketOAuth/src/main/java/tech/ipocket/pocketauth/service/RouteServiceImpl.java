package tech.ipocket.pocketauth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.entity.Route;
import tech.ipocket.pocketauth.model.RedisRoute;
import tech.ipocket.pocketauth.repository.RouteRepository;
import tech.ipocket.pocketauth.request.RouteRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.response.ErrorObject;

import java.util.List;
import java.util.Optional;

@Service
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private RedisRouteService redisRouteService;

    @Override
    public BaseResponseObject save(RouteRequest request) {

        BaseResponseObject responseObject = new BaseResponseObject();

        Route route = new Route();
        route.setRouteContextPath(request.getRouteContextPath());
        route.setRouteId(request.getRouteId());
        route.setRoutePath(request.getRoutePath());
        route.setRouteServiceId(request.getRouteServiceId());
        route.setRouteUrl(request.getRouteUrl());

        try {
            Route newRoute = routeRepository.save(route);

            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(newRoute);

            RedisRoute redisRoute = new RedisRoute();

            redisRoute.setRequestId(request.getRouteId());
            redisRoute.setContextPath(request.getRouteContextPath());
            redisRoute.setPath(request.getRoutePath());
            redisRoute.setServiceId(request.getRouteServiceId());
            redisRoute.setUrl(request.getRouteUrl());


            redisRouteService.save(redisRoute);

        }catch (Exception ex){
            responseObject.setError(new ErrorObject("Route cannot save",HttpStatus.CONFLICT.value()));
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(null);
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject update(RouteRequest request, int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        Route route = new Route();
        route.setRouteContextPath(request.getRouteContextPath());
        route.setRouteId(request.getRouteId());
        route.setRoutePath(request.getRoutePath());
        route.setRouteServiceId(request.getRouteServiceId());
        route.setRouteUrl(request.getRouteUrl());
        route.setId(id);

        try {
            Route updateRoute = routeRepository.save(route);

            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(updateRoute);


            RedisRoute redisRoute = new RedisRoute();

            redisRoute.setRequestId(request.getRouteId());
            redisRoute.setContextPath(request.getRouteContextPath());
            redisRoute.setPath(request.getRoutePath());
            redisRoute.setServiceId(request.getRouteServiceId());
            redisRoute.setUrl(request.getRouteUrl());


            redisRouteService.update(redisRoute);

        }catch (Exception ex){

            responseObject.setError(new ErrorObject("Route cannot update",HttpStatus.CONFLICT.value()));
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(null);
        }


        return responseObject;
    }

    @Override
    public BaseResponseObject findAll() {
        BaseResponseObject responseObject = new BaseResponseObject();

        try {
            List<Route> all = routeRepository.findAll();
            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(all);
        }catch (Exception ex){
            responseObject.setError(new ErrorObject("List not found",HttpStatus.CONFLICT.value()));
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(null);
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject findById(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();


        try {
            Optional<Route> byId = routeRepository.findById(id);

            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(byId.orElse(null));
        }catch (Exception ex){

            responseObject.setError(new ErrorObject("Route not found",HttpStatus.NOT_FOUND.value()));
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(null);
        }


        return responseObject;
    }

    @Override
    public BaseResponseObject delete(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();


        Optional<Route> byId = routeRepository.findById(id);

        if(!byId.isPresent()){
            responseObject.setError(new ErrorObject("Route cannot found",
                    HttpStatus.NOT_FOUND.value()));
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData(null);
        }else{
            Route route = byId.get();
            routeRepository.delete(route);

            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setData("Delete done");

            redisRouteService.delete(route.getRouteId());

        }

        return responseObject;
    }
}
