package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.model.RedisClient;

public interface RedisClientService {

    void save(RedisClient client);
    void update(RedisClient redisClient);
    void delete(String clientId);
}
