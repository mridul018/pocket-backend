package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.request.RouteRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;

public interface RouteService {

    BaseResponseObject save(RouteRequest request);

    BaseResponseObject update(RouteRequest request, int id);

    BaseResponseObject findAll();

    BaseResponseObject findById(int id);

    BaseResponseObject delete(int id);
}
