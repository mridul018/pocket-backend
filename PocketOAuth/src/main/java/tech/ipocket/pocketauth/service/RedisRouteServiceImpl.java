package tech.ipocket.pocketauth.service;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.model.RedisRoute;

@Service
public class RedisRouteServiceImpl implements RedisRouteService {

    private static final String KEY="ROUTE";
    private RedisTemplate<String, RedisRoute> redisTemplate;
    private HashOperations hashOperations;


    public RedisRouteServiceImpl(RedisTemplate<String, RedisRoute> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @Override
    public void save(RedisRoute redisRoute) {
        try{
            this.redisTemplate.boundHashOps(KEY).put(redisRoute.getRequestId(),redisRoute);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }

    @Override
    public void update(RedisRoute redisRoute) {
        save(redisRoute);
    }

    @Override
    public void delete(String requestId) {
        try {
            this.redisTemplate.boundHashOps(KEY).delete(requestId);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }
}
