package tech.ipocket.pocketauth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.entity.ClientRouteMap;
import tech.ipocket.pocketauth.model.RedisMapping;
import tech.ipocket.pocketauth.repository.ClientRouteMapRepository;
import tech.ipocket.pocketauth.request.MappingRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.response.ErrorObject;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class MappingServiceImpl implements MappingService {

    @Autowired
    private ClientRouteMapRepository mapRepository;

    @Autowired
    private RedisMappingServier redisMappingServier;

    @Override
    public BaseResponseObject save(MappingRequest request) {
        BaseResponseObject responseObject = new BaseResponseObject();

        ClientRouteMap map = new ClientRouteMap();
        map.setAuthRequire(request.getAuthRequire());
        map.setClientId(request.getClientId());
        map.setCreateAt(new Timestamp(System.currentTimeMillis()));
        map.setRouteId(request.getRouteId());
        map.setStatus(request.getStatus());

        try {
            ClientRouteMap newMap = mapRepository.save(map);

            responseObject.setData(newMap);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setError(null);

            RedisMapping redisMapping = new RedisMapping();
            redisMapping.setClientId(request.getClientId());
            redisMapping.setId(newMap.getId());
            redisMapping.setRouteId(request.getRouteId());

            redisMappingServier.save(redisMapping);

        }catch (Exception ex){

            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
            responseObject.setError(new ErrorObject(
                    "Mapping cannot save",
                    HttpStatus.BAD_REQUEST.value()
            ));
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject update(MappingRequest request, int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        ClientRouteMap map = new ClientRouteMap();
        map.setAuthRequire(request.getAuthRequire());
        map.setClientId(request.getClientId());
        map.setCreateAt(new Timestamp(System.currentTimeMillis()));
        map.setRouteId(request.getRouteId());
        map.setStatus(request.getStatus());
        map.setId(id);

        try {
            ClientRouteMap newMap = mapRepository.save(map);

            responseObject.setData(newMap);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setError(null);

            RedisMapping redisMapping = new RedisMapping();
            redisMapping.setClientId(request.getClientId());
            redisMapping.setId(newMap.getId());
            redisMapping.setRouteId(request.getRouteId());

            redisMappingServier.update(redisMapping);

        }catch (Exception ex){

            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
            responseObject.setError(new ErrorObject(
                    "Mapping cannot update",
                    HttpStatus.BAD_REQUEST.value()
            ));
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject all() {
        BaseResponseObject responseObject = new BaseResponseObject();

        try {
            List<ClientRouteMap> all = mapRepository.findAll();

            responseObject.setData(all);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setError(null);
        }catch (Exception ex){

            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
            responseObject.setError(new ErrorObject(
                    "No mapping found",
                    HttpStatus.BAD_REQUEST.value()
            ));
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject findById(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        try {
            Optional<ClientRouteMap> byId = mapRepository.findById(id);
            if(byId.isPresent()){
                responseObject.setData(byId.get());
                responseObject.setStatus(HttpStatus.OK.value());
                responseObject.setError(null);
            }else{
                responseObject.setData(null);
                responseObject.setStatus(HttpStatus.NOT_FOUND.value());
                responseObject.setError(new ErrorObject(
                        "Not mapping found",
                        HttpStatus.NOT_FOUND.value()
                ));
            }

        }catch (Exception ex){

            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
            responseObject.setError(new ErrorObject(
                    "Unexpected occur",
                    HttpStatus.BAD_REQUEST.value()
            ));
        }

        return responseObject;
    }

    @Override
    public BaseResponseObject delete(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        try {
            Optional<ClientRouteMap> byId = mapRepository.findById(id);
            if(byId.isPresent()) {
                mapRepository.delete(byId.get());
                responseObject.setData("Mapping deleted");
                responseObject.setStatus(HttpStatus.OK.value());
                responseObject.setError(null);

                redisMappingServier.delete(id);

            } else{
                responseObject.setData(null);
                responseObject.setStatus(HttpStatus.NOT_FOUND.value());
                responseObject.setError(new ErrorObject(
                        "Not mapping found",
                        HttpStatus.NOT_FOUND.value()
                ));
            }

        }catch (Exception ex){

            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
            responseObject.setError(new ErrorObject(
                    "Unexpected occur",
                    HttpStatus.BAD_REQUEST.value()
            ));
        }

        return responseObject;
    }
}
