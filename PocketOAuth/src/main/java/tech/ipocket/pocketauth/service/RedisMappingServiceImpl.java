package tech.ipocket.pocketauth.service;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.model.RedisMapping;

@Service
public class RedisMappingServiceImpl implements RedisMappingServier {

    private static final String KEY="MAPPING";
    private RedisTemplate<String, RedisMapping> redisTemplate;
    private HashOperations hashOperations;

    public RedisMappingServiceImpl(RedisTemplate<String, RedisMapping> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @Override
    public void save(RedisMapping redisClientRouteMapping) {
        try {
            this.redisTemplate.boundHashOps(KEY).put(redisClientRouteMapping.getId(), redisClientRouteMapping);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update(RedisMapping redisClientRouteMapping) {
        save(redisClientRouteMapping);
    }

    @Override
    public void delete(Integer id) {
        try {
            this.redisTemplate.boundHashOps(KEY).delete(id);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
