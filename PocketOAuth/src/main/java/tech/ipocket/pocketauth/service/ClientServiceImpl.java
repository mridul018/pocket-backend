package tech.ipocket.pocketauth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.entity.Client;
import tech.ipocket.pocketauth.model.RedisClient;
import tech.ipocket.pocketauth.repository.ClientRepository;
import tech.ipocket.pocketauth.request.ClientRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.response.ErrorObject;

import java.sql.Timestamp;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private RedisClientService redisClientService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public BaseResponseObject save(ClientRequest request) {
        BaseResponseObject responseObject = new BaseResponseObject();

        Client client = new Client();
        client.setClientId(request.getClientId());
        client.setClientName(request.getClientName());
        client.setClientPassword(passwordEncoder.encode(request.getClientPassword()));
        client.setClientValid(request.getClientValid());
        client.setCreateAt(new Timestamp(System.currentTimeMillis()));

        try {
            Client newClient = clientRepository.save(client);

            responseObject.setData(newClient);
            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());

            RedisClient redisClient = new RedisClient();
            redisClient.setClientId(request.getClientId());
            redisClient.setClientName(request.getClientName());
            redisClient.setClientPassword(request.getClientPassword());

            redisClientService.save(redisClient);
        }catch (Exception ex){
            responseObject.setData(null);
            responseObject.setError(new ErrorObject(
                    "Client not save",
                    HttpStatus.BAD_REQUEST.value()
            ));
            responseObject.setStatus(HttpStatus.OK.value());
        }


        return responseObject;
    }

    @Override
    public BaseResponseObject update(ClientRequest request, int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        Client client = new Client();
        client.setClientId(request.getClientId());
        client.setClientName(request.getClientName());
        client.setClientPassword(passwordEncoder.encode(request.getClientPassword()));
        client.setClientValid(request.getClientValid());
        client.setCreateAt(new Timestamp(System.currentTimeMillis()));
        client.setId(id);

        try {
            Client updatedClient = clientRepository.save(client);

            responseObject.setData(updatedClient);
            responseObject.setError(null);
            responseObject.setStatus(HttpStatus.OK.value());

            RedisClient redisClient = new RedisClient();
            redisClient.setClientId(request.getClientId());
            redisClient.setClientName(request.getClientName());
            redisClient.setClientPassword(request.getClientPassword());

            redisClientService.update(redisClient);

        }catch (Exception ex){
            responseObject.setData(null);
            responseObject.setError(new ErrorObject(
                    "Client not update",
                    HttpStatus.BAD_REQUEST.value()
            ));
            responseObject.setStatus(HttpStatus.OK.value());
        }



        return responseObject;
    }

    @Override
    public BaseResponseObject findAll() {
        BaseResponseObject responseObject = new BaseResponseObject();

        List<Client> all = clientRepository.findAll();

        responseObject.setData(all);
        responseObject.setStatus(HttpStatus.OK.value());
        responseObject.setError(null);

        return responseObject;
    }

    @Override
    public BaseResponseObject findById(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();
        Client client = clientRepository.findById(id);

        responseObject.setData(client);
        responseObject.setError(null);
        responseObject.setStatus(HttpStatus.OK.value());

        return responseObject;
    }

    @Override
    public BaseResponseObject delete(int id) {
        BaseResponseObject responseObject = new BaseResponseObject();

        try {
            Client client = clientRepository.findById(id);

            if(client == null){
                responseObject.setData(null);
                responseObject.setStatus(HttpStatus.BAD_REQUEST.value());
                responseObject.setError(new ErrorObject(
                        "No client of Id : "+id,
                        HttpStatus.NOT_FOUND.value()
                ));
            }else{
                client.setClientValid(false);
                clientRepository.delete(client);

                responseObject.setData("Delete done");
                responseObject.setStatus(HttpStatus.OK.value());
                responseObject.setError(null);

                redisClientService.delete(client.getClientId());
            }


        }catch (Exception ex){
            responseObject.setData(null);
            responseObject.setStatus(HttpStatus.OK.value());
            responseObject.setError(null);
        }


        return responseObject;
    }
}
