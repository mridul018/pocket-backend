package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.request.MappingRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;

public interface MappingService {

    BaseResponseObject save(MappingRequest request);

    BaseResponseObject update(MappingRequest request, int id);

    BaseResponseObject all();

    BaseResponseObject findById(int id);

    BaseResponseObject delete(int id);
}
