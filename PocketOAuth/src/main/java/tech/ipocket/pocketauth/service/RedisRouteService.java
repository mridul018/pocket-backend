package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.model.RedisRoute;

public interface RedisRouteService {

    void save(RedisRoute redisRoute);
    void update(RedisRoute redisRoute);
    void delete(String id);
}
