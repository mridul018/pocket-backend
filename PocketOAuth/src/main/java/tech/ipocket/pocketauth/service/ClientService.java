package tech.ipocket.pocketauth.service;

import tech.ipocket.pocketauth.request.ClientRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;

public interface ClientService {

    BaseResponseObject save(ClientRequest request);

    BaseResponseObject update(ClientRequest request, int id);

    BaseResponseObject findAll();

    BaseResponseObject findById(int id);

    BaseResponseObject delete(int id);
}
