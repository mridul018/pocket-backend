package tech.ipocket.pocketauth.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tech.ipocket.pocketauth.entity.Client;
import tech.ipocket.pocketauth.repository.ClientRepository;

import java.util.Collections;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ClientRepository clientRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Client> byClientName = clientRepository.findByClientName(username);
        if(byClientName.isPresent()){
            return byClientName.map(client -> {
                return new User(client.getClientName(), client.getClientPassword(),
                        client.getClientValid(),
                        client.getClientValid(),
                        client.getClientValid(),
                        client.getClientValid(), Collections.emptyList());
            }).get();
        }
        return (UserDetails) new UsernameNotFoundException("Client not found");
    }
}
