package tech.ipocket.pocketauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocketauth.request.MappingRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.service.MappingService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value = "/")
public class MappingController {

    @Autowired
    private MappingService mappingService;

    @PostMapping(value = "/addNewMapping")
    public ResponseEntity<BaseResponseObject> addNewMapping(@Valid @RequestBody MappingRequest request){
        return new ResponseEntity<>(mappingService.save(request), HttpStatus.OK);
    }

    @PostMapping(value = "/updateMapping/{id}")
    public ResponseEntity<BaseResponseObject> addNewMapping(@Valid @RequestBody MappingRequest request,
                                                            @Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(mappingService.update(request, id), HttpStatus.OK);
    }

    @GetMapping(value = "/findAll")
    public ResponseEntity<BaseResponseObject> findAll(){
        return new ResponseEntity<>(mappingService.all(), HttpStatus.OK);
    }

    @GetMapping(value = "/findBy/{id}")
    public ResponseEntity<BaseResponseObject> findAll(@Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(mappingService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteBy/{id}")
    public ResponseEntity<BaseResponseObject> deleteBy(@Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(mappingService.delete(id), HttpStatus.OK);
    }


}
