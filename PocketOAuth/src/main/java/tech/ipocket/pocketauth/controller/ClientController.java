package tech.ipocket.pocketauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocketauth.request.ClientRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.service.ClientService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value = "/")
public class ClientController {

    @Autowired
    private ClientService clientService;


    @PostMapping(value = "/createClient")
    public ResponseEntity<BaseResponseObject> createClient(@RequestBody @Valid ClientRequest request){

        return new ResponseEntity<>(clientService.save(request), HttpStatus.OK);

    }

    @PutMapping(value = "/updateClient/{id}")
    public ResponseEntity<BaseResponseObject> updateClient(@RequestBody @Valid ClientRequest request,
                                                           @PathParam(value = "id") @Valid int id){

        return new ResponseEntity<>(clientService.update(request, id), HttpStatus.OK);
    }

    @GetMapping(value = "/listOfClient")
    public ResponseEntity<BaseResponseObject> allClient(){

        return new ResponseEntity<>(clientService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/findClientById/{id}")
    public ResponseEntity<BaseResponseObject> findById(@Valid @PathParam(value = "id") int id){

        return new ResponseEntity<>(clientService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteClientById/{id}")
    public ResponseEntity<BaseResponseObject> deleteClientById(@Valid @PathParam(value = "id") int id){

        return new ResponseEntity<>(clientService.delete(id), HttpStatus.OK);
    }


}
