package tech.ipocket.pocketauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocketauth.request.RouteRequest;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.service.RouteService;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value = "/")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @PostMapping(value = "/addRoute")
    public ResponseEntity<BaseResponseObject> addRoute(@Valid @RequestBody RouteRequest request){
        return new ResponseEntity<>(routeService.save(request), HttpStatus.OK);
    }

    @PutMapping(value = "/updateRoute/{id}")
    public ResponseEntity<BaseResponseObject> updateRoute(@Valid @RequestBody RouteRequest request, @Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(routeService.update(request, id), HttpStatus.OK);
    }

    @GetMapping(value = "/allRoute")
    public ResponseEntity<BaseResponseObject> findAll(){
        return new ResponseEntity<>(routeService.findAll(),HttpStatus.OK);
    }

    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<BaseResponseObject> findById(@Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(routeService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}")
    public ResponseEntity<BaseResponseObject> deleteRoute(@Valid @PathParam(value = "id") int id){
        return new ResponseEntity<>(routeService.delete(id),HttpStatus.OK);
    }

}
