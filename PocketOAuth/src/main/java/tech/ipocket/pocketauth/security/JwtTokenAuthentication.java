package tech.ipocket.pocketauth.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tech.ipocket.pocketauth.config.JwtConfig;
import tech.ipocket.pocketauth.entity.Client;
import tech.ipocket.pocketauth.response.BaseResponseObject;
import tech.ipocket.pocketauth.response.ErrorObject;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtTokenAuthentication extends UsernamePasswordAuthenticationFilter {

    private JwtConfig jwtConfig;
    private AuthenticationManager authenticationManager;

    public JwtTokenAuthentication(JwtConfig jwtConfig, AuthenticationManager authenticationManager) {
        this.jwtConfig = jwtConfig;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

        try {

            /**
             * 1. get credential from request
             * 2. Create auth object which will be used by auth manager
             * 3. Auth manager authenticate user by using userDetailsServiceImpl
             */

            Client clientInfo = new ObjectMapper().readValue(request.getInputStream(), Client.class);

            UsernamePasswordAuthenticationToken authToken =
                    new UsernamePasswordAuthenticationToken(clientInfo.getClientName(), clientInfo.getClientPassword());

            return authenticationManager.authenticate(authToken);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        Date tokenExpirationDate = getExpiration();

        String token = Jwts.builder()
                .setClaims(getClaims(authResult))
                .setSubject(getSubjects(authResult))
                .setExpiration(tokenExpirationDate)
                .signWith(SignatureAlgorithm.HS512,getSignatureContent())
                .compact();


        BaseResponseObject responseObject = new BaseResponseObject();
        responseObject.setData(new TokenResponse(token,tokenExpirationDate));
        responseObject.setError(null);
        responseObject.setStatus(HttpServletResponse.SC_OK);

        response.setContentType("application/json");
        response.getWriter().write(new ObjectMapper().writeValueAsString(responseObject));
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        BaseResponseObject responseObject = new BaseResponseObject();
        responseObject.setData(null);
        responseObject.setError(new ErrorObject("UnAuthorize user",HttpServletResponse.SC_UNAUTHORIZED));
        responseObject.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        response.setContentType("application/json");
        response.getWriter().write(new ObjectMapper().writeValueAsString(responseObject));
    }

    private String getSignatureContent() {
        return jwtConfig.getKey();
    }

    private Date getExpiration() {
        long currentTime = System.currentTimeMillis();

        return new Date(currentTime+jwtConfig.getExpiration());

    }

    private String getSubjects(Authentication authResult) {
        return ((User)authResult.getPrincipal()).getUsername();
    }

    private Map<String, Object> getClaims(Authentication authResult) {
        String loginId = ((User)authResult.getPrincipal()).getUsername();

        Map<String,Object> claims = new HashMap<>();

        claims.put("username",loginId);

        return claims;

    }

    private class TokenResponse{
        private String token;
        private Date expiration;

        public TokenResponse(String token, Date expiration) {
            this.token = token;
            this.expiration = expiration;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Date getExpiration() {
            return expiration;
        }

        public void setExpiration(Date expiration) {
            this.expiration = expiration;
        }
    }
}
