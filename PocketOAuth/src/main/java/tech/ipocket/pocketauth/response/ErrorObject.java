package tech.ipocket.pocketauth.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ErrorObject {
    private String errorMessage;
    private int errorCode;
}
