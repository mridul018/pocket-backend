package tech.ipocket.pocketauth.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BaseResponseObject {
    private Object data;
    private ErrorObject error;
    private int status;
    private String requestId;
}