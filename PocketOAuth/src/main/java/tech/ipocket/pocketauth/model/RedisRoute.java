package tech.ipocket.pocketauth.model;

import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RedisRoute implements Serializable {

    private String requestId;
    private String path;
    private String serviceId;
    private String url;
    private String contextPath;
}
