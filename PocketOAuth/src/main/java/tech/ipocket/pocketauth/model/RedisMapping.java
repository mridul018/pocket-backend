package tech.ipocket.pocketauth.model;

import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RedisMapping implements Serializable {
    private int id;
    private String clientId;
    private String routeId;
}
