package tech.ipocket.pocketauth.model;

import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RedisClient implements Serializable {

    private String clientId;
    private String clientName;
    private String clientPassword;
}
