package tech.ipocket.pocketexternal.service;

import tech.ipocket.pocketexternal.request.ServiceRequest;
import tech.ipocket.pocketexternal.response.BaseResponseObject;

public interface ExternalService {
    BaseResponseObject sendEmail(ServiceRequest request);
    BaseResponseObject sendSMS(ServiceRequest request);
    BaseResponseObject sendNotification(ServiceRequest request);
}
