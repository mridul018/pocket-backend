package tech.ipocket.pocketexternal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.ipocket.pocketexternal.request.ServiceRequest;
import tech.ipocket.pocketexternal.response.BaseResponseObject;
import tech.ipocket.pocketexternal.response.ErrorObject;
import tech.ipocket.pocketexternal.utils.ExternalConstant;

@Component
public class ExternalServiceFactory {

    @Autowired
    private ExternalService externalService;

    public BaseResponseObject externalServiceFactory(ServiceRequest request){

        switch (request.getType()){
            case ExternalConstant.EMAIL:
                return externalService.sendEmail(request);
            case ExternalConstant.SMS:
                return externalService.sendSMS(request);
            case ExternalConstant.NOTIFICATION:
                return externalService.sendNotification(request);

                default:
                    return new BaseResponseObject(null, new ErrorObject("External type not match","5000"),"400",null);
        }
    }
}
