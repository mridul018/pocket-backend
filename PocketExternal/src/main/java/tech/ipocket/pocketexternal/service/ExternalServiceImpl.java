package tech.ipocket.pocketexternal.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocketexternal.config.EmailConfig;
import tech.ipocket.pocketexternal.config.NotificationConfig;
import tech.ipocket.pocketexternal.config.SMSConfig;
import tech.ipocket.pocketexternal.request.ServiceRequest;
import tech.ipocket.pocketexternal.request.SmsSendRequest;
import tech.ipocket.pocketexternal.response.BaseResponseObject;
import tech.ipocket.pocketexternal.response.ErrorObject;
import tech.ipocket.pocketexternal.utils.HeaderRequestInterceptor;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class ExternalServiceImpl implements ExternalService {

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private SMSConfig smsConfig;

    @Autowired
    private NotificationConfig notificationConfig;

    @Override
    public BaseResponseObject sendEmail(ServiceRequest request) {
        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setPort(emailConfig.getPort());
            mailSender.setHost(emailConfig.getHost());
            mailSender.setUsername(emailConfig.getUserName());
            mailSender.setPassword(emailConfig.getPassword());
            mailSender.setJavaMailProperties(getProperties());
            mailSender.setSession(getSession());

            request.getTo().forEach(to -> {
                SimpleMailMessage mailMessage = new SimpleMailMessage();
                mailMessage.setFrom(emailConfig.getUserName());
                mailMessage.setTo(to);
                mailMessage.setSubject(request.getSubject());
                mailMessage.setText(request.getBody());

                mailSender.send(mailMessage);
            });



            return new BaseResponseObject("Email send",null,String.valueOf(HttpStatus.OK),"");
        }catch (Exception ex){
            System.err.println("Email sending error: "+ex.getMessage());
        }

        return new BaseResponseObject(null,new ErrorObject("Email not send",String.valueOf(HttpStatus.BAD_REQUEST)),String.valueOf(HttpStatus.BAD_REQUEST),"");
    }

    @Override
    public BaseResponseObject sendSMS(ServiceRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(smsConfig.getUsername(), smsConfig.getPassword());

        try {
            request.getTo().forEach(to -> {
                SmsSendRequest smsSendRequest = new SmsSendRequest();
                smsSendRequest.setFrom(smsConfig.getFrom());
                smsSendRequest.setTo(to);
                smsSendRequest.setText(request.getBody());

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.exchange(smsConfig.getUrl(), HttpMethod.POST, new HttpEntity<>(smsSendRequest, headers), String.class);
            });

            return new BaseResponseObject("SMS send",null,"200","");
        }catch (Exception ex){
            return new BaseResponseObject(null, new ErrorObject("SMS not send","400"),"400","");
        }

    }

    @Override
    public BaseResponseObject sendNotification(ServiceRequest request) {
        try {
            JSONObject object = new JSONObject();
            object.put("title",request.getSubject());
            object.put("body",request.getBody());
            object.put("notificationType","Test");
            object.put("notification",request.getBody());

            request.getTo().forEach(to -> {
                object.put("to",to);
                HttpEntity<String> httpEntity = new HttpEntity<>(object.toString());
                CompletableFuture<String> fcmResponse = send(httpEntity);
                CompletableFuture.allOf(fcmResponse).join();

                try {
                    fcmResponse.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            return new BaseResponseObject("Notification send",null,"200","");
        }catch (Exception ex){
            return new BaseResponseObject(null, new ErrorObject("Notification not send","400"),"400","");
        }
    }


    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.starttls.enable", "true");

        return properties;
    }

    private Session getSession() {
        return Session.getDefaultInstance(getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailConfig.getUserName(), emailConfig.getPassword());
            }
        });
    }

    @Async
    public CompletableFuture<String> send(HttpEntity<String> entity){
        RestTemplate restTemplate = new RestTemplate();
        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Authorization","key=" + notificationConfig.getServerKey()));
        interceptors.add(new HeaderRequestInterceptor("Content-type","application/json"));
        restTemplate.setInterceptors(interceptors);
        String fireballResponse = restTemplate.postForObject(notificationConfig.getUrl(),entity,String.class);
        return CompletableFuture.completedFuture(fireballResponse);
    }
}
