package tech.ipocket.pocketexternal.response;

public class BaseResponseObject {
    private Object data;
    private ErrorObject error;
    private String status;
    private String requestId;

    public BaseResponseObject() {
    }

    public BaseResponseObject(Object data, ErrorObject error, String status, String requestId) {
        this.data = data;
        this.error = error;
        this.status = status;
        this.requestId = requestId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ErrorObject getError() {
        return error;
    }

    public void setError(ErrorObject error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
