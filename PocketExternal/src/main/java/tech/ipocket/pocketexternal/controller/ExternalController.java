package tech.ipocket.pocketexternal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocketexternal.request.ServiceRequest;
import tech.ipocket.pocketexternal.response.BaseResponseObject;
import tech.ipocket.pocketexternal.service.ExternalServiceFactory;

@RestController
@RequestMapping(value = "/")
public class ExternalController {

    @Autowired
    private ExternalServiceFactory externalServiceFactory;

    @PostMapping(value = "/services")
    public ResponseEntity<BaseResponseObject> serviceProvider(@RequestBody ServiceRequest request){
        return new ResponseEntity<>(externalServiceFactory.externalServiceFactory(request), HttpStatus.OK);

    }
}
