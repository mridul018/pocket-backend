package tech.ipocket.pocket.request.web;


import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.Date;

public class GroupRequest extends BaseRequestObject {

    private int GroupId;
    private String groupCode;
    private String groupName;
    private String description;
    private String groupStatus;

    public GroupRequest() {
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (groupCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeRequired);
            return;
        }

        if (groupCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeLengthError);
            return;
        }

        if (groupName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupNameRequired);
            return;
        }
    }


}
