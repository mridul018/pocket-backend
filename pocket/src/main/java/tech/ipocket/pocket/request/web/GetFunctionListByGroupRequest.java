package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetFunctionListByGroupRequest extends BaseRequestObject {

    private String groupCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (groupCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeRequired);
            return;
        }

    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
}
