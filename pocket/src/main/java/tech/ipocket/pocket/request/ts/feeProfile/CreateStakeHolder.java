package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateStakeHolder extends BaseRequestObject {
    private String stakeholderName;
    private String glCode;

    public String getStakeholderName() {
        return stakeholderName;
    }

    public void setStakeholderName(String stakeholderName) {
        this.stakeholderName = stakeholderName;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (stakeholderName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.StakeHolderNameRequired));
            return;
        }

        if (glCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.GlCodeNotRequired));
            return;
        }
    }
}
