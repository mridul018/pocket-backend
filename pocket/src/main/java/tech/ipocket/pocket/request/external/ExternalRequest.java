package tech.ipocket.pocket.request.external;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ExternalRequest extends BaseRequestObject {

    private String to;
    private String body;
    private String subject;
    private String type;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (to == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_TO_NOT_FOUND));
            return;
        }

        if (body == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_BODY_NOT_FOUND));
            return;
        }

        if (type == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_TYPE_NOT_FOUND));
            return;
        }
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
