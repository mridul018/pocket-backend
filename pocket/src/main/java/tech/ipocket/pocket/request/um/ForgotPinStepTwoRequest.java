package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ForgotPinStepTwoRequest extends BaseRequestObject {

    private String newPin;
    private String otpSecret;
    private String otpText;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (newPin == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.CredentialRequired));
            return;
        }

        if (otpSecret == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.InvalidInput));
            return;
        }

        if (otpText == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.OtpRequired));
            return;
        }
    }

    public String getNewPin() {
        return newPin;
    }

    public void setNewPin(String newPin) {
        this.newPin = newPin;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }
}
