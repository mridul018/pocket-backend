package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;


public class RegistrationRequest extends BaseRequestObject {

    private String fullName;
    private String mobileNo;
    private String email;
    private String password;
    private String groupCode;
    private String primaryIdNumber; // m
    private String primaryIdIssueDate; // m
    private String primaryIdExpiryDate; // m
    private String primaryIdType; //m
    private String nationality; // m
    private String metaData;

    public RegistrationRequest() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    public String getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(String primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    public String getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(String primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (fullName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FullNameRequired);
            return;
        }
        if (password == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.CredentialRequired);
            return;
        }
        if (groupCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeRequired);
            return;
        }
    }
}
