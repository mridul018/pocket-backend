package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserListRequest extends BaseRequestObject {


    private String groupCode;
    private int page;
    private int size;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (page < 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PAGE_NUMBER_NOT_FOUND));
            return;
        }

        if (size <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PAGE_SIZE_NOT_FOUND));
            return;
        }

    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
