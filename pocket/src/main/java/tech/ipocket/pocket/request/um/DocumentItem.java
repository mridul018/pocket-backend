package tech.ipocket.pocket.request.um;

public class DocumentItem {
    private String documentAsString;
    private String documentType;

    public String getDocumentAsString() {
        return documentAsString;
    }

    public void setDocumentAsString(String documentAsString) {
        this.documentAsString = documentAsString;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
