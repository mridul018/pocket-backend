package tech.ipocket.pocket.request;

public abstract class PageItemBaseRequestObject extends BaseRequestObject{
    private int pageId;
    private int numberOfItemPerPage=20;

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getNumberOfItemPerPage() {
        return numberOfItemPerPage;
    }

    public void setNumberOfItemPerPage(int numberOfItemPerPage) {
        this.numberOfItemPerPage = numberOfItemPerPage;
    }
}
