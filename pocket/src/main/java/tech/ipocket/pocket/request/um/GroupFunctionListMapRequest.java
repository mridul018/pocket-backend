package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.web.GroupFunctionMap;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class GroupFunctionListMapRequest extends BaseRequestObject {
    private String groupCode;
    List<String> functionsCodes;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(groupCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeRequired);
            return;
        }

        if(functionsCodes==null||functionsCodes.size()==0){
            baseResponseObject.setErrorCode(PocketErrorCode.FunctionListRequired);
            return;
        }

    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public List<String> getFunctionsCodes() {
        return functionsCodes;
    }

    public void setFunctionsCodes(List<String> functionsCodes) {
        this.functionsCodes = functionsCodes;
    }
}
