package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class CreateStakeHolderFeeMapRequest extends BaseRequestObject {

    private int feeId;

    private List<FeeStakeHolderItem> stakeHolderItems;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (feeId <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeIdRequired));
            return;
        }

        if (stakeHolderItems == null || stakeHolderItems.size() == 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.StakeHolderRequired));
            return;
        }
    }

    public int getFeeId() {
        return feeId;
    }

    public void setFeeId(int feeId) {
        this.feeId = feeId;
    }

    public List<FeeStakeHolderItem> getStakeHolderItems() {
        return stakeHolderItems;
    }

    public void setStakeHolderItems(List<FeeStakeHolderItem> stakeHolderItems) {
        this.stakeHolderItems = stakeHolderItems;
    }
}
