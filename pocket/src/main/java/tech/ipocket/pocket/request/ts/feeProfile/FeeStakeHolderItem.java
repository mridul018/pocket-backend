package tech.ipocket.pocket.request.ts.feeProfile;

public class FeeStakeHolderItem {
    private int stakeHolderId;
    private String stakeholderName;
    private String glCode;
    private double percentage;

    public String getStakeholderName() {
        return stakeholderName;
    }

    public void setStakeholderName(String stakeholderName) {
        this.stakeholderName = stakeholderName;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public int getStakeHolderId() {
        return stakeHolderId;
    }

    public void setStakeHolderId(int stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
    }
}
