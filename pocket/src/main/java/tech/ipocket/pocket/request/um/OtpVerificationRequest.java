package tech.ipocket.pocket.request.um;


import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class OtpVerificationRequest extends BaseRequestObject {
    private String otpText;
    private String otpSecret;

    public OtpVerificationRequest() {
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (otpSecret == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.OtpSecretRequired);
            return;
        }

        if (otpText == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.OtpRequired);
            return;
        }
    }
}
