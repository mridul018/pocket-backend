package tech.ipocket.pocket.request.um;

import com.sun.org.apache.regexp.internal.RE;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserDeviceListUpdateRequest extends BaseRequestObject {
    private int deviceId;
    private int userId;
    private String status;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (deviceId <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.DEVICE_NOT_FOUND));
            return;
        }
        if (userId <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.USER_NOT_FOUND));
            return;
        }
        if (status == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.DEVICE_STATUS_NOT_FOUND));
            return;
        }
        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }
        if (getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
