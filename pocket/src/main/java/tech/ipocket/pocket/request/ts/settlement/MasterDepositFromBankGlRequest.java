package tech.ipocket.pocket.request.ts.settlement;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class MasterDepositFromBankGlRequest extends BaseRequestObject {
    private String bankGlCode;
    private BigDecimal amount;
    private String bankCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (bankGlCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankGlCodeRequired);
            return;
        }

        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public String getBankGlCode() {
        return bankGlCode;
    }

    public void setBankGlCode(String bankGlCode) {
        this.bankGlCode = bankGlCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
