package tech.ipocket.pocket.request.ts.transactionProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateTransactionProfileRequest extends BaseRequestObject {
    private String profileCode;
    private String profileName;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (profileCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TransactionProfileCodeRequired));
            return;
        }


        if (profileCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.ProfileCodeLengthError);
            return;
        }

        if (profileName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TransactionProfileNameRequired));
            return;
        }

    }

    public String getProfileCode() {
        return profileCode;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }
}
