package tech.ipocket.pocket.request.ts.transaction;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TransactionRequest extends BaseRequestObject {
    private Double transferAmount;
    private String transactionType;
    private String senderMobileNumber;
    private String receiverMobileNumber;
    private String otpText;
    private String otpSecret;
    private String pin;
    private String notes;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (senderMobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.SenderWalletIsRequired);
            return;
        }
        if (receiverMobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.ReceiverWalletIsRequired);
            return;
        }

        if (transactionType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionType);
            return;
        }


        if (transferAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public Double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Double transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getReceiverMobileNumber() {
        return receiverMobileNumber;
    }

    public void setReceiverMobileNumber(String receiverMobileNumber) {
        this.receiverMobileNumber = receiverMobileNumber;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
