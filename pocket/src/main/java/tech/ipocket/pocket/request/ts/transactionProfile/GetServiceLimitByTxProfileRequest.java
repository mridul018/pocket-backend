package tech.ipocket.pocket.request.ts.transactionProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetServiceLimitByTxProfileRequest extends BaseRequestObject {

    private String txProfileCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if(txProfileCode==null){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TransactionProfileCodeRequired));
            return;
        }
    }

    public String getTxProfileCode() {
        return txProfileCode;
    }

    public void setTxProfileCode(String txProfileCode) {
        this.txProfileCode = txProfileCode;
    }
}
