package tech.ipocket.pocket.response.ts.transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryResponseRoot {

    private List<TransactionHistoryResponse> history;

    public TransactionHistoryResponseRoot() {
        history=new ArrayList<>();
    }

    public List<TransactionHistoryResponse> getHistory() {
        return history;
    }

    public void setHistory(List<TransactionHistoryResponse> history) {
        this.history = history;
    }

}
