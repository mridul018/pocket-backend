package tech.ipocket.pocket.response.external;

import tech.ipocket.pocket.response.BaseResponseObject;

public class ExternalResponse extends BaseResponseObject {
    private boolean ableToSend;

    public ExternalResponse() {
    }

    public ExternalResponse(boolean ableToSend) {
        this.ableToSend = ableToSend;
    }

    public boolean isAbleToSend() {
        return ableToSend;
    }

    public void setAbleToSend(boolean ableToSend) {
        this.ableToSend = ableToSend;
    }
}
