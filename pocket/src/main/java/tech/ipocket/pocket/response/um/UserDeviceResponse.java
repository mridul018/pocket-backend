package tech.ipocket.pocket.response.um;

import java.util.Date;

public class UserDeviceResponse {
    private int id;
    private String hardwareSignature;
    private String deviceName;
    private String metaData;
    private Date createdDate;
    private String status;

    public UserDeviceResponse() {
    }

    public UserDeviceResponse(int id, String hardwareSignature, String deviceName, String metaData, Date createdDate, String status) {
        this.id = id;
        this.hardwareSignature = hardwareSignature;
        this.deviceName = deviceName;
        this.metaData = metaData;
        this.createdDate = createdDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHardwareSignature() {
        return hardwareSignature;
    }

    public void setHardwareSignature(String hardwareSignature) {
        this.hardwareSignature = hardwareSignature;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
