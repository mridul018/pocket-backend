package tech.ipocket.pocket.response.um;

import java.util.List;

public class LoginResponse {
    private boolean deviceVerificationNeeded;
    private String secretKey;

    private String fullName;
    private String mobileNumber;
    private String accountType;
    private String profilePic;
    private UserSettingsResponse settingsResponse;

    List<UserFunctionResponseItem> functions;
    private String sessionToken;


    public boolean isDeviceVerificationNeeded() {
        return deviceVerificationNeeded;
    }

    public void setDeviceVerificationNeeded(boolean deviceVerificationNeeded) {
        this.deviceVerificationNeeded = deviceVerificationNeeded;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public UserSettingsResponse getSettingsResponse() {
        return settingsResponse;
    }

    public void setSettingsResponse(UserSettingsResponse settingsResponse) {
        this.settingsResponse = settingsResponse;
    }

    public List<UserFunctionResponseItem> getFunctions() {
        return functions;
    }

    public void setFunctions(List<UserFunctionResponseItem> functions) {
        this.functions = functions;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getSessionToken() {
        return sessionToken;
    }
}
