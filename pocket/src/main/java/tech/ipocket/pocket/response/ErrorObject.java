package tech.ipocket.pocket.response;

import tech.ipocket.pocket.utils.PocketErrorCode;

public class ErrorObject {
    private String errorMessage;
    private String errorCode;

    public ErrorObject() {
    }

    public ErrorObject(String errorCode, String localizedMessage) {
        this.errorCode = errorCode;
        this.errorMessage = localizedMessage;
    }

    public ErrorObject(PocketErrorCode pocketErrorCode) {
        this.setErrorCode("" + pocketErrorCode.getErrorCode());
        this.errorMessage = null;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
