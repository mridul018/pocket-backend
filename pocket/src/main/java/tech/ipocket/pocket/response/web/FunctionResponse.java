package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.response.BaseResponseObject;

public class FunctionResponse {
    private int functionId;
    private String message;

    public FunctionResponse() {
    }

    public FunctionResponse(int functionId, String message) {
        this.functionId = functionId;
        this.message = message;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
