package tech.ipocket.pocket.response.ts.transactionValidity;

import java.math.BigDecimal;

public class TransactionValidityResponse {

    private BigDecimal fee;
    private String payer;

    public TransactionValidityResponse() {
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }
}