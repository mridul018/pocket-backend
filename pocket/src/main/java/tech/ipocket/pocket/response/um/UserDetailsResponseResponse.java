package tech.ipocket.pocket.response.um;

public class UserDetailsResponseResponse {
    private String fullName;
    private String mobileNumber;
    private String groupCode;
    private String photoUrl;
    private int userId;
    private Boolean isMobileNumberVerified;
    private Boolean isPrimaryIdVerified;

    public UserDetailsResponseResponse() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Boolean getMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setMobileNumberVerified(Boolean mobileNumberVerified) {
        isMobileNumberVerified = mobileNumberVerified;
    }

    public Boolean getPrimaryIdVerified() {
        return isPrimaryIdVerified;
    }

    public void setPrimaryIdVerified(Boolean primaryIdVerified) {
        isPrimaryIdVerified = primaryIdVerified;
    }
}
