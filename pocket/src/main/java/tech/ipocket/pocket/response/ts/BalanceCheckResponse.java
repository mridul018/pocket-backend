package tech.ipocket.pocket.response.ts;

import java.math.BigDecimal;

public class BalanceCheckResponse {
    private BigDecimal balance;
    private String date;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
