package tech.ipocket.pocket.response.security;

import tech.ipocket.pocket.response.BaseResponseObject;

public class TokenGenerateResponse extends BaseResponseObject {
    private String token;
    private int sessionId;

    public TokenGenerateResponse() {
    }

    public TokenGenerateResponse(String requestId, String token, int sessionId) {
        super(requestId);
        this.token = token;
        this.sessionId = sessionId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }
}
