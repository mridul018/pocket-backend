package tech.ipocket.pocket.controllers.external;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.external.ExternalFactory;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/external")
@Api(tags = "external")
public class ExternalController {

    @Autowired
    private ExternalFactory externalFactory;

    @PostMapping(value = "/services")
    public ResponseEntity<BaseResponseObject> externalServices(@RequestBody ExternalRequest request) throws PocketException {
        return new ResponseEntity<>(externalFactory.externalServices(request), HttpStatus.OK);
    }
}
