package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.GroupFunctionListMapRequest;
import tech.ipocket.pocket.request.um.GroupFunctionMapRequest;
import tech.ipocket.pocket.request.web.GetFunctionListByGroupRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.web.FunctionListResponse;
import tech.ipocket.pocket.response.web.GroupFunctionMapListResponse;
import tech.ipocket.pocket.response.web.GroupFunctionMapResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.web.GroupFunctionMap.GroupFunctionMapService;
import tech.ipocket.pocket.services.web.function.FunctionService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import javax.swing.border.EmptyBorder;

@RestController
@RequestMapping(value = "/")
@Api(tags = "web")
public class GroupFunctionMapController extends BaseEndpoint {

    @Autowired
    private GroupFunctionMapService groupFunctionMapService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private FunctionService functionService;

    @ApiOperation(value = "This api is about groupFunction add, update and delete")
    @PostMapping(value = "/newMapping")
    public ResponseEntity<BaseResponseObject> groupFunctionMapping(@RequestBody GroupFunctionMapRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        GroupFunctionMapResponse groupFunctionMapResponse = groupFunctionMapService.addUpdateDeleteGroupFunctionMap(request);

        response.setData(groupFunctionMapResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @ApiOperation(value = "This api is about Map group with functions")
    @PostMapping(value = "/mapGroupWithFunctions")
    public ResponseEntity<BaseResponseObject> mapGroupWithFunctions(@RequestBody GroupFunctionListMapRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        BaseResponseObject groupFunctionMapResponse = groupFunctionMapService.mapGroupWithFunctions(request);
        return new ResponseEntity<>(groupFunctionMapResponse, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about all groupFunction mapping.")
    @PostMapping(value = "/allMapping")
    public ResponseEntity<BaseResponseObject> allMapping(@RequestBody EmptyRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        GroupFunctionMapListResponse groupFunctionMapListResponse = groupFunctionMapService.allGroupFunctionMapList();

        response.setData(groupFunctionMapListResponse);
        response.setStatus(PocketConstants.OK);
        response.setError(null);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about getting function by using group code")
    @PostMapping(value = "/getFunctionsByGroupCode")
    public ResponseEntity<BaseResponseObject> getFunctionsByGroupCode(@RequestBody GetFunctionListByGroupRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        FunctionListResponse groupFunctionMapListResponse = functionService.getFunctionsByGroupCode(request.getGroupCode());

        response.setData(groupFunctionMapListResponse);
        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setError(null);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    private boolean tokenVerify(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse tokenValidateUmResponse =
                securityService.validateToken(request);
        if (tokenValidateUmResponse != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
