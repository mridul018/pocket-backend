package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.request.ts.account.TsUpdateClientStatusRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.RegistrationResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.registration.RegistrationFactory;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/")
@Api(tags = "app")
public class AccountController extends BaseEndpoint {

    @Autowired
    private AccountService accountService;

    @Autowired
    private RegistrationFactory registrationFactory;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/createWallet")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createWallet(@RequestBody WalletCreateRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        RegistrationResponse response = registrationFactory
                .doRegistration(request, request.getGroupCode());

        if (response == null || response.getStatus() != 200) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/checkBalance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkBalance(@RequestBody BalanceCheckRequest request) throws Exception {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(accountService.checkBalance(tsSessionObject.getMobileNumber(),
                request.getRequestId()), HttpStatus.OK);
    }


    @PostMapping(value = "/updateClientStatus")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateClientStatus(@RequestBody TsUpdateClientStatusRequest request) throws Exception {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(accountService.updateClientStatus(request.getWalletId(), request.getNewStatus(),
                request.getRequestId()), HttpStatus.OK);
    }

}
