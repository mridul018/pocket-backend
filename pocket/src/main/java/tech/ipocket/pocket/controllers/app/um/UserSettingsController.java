package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.security.TokenValidateRequest;
import tech.ipocket.pocket.request.um.UserSettingsRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.um.UserSettingsResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.settings.UserSettingsService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/")
@Api(tags = "app")
public class UserSettingsController extends BaseEndpoint {

    @Autowired
    private UserSettingsService userSettingsService;
    @Autowired
    private SecurityService securityService;

    @PostMapping(value = "/settings")
    public ResponseEntity<BaseResponseObject> userSettings(@RequestBody UserSettingsRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);
        //TODO verify token
        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(
                new TokenValidateRequest(request.getSessionToken(), request.getHardwareSignature())
        );
        if (tokenValidateUmResponse == null) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            response.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        UserSettingsResponse userSettings = userSettingsService.getUserSettings(request.getLoginId());
        response.setData(userSettings);
        response.setError(null);
        response.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
