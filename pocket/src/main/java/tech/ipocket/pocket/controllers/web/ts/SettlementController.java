package tech.ipocket.pocket.controllers.web.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.settlement.AdminCashInToCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.MasterDepositFromBankGlRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.settlement.SettlementService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/")
@Api(tags = "web")
public class SettlementController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private SettlementService settlementService;

    @PostMapping(value = "/depositToMasterFromBankGl")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> depositToMasterFromBankGl(@RequestBody MasterDepositFromBankGlRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        return new ResponseEntity<>(settlementService.depositToMasterFromBankGl(request), HttpStatus.OK);

    }

    @PostMapping(value = "/adminCashInToCustomer")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> adminCashInToCustomer(@RequestBody AdminCashInToCustomerRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        return new ResponseEntity<>(settlementService.adminCashInToCustomer(request), HttpStatus.OK);

    }

}
