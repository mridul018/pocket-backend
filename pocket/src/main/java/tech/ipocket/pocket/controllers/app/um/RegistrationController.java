package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.OtpVerificationRequest;
import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.services.um.registration.RegistrationService;
import tech.ipocket.pocket.services.um.registration.UMRegistrationFactory;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/")
public class RegistrationController extends BaseEndpoint {

    @Autowired
    private UMRegistrationFactory umRegistrationFactory;

    @Autowired
    private RegistrationService registrationService;

    @ApiOperation(value = "", tags = "mobile")
    @PostMapping(value = "/registration")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> mobileRegistration(@RequestBody RegistrationRequest registrationRequest) throws PocketException {
        return doRegistration(registrationRequest);
    }

    private ResponseEntity<BaseResponseObject> doRegistration(RegistrationRequest registrationRequest) throws PocketException {
        BaseResponseObject registrationResponse = new BaseResponseObject();

        validateRequest(registrationRequest, registrationResponse);

        RegistrationResponse response = umRegistrationFactory.doRegistration(registrationRequest, registrationRequest.getGroupCode());

        // send success response

        if (response == null) {
            registrationResponse.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            registrationResponse.setData(null);
            registrationResponse.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(registrationResponse, HttpStatus.BAD_REQUEST);
        } else {
            registrationResponse.setError(null);
            registrationResponse.setData(response);
            registrationResponse.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "", tags = "mobile")
    @PostMapping(value = "/regOtpVerify")
    public ResponseEntity<BaseResponseObject> regOtpVerify(@RequestBody OtpVerificationRequest otpVerificationRequest) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        validateRequest(otpVerificationRequest, baseResponseObject);

        baseResponseObject=registrationService.doVerifyUser(otpVerificationRequest);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);

    }
}
