package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.security.TokenValidateRequest;
import tech.ipocket.pocket.request.um.UserDeviceListRequest;
import tech.ipocket.pocket.request.um.UserDeviceListUpdateRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.um.UserDeviceListUpdateResponse;
import tech.ipocket.pocket.response.um.UserDeviceResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.device.UserDeviceService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/")
@Api(tags = "app")
public class UserDeviceController extends BaseEndpoint {

    @Autowired
    private UserDeviceService userDeviceService;

    @Autowired
    private SecurityService securityService;

    @PostMapping(value = "/deviceList")
    public ResponseEntity<BaseResponseObject> allUserDeviceList(@RequestBody UserDeviceListRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);
        // TODO Token validate
        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(
                new TokenValidateRequest(request.getSessionToken(), request.getHardwareSignature())
        );

        if (tokenValidateUmResponse == null) {
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);
        }

        ArrayList<UserDeviceResponse> allUserDevice = userDeviceService.getAllUserDevice(request.getLoginId());

        response.setData(allUserDevice);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/updateDeviceList")
    public ResponseEntity<BaseResponseObject> updateDeviceList(@RequestBody UserDeviceListUpdateRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);
        // TODO Token validate
        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(
                new TokenValidateRequest(request.getSessionToken(), request.getHardwareSignature())
        );

        if (tokenValidateUmResponse == null) {
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);
        }

        UserDeviceListUpdateResponse updateResponse = userDeviceService.updateUserDevice(request);

        response.setData(updateResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
