package tech.ipocket.pocket.controllers.mobile.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transaction.AgentCashInService;
import tech.ipocket.pocket.services.ts.transaction.FundTransferService;
import tech.ipocket.pocket.services.ts.transaction.MerchantPaymentService;
import tech.ipocket.pocket.services.ts.transaction.MobileTopUpService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.ServiceCodeConstants;

@RestController
@RequestMapping(value = "/")
@Api(tags = "mobile")
public class TransactionController extends BaseEndpoint {


    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private FundTransferService fundTransferService;
    @Autowired
    private MobileTopUpService mobileTopUpService;

    @Autowired
    private AgentCashInService agentCashInService;

    @Autowired
    private MerchantPaymentService merchantPaymentService;



    @PostMapping(value = "/createTransaction")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createTransaction(@RequestBody TransactionRequest request) throws Exception {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        SessionCheckWithCredentialRequest requestWithPin=createRequest(request);

        TsSessionObject tsSessionObject = requestValidator.validateSessionWithCredential(requestWithPin);

        switch (request.getTransactionType()) {
            case ServiceCodeConstants
                    .Fund_Transfer:
                baseResponseObject = fundTransferService.doFundTransfer(request, tsSessionObject);
                break;
            case ServiceCodeConstants
                    .CashIn:
                baseResponseObject = agentCashInService.doAgentCashIn(request, tsSessionObject);
                break;
            case ServiceCodeConstants.Top_Up:
                baseResponseObject = mobileTopUpService.doMobileTopUp(request, tsSessionObject);
                break;
            case ServiceCodeConstants.Merchant_Payment:
                baseResponseObject = merchantPaymentService.doMerchantPayment(request, tsSessionObject);
                break;
            default:
                throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    private SessionCheckWithCredentialRequest createRequest(TransactionRequest request) {

        SessionCheckWithCredentialRequest withCredentialRequest=new SessionCheckWithCredentialRequest();
        withCredentialRequest.setCredential(request.getPin());
        withCredentialRequest.setOtpText(request.getOtpText());
        withCredentialRequest.setOtpSecret(request.getOtpSecret());
        withCredentialRequest.setHardwareSignature(request.getHardwareSignature());
        withCredentialRequest.setSessionToken(request.getSessionToken());
        withCredentialRequest.setRequestId(request.getRequestId());

        return withCredentialRequest;
    }

}
