package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.ChangeCredentialRequest;
import tech.ipocket.pocket.request.um.GetUserInfoRequest;
import tech.ipocket.pocket.request.um.UpdateDocumentRequest;
import tech.ipocket.pocket.request.um.UpdateFcmKeyRequest;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.ProfileService;
import tech.ipocket.pocket.services.web.user.UserService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/")
@Api(tags = "web")
public class UserController extends BaseEndpoint {

    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;

    @Autowired
    private ProfileService profileService;

    @ApiOperation(value = "This api is about to get user info by loginId or mobile number")
    @PostMapping(value = "/getUserInfo")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserInfo(@RequestBody GetUserInfoRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = profileService.getUserInfo(request);

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(validateResponse);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get user info by loginId or mobile number")
    @PostMapping(value = "/getUserDetails")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserDetails(@RequestBody GetUserInfoRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject(request.getRequestId());

        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = profileService.getUserDetails(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get all users. In this " +
            " request you have to proved your page number and size of data " +
            "that you want to show")
    @PostMapping(value = "/listOfAllUser")
    public ResponseEntity<BaseResponseObject> listOfAllUser(@RequestBody UserListRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.getUserList(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to search any user using mobile number or " +
            "email address.")
    @PostMapping(value = "/searchUser")
    public ResponseEntity<BaseResponseObject> userSearch(@RequestBody UserSearchRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.searchUser(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user")
    @PostMapping(value = "/edit_user")
    public ResponseEntity<BaseResponseObject> userEdit(@RequestBody UserEditRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.editUser(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to change login credentials.")
    @PostMapping(value = "/changeCredential")
    public ResponseEntity<BaseResponseObject> changeCredential(@RequestBody ChangeCredentialRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);


        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        response = userService.changeCredential(request, validateResponse);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to reset PIN/Password")
    @PostMapping(value = "/pinOrPasswordReset")
    public ResponseEntity<BaseResponseObject> pinOrPasswordReset(@RequestBody UserPinPasswordResetRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.pinReset(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to lock or unlock user wallet status.")
    @PostMapping(value = "/lockOrUnlockWallet")
    public ResponseEntity<BaseResponseObject> lockOrUnlockWallet(@RequestBody UserLockUnlockWalletRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.localUnlockWallet(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to verify user primary identification number, issue date and expire date")
    @PostMapping(value = "/verifyPrimaryInformation")
    public ResponseEntity<BaseResponseObject> verifyPrimaryIdInfo(@RequestBody UserPrimaryIdInfoVerificationRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.verifyPrimaryIdInfo(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get user details with wallet information")
    @PostMapping(value = "/userDetailsWithWallet")
    public ResponseEntity<BaseResponseObject> userDetailsWithWallet(@RequestBody UserDetailsWithWalletRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.userDetailsWithWallet(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to show all device information which is being used in this apps.")
    @PostMapping(value = "/getUserDevices")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserDevices(@RequestBody EmptyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        baseResponseObject = profileService.getUserDevices(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user FCM or cloud messaging key")
    @PostMapping(value = "/updateFcmKey")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateFcmKey(@RequestBody UpdateFcmKeyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        baseResponseObject = profileService.updateFcmKey(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user document")
    @PostMapping(value = "/updateUserDocuments")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateUserDocuments(@RequestBody UpdateDocumentRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        if (request.getMobileNumber() == null) {
            request.setMobileNumber(validateResponse.getMobileNumber());
        }

        baseResponseObject = profileService.updateUserDocuments(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }


    private boolean tokenVerify(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse tokenValidateUmResponse =
                securityService.validateToken(request);
        if (tokenValidateUmResponse != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
