package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "UM_user_info", schema = "PocketWallet", catalog = "")
public class UmUserInfo {
    private int id;
    private String fullName;
    private String mobileNumber;
    private String loginId;
    private String email;
    private String password;
    private String groupCode;
    private int accountStatus;
    private Integer photoId;
    private String dob;
    private Collection<UmActivityLog> umActivityLogsById;
    private Collection<UmBeneficiary> umBeneficiariesById;
    private Collection<UmDocuments> umDocumentsById;
    private Collection<UmOtp> umOtpsById;
    private Collection<UmQrCodes> umQrCodesById;
    private Collection<UmUserDetails> umUserDetailsById;
    private Collection<UmUserDeviceMapping> umUserDeviceMappingsById;
    private Collection<UmUserSession> umUserSessionsById;
    private Collection<UmUserSettings> umUserSettingsById;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "mobile_number")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Basic
    @Column(name = "login_id")
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "group_code")
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Basic
    @Column(name = "account_status")
    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Basic
    @Column(name = "photo_id")
    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    @Basic
    @Column(name = "dob")
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmUserInfo userInfo = (UmUserInfo) o;
        return id == userInfo.id &&
                accountStatus == userInfo.accountStatus &&
                Objects.equals(fullName, userInfo.fullName) &&
                Objects.equals(mobileNumber, userInfo.mobileNumber) &&
                Objects.equals(loginId, userInfo.loginId) &&
                Objects.equals(email, userInfo.email) &&
                Objects.equals(password, userInfo.password) &&
                Objects.equals(groupCode, userInfo.groupCode) &&
                Objects.equals(photoId, userInfo.photoId) &&
                Objects.equals(dob, userInfo.dob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, mobileNumber, loginId, email, password, groupCode, accountStatus, photoId, dob);
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmActivityLog> getUmActivityLogsById() {
        return umActivityLogsById;
    }

    public void setUmActivityLogsById(Collection<UmActivityLog> umActivityLogsById) {
        this.umActivityLogsById = umActivityLogsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmBeneficiary> getUmBeneficiariesById() {
        return umBeneficiariesById;
    }

    public void setUmBeneficiariesById(Collection<UmBeneficiary> umBeneficiariesById) {
        this.umBeneficiariesById = umBeneficiariesById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmDocuments> getUmDocumentsById() {
        return umDocumentsById;
    }

    public void setUmDocumentsById(Collection<UmDocuments> umDocumentsById) {
        this.umDocumentsById = umDocumentsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmOtp> getUmOtpsById() {
        return umOtpsById;
    }

    public void setUmOtpsById(Collection<UmOtp> umOtpsById) {
        this.umOtpsById = umOtpsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmQrCodes> getUmQrCodesById() {
        return umQrCodesById;
    }

    public void setUmQrCodesById(Collection<UmQrCodes> umQrCodesById) {
        this.umQrCodesById = umQrCodesById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmUserDetails> getUmUserDetailsById() {
        return umUserDetailsById;
    }

    public void setUmUserDetailsById(Collection<UmUserDetails> umUserDetailsById) {
        this.umUserDetailsById = umUserDetailsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmUserDeviceMapping> getUmUserDeviceMappingsById() {
        return umUserDeviceMappingsById;
    }

    public void setUmUserDeviceMappingsById(Collection<UmUserDeviceMapping> umUserDeviceMappingsById) {
        this.umUserDeviceMappingsById = umUserDeviceMappingsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmUserSession> getUmUserSessionsById() {
        return umUserSessionsById;
    }

    public void setUmUserSessionsById(Collection<UmUserSession> umUserSessionsById) {
        this.umUserSessionsById = umUserSessionsById;
    }

    @OneToMany(mappedBy = "umUserInfoByUserId")
    public Collection<UmUserSettings> getUmUserSettingsById() {
        return umUserSettingsById;
    }

    public void setUmUserSettingsById(Collection<UmUserSettings> umUserSettingsById) {
        this.umUserSettingsById = umUserSettingsById;
    }
}
