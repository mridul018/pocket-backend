package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TS_TRANSACTION_LIMIT", schema = "PocketWallet")
public class TsLimit {
    private int id;
    private String limitCode;
    private String limitName;
    private double minAmount;
    private double maxAmount;
    private int dailyMaxNoOfTransaction;
    private double dailyMaxTransactionAmount;
    private int montlyMaxNoOfTransaction;
    private double monthlyMaxTransactionAmount;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "limit_code")
    public String getLimitCode() {
        return limitCode;
    }

    public void setLimitCode(String limitCode) {
        this.limitCode = limitCode;
    }

    @Basic
    @Column(name = "limit_name")
    public String getLimitName() {
        return limitName;
    }

    public void setLimitName(String limitName) {
        this.limitName = limitName;
    }

    @Basic
    @Column(name = "min_amount")
    public double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    @Basic
    @Column(name = "max_amount")
    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Basic
    @Column(name = "daily_max_no_of_transaction")
    public int getDailyMaxNoOfTransaction() {
        return dailyMaxNoOfTransaction;
    }

    public void setDailyMaxNoOfTransaction(int dailyMaxNoOfTransaction) {
        this.dailyMaxNoOfTransaction = dailyMaxNoOfTransaction;
    }

    @Basic
    @Column(name = "daily_max_transaction_amount")
    public double getDailyMaxTransactionAmount() {
        return dailyMaxTransactionAmount;
    }

    public void setDailyMaxTransactionAmount(double dailyMaxTransactionAmount) {
        this.dailyMaxTransactionAmount = dailyMaxTransactionAmount;
    }

    @Basic
    @Column(name = "montly_max_no_of_transaction")
    public int getMontlyMaxNoOfTransaction() {
        return montlyMaxNoOfTransaction;
    }

    public void setMontlyMaxNoOfTransaction(int montlyMaxNoOfTransaction) {
        this.montlyMaxNoOfTransaction = montlyMaxNoOfTransaction;
    }

    @Basic
    @Column(name = "monthly_max_transaction_amount")
    public double getMonthlyMaxTransactionAmount() {
        return monthlyMaxTransactionAmount;
    }

    public void setMonthlyMaxTransactionAmount(double monthlyMaxTransactionAmount) {
        this.monthlyMaxTransactionAmount = monthlyMaxTransactionAmount;
    }

}
