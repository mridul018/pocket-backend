package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "UM_USER_DETAILS", schema = "PocketWallet", catalog = "")
public class UmUserDetails {
    private int id;
    private String primaryIdNumber; // m
    private Date primaryIdIssueDate; // m
    private Date primaryIdExpiryDate; // m
    private Date primaryIdVerificationDate;
    private String givenName;
    private String familyName;
    private String primaryIdType; //m
    private String nationality; // m
    private String presentAddress;
    private String permanentAddress;
    private UmUserInfo umUserInfoByUserId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "primary_id_number")
    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    @Basic
    @Column(name = "primary_id_issue_date")
    public Date getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(Date primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    @Basic
    @Column(name = "primary_id_expiry_date")
    public Date getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(Date primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    @Basic
    @Column(name = "primary_id_verification_date")
    public Date getPrimaryIdVerificationDate() {
        return primaryIdVerificationDate;
    }

    public void setPrimaryIdVerificationDate(Date primaryIdVerificationDate) {
        this.primaryIdVerificationDate = primaryIdVerificationDate;
    }

    @Basic
    @Column(name = "given_name")
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @Basic
    @Column(name = "family_name")
    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Basic
    @Column(name = "primary_id_type")
    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    @Basic
    @Column(name = "nationality")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Basic
    @Column(name = "present_address")
    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    @Basic
    @Column(name = "permanent_address")
    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmUserDetails that = (UmUserDetails) o;
        return id == that.id &&
                Objects.equals(primaryIdNumber, that.primaryIdNumber) &&
                Objects.equals(primaryIdIssueDate, that.primaryIdIssueDate) &&
                Objects.equals(primaryIdExpiryDate, that.primaryIdExpiryDate) &&
                Objects.equals(primaryIdVerificationDate, that.primaryIdVerificationDate) &&
                Objects.equals(givenName, that.givenName) &&
                Objects.equals(familyName, that.familyName) &&
                Objects.equals(primaryIdType, that.primaryIdType) &&
                Objects.equals(nationality, that.nationality) &&
                Objects.equals(presentAddress, that.presentAddress) &&
                Objects.equals(permanentAddress, that.permanentAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, primaryIdNumber, primaryIdIssueDate, primaryIdExpiryDate, primaryIdVerificationDate, givenName, familyName, primaryIdType, nationality, presentAddress, permanentAddress);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }
}
