package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TS_FEE_PROFILE_LIMIT_MAPPING", schema = "PocketWallet")
public class TsFeeProfileLimitMapping {
    private int id;
    private String feeProfileCode;
    private String serviceCode;
    private String feeCode;
    private Date createdDate;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fee_profile_code")
    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }

    @Basic
    @Column(name = "service_code")
    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    @Basic
    @Column(name = "fee_code")
    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


}
