package tech.ipocket.pocket.entity;

import javax.annotation.Generated;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "TS_CLIENT_BALANCE", schema = "PocketWallet", catalog = "")
public class TsClientBalance {
    private int id;
    private BigDecimal balance;
    private TsClient tsClientByClientId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "balance")
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TsClientBalance that = (TsClientBalance) o;
        return id == that.id &&
                Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id", nullable = false)
    public TsClient getTsClientByClientId() {
        return tsClientByClientId;
    }

    public void setTsClientByClientId(TsClient tsClientByClientId) {
        this.tsClientByClientId = tsClientByClientId;
    }
}
