package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "UM_USER_GROUP", schema = "PocketWallet", catalog = "")
public class UmUserGroup {
    private int id;
    private String groupCode;
    private String groupName;
    private String description;
    private String groupStatus;
    private Timestamp createdDate;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "group_code")
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Basic
    @Column(name = "group_name")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "group_status")
    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    @Basic
    @Column(name = "created_date")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmUserGroup userGroup = (UmUserGroup) o;
        return id == userGroup.id &&
                Objects.equals(groupCode, userGroup.groupCode) &&
                Objects.equals(groupName, userGroup.groupName) &&
                Objects.equals(description, userGroup.description) &&
                Objects.equals(groupStatus, userGroup.groupStatus) &&
                Objects.equals(createdDate, userGroup.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, groupCode, groupName, description, groupStatus, createdDate);
    }
}
