package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TS_TRANSACTION_PROFILE_LIMIT_MAPPING", schema = "PocketWallet")
public class TsTransactionProfileLimitMapping {
    private int id;
    private String txProfileCode;
    private String serviceCode;
    private String limitCode;
    private Date createdDate;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tx_profile_code")
    public String getTxProfileCode() {
        return txProfileCode;
    }

    public void setTxProfileCode(String txProfileCode) {
        this.txProfileCode = txProfileCode;
    }

    @Basic
    @Column(name = "service_code")
    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    @Basic
    @Column(name = "limit_code")
    public String getLimitCode() {
        return limitCode;
    }

    public void setLimitCode(String limitCode) {
        this.limitCode = limitCode;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
