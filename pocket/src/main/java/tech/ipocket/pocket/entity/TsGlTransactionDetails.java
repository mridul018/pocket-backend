package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "TS_GL_TRANSACTION_DETAILS", schema = "PocketWallet")
public class TsGlTransactionDetails {
    private int id;
    private BigDecimal creditAmount;
    private BigDecimal debitCard;
    private String glCode;
    private String eodStatus;
    private TsTransaction tsTransactionByTransactionId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "credit_amount")
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "debit_card")
    public BigDecimal getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(BigDecimal debitCard) {
        this.debitCard = debitCard;
    }

    @Basic
    @Column(name = "gl_code")
    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }


    @Basic
    @Column(name = "eod_status")
    public String getEodStatus() {
        return eodStatus;
    }

    public void setEodStatus(String eodStatus) {
        this.eodStatus = eodStatus;
    }


    @ManyToOne
    @JoinColumn(name = "transaction_id", referencedColumnName = "id", nullable = false)
    public TsTransaction getTsTransactionByTransactionId() {
        return tsTransactionByTransactionId;
    }

    public void setTsTransactionByTransactionId(TsTransaction tsTransactionByTransactionId) {
        this.tsTransactionByTransactionId = tsTransactionByTransactionId;
    }
}
