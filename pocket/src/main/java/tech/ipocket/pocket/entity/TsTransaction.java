package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TS_TRANSACTION", schema = "PocketWallet")
public class TsTransaction {
    private int id;
    private String token;
    private BigDecimal transactionAmount;
    private String senderWallet;
    private String senderGl;
    private BigDecimal senderDebitAmount;
    private BigDecimal senderRunningBalance;
    private String receiverWallet;
    private String receiverGl;
    private BigDecimal receiverCreditAmount;
    private BigDecimal receiverRunningBalance;
    private String transactionType;
    private String transactionStatus;
    private Boolean isDisputable;
    private String feeCode;
    private String feePayer;
    private BigDecimal feeAmount;
    private String logicalSender;
    private String logicalReceiver;
    private String prevTransactionRef;
    private String eodStatus;
    private Date createdDate;
    private Date updatedDate;
    private Collection<TsGlTransactionDetails> tsGlTransactionDetailsById;
    private TsClient tsClientBySenderClientId;
    private TsClient tsClientByReceiverClientId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "transaction_amount")
    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    @Basic
    @Column(name = "sender_wallet")
    public String getSenderWallet() {
        return senderWallet;
    }

    public void setSenderWallet(String senderWallet) {
        this.senderWallet = senderWallet;
    }

    @Basic
    @Column(name = "sender_gl")
    public String getSenderGl() {
        return senderGl;
    }

    public void setSenderGl(String senderGl) {
        this.senderGl = senderGl;
    }

    @Basic
    @Column(name = "sender_debit_amount")
    public BigDecimal getSenderDebitAmount() {
        return senderDebitAmount;
    }

    public void setSenderDebitAmount(BigDecimal senderDebitAmount) {
        this.senderDebitAmount = senderDebitAmount;
    }

    @Basic
    @Column(name = "sender_running_balance")
    public BigDecimal getSenderRunningBalance() {
        return senderRunningBalance;
    }

    public void setSenderRunningBalance(BigDecimal senderRunningBalance) {
        this.senderRunningBalance = senderRunningBalance;
    }

    @Basic
    @Column(name = "receiver_wallet")
    public String getReceiverWallet() {
        return receiverWallet;
    }

    public void setReceiverWallet(String receiverWallet) {
        this.receiverWallet = receiverWallet;
    }

    @Basic
    @Column(name = "receiver_gl")
    public String getReceiverGl() {
        return receiverGl;
    }

    public void setReceiverGl(String receiverGl) {
        this.receiverGl = receiverGl;
    }

    @Basic
    @Column(name = "receiver_credit_amount")
    public BigDecimal getReceiverCreditAmount() {
        return receiverCreditAmount;
    }

    public void setReceiverCreditAmount(BigDecimal receiverCreditAmount) {
        this.receiverCreditAmount = receiverCreditAmount;
    }

    @Basic
    @Column(name = "receiver_running_balance")
    public BigDecimal getReceiverRunningBalance() {
        return receiverRunningBalance;
    }

    public void setReceiverRunningBalance(BigDecimal receiverRunningBalance) {
        this.receiverRunningBalance = receiverRunningBalance;
    }

    @Basic
    @Column(name = "transaction_type")
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Basic
    @Column(name = "transaction_status")
    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Basic
    @Column(name = "is_disputable")
    public Boolean getIsDisputable() {
        return isDisputable;
    }

    public void setIsDisputable(Boolean isDisputable) {
        this.isDisputable = isDisputable;
    }

    @Basic
    @Column(name = "fee_code")
    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    @Basic
    @Column(name = "fee_payer")
    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    @Basic
    @Column(name = "fee_amount")
    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    @Basic
    @Column(name = "logical_sender")
    public String getLogicalSender() {
        return logicalSender;
    }

    public void setLogicalSender(String logicalSender) {
        this.logicalSender = logicalSender;
    }

    @Basic
    @Column(name = "logical_receiver")
    public String getLogicalReceiver() {
        return logicalReceiver;
    }

    public void setLogicalReceiver(String logicalReceiver) {
        this.logicalReceiver = logicalReceiver;
    }

    @Basic
    @Column(name = "prev_transaction_ref")
    public String getPrevTransactionRef() {
        return prevTransactionRef;
    }

    public void setPrevTransactionRef(String prevTransactionRef) {
        this.prevTransactionRef = prevTransactionRef;
    }

    @Basic
    @Column(name = "eod_status")
    public String getEodStatus() {
        return eodStatus;
    }

    public void setEodStatus(String eodStatus) {
        this.eodStatus = eodStatus;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }


    @OneToMany(mappedBy = "tsTransactionByTransactionId")
    public Collection<TsGlTransactionDetails> getTsGlTransactionDetailsById() {
        return tsGlTransactionDetailsById;
    }

    public void setTsGlTransactionDetailsById(Collection<TsGlTransactionDetails> tsGlTransactionDetailsById) {
        this.tsGlTransactionDetailsById = tsGlTransactionDetailsById;
    }

    @ManyToOne
    @JoinColumn(name = "sender_client_id", referencedColumnName = "id", nullable = false)
    public TsClient getTsClientBySenderClientId() {
        return tsClientBySenderClientId;
    }

    public void setTsClientBySenderClientId(TsClient tsClientBySenderClientId) {
        this.tsClientBySenderClientId = tsClientBySenderClientId;
    }

    @ManyToOne
    @JoinColumn(name = "receiver_client_id", referencedColumnName = "id", nullable = false)
    public TsClient getTsClientByReceiverClientId() {
        return tsClientByReceiverClientId;
    }

    public void setTsClientByReceiverClientId(TsClient tsClientByReceiverClientId) {
        this.tsClientByReceiverClientId = tsClientByReceiverClientId;
    }
}
