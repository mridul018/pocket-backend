package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeFeeStakeholderMap;

import java.util.List;

public interface TsFeeFeeStakeholderMapRepository extends CrudRepository<TsFeeFeeStakeholderMap, Integer> {
    List<TsFeeFeeStakeholderMap> findAllByFeeProductCode(String feeCode);
}
