package tech.ipocket.pocket.repository.web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserGroup;

@Repository
public interface UserGroupRepository extends JpaRepository<UmUserGroup, Integer> {
    UmUserGroup findByGroupCode(String groupCode);
}
