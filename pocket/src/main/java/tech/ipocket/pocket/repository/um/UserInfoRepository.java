package tech.ipocket.pocket.repository.um;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.entity.UmUserInfo;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UmUserInfo, Integer> {
    UmUserInfo findByMobileNumber(String mobileNumber);

    UmUserInfo findByMobileNumberAndAccountStatusIn(String mobileNumber, List<Integer> statusIn);
    UmUserInfo findByLoginIdAndAccountStatusIn(String loginId, List<Integer> statusIn);

    UmUserInfo findByMobileNumberAndAccountStatusNotIn(String mobileNumber, List<Integer> statusNotIn);
    UmUserInfo findByMobileNumberOrEmail(String mobileNumber, String email);

    UmUserInfo findFirstByEmail(String email);

    List<UmUserInfo> findByLoginIdContaining(String loginId);

    List<UmUserInfo> findByFullNameContaining(String fullName);

    UmUserInfo findByLoginId(String loginId);

    UmUserInfo findFirstById(Integer credentialId);

    Page<UmUserInfo> findAllByGroupCode(String groupCode, Pageable pageRequest);
}
