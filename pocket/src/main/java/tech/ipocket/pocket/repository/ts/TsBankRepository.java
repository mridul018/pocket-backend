package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsBanks;

public interface TsBankRepository extends CrudRepository<TsBanks, Integer> {

    TsBanks findFirstByBankName(String bankName);

    TsBanks findFirstByGlCode(String bankGlCode);

    TsBanks findFirstByBankNameAndGlCode(String bankName, String bankGlCode);

    TsBanks findFirstByBankCode(String bankCode);
}
