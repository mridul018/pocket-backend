package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsGlTransactionDetails;

public interface TsGlTransactionDetailsRepository extends CrudRepository<TsGlTransactionDetails, Integer> {
}
