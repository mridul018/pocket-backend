package tech.ipocket.pocket.repository.ts;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.ITsTransaction;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsTransaction;

import java.sql.Date;
import java.util.List;

public interface TsTransactionRepository extends CrudRepository<TsTransaction, Integer> {
    List<TsTransaction> findByTransactionTypeAndTsClientBySenderClientIdAndCreatedDateBetween(String serviceCode, TsClient senderClient, Date startDate, Date endDate);

    List<TsTransaction> findByTransactionTypeAndTsClientByReceiverClientIdAndCreatedDateBetween(String serviceCode, TsClient receiverClient, Date startDate, Date endDate);


    @Query("select transaction from TsTransaction transaction" +
            " where  (transaction.receiverWallet=?1 OR transaction.senderWallet=?2 )")
    List<ITsTransaction> findAllByReceiverWalletOrSenderWallet(String receiverWallet, String senderWallet, Pageable pageRequest);


    @Query("select transaction from TsTransaction transaction" +
            " where  (transaction.receiverWallet=?1 OR transaction.senderWallet=?2 ) AND transaction.createdDate between ?3 AND ?4")
    List<ITsTransaction> findAllByReceiverWalletOrSenderWalletDateBetween(String receiverWallet, String senderWallet,
                                                                          Date fromDate, Date toDate, PageRequest pageRequest);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType in (?1) AND (transaction.receiverWallet=?2 OR transaction.senderWallet=?3 )")
    List<ITsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWallet(List<String> txTypes,
                                                                                   String receiverWallet, String senderWallet, Pageable pageRequest);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType in (?1) AND (transaction.receiverWallet=?2 OR transaction.senderWallet=?3 ) AND transaction.createdDate between ?4 AND ?5")
    List<ITsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWalletDateBetween(List<String> txTypes,
                                                                                   String receiverWallet, String senderWallet,
                                                                                              Date fromDate,Date toDate, Pageable pageRequest);

}
