package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmDocuments;
import tech.ipocket.pocket.entity.UmUserInfo;

@Repository
public interface UserDocumentRepository extends CrudRepository<UmDocuments, Integer> {
    UmDocuments findLastByUmUserInfoByUserId(UmUserInfo umUserInfo);
    UmDocuments findLastByUmUserInfoByUserIdAndDocumentType(UmUserInfo umUserInfo,String documentType);
}
