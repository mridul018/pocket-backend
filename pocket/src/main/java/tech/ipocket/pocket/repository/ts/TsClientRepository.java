package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.TsClient;

import java.util.List;

public interface TsClientRepository extends CrudRepository<TsClient, Integer> {

    Integer countAllByWalletNoAndUserStatusNotIn(String mobileNumber, List<String> clientStatus);

    TsClient findFirstByWalletNoAndUserStatusNotIn(String mobileNumber, List<String> clientStatus);

    TsClient findFirstByGroupCodeAndUserStatusNotIn(String groupCode, List<String> clientStatus);

    TsClient findByWalletNo(String walletNumber);
}
