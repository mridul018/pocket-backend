package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeStakeholder;

public interface TsFeeStakeholderRepository extends CrudRepository<TsFeeStakeholder, Integer> {
    TsFeeStakeholder findFirstByStakeholderName(String stakeholderName);

    TsFeeStakeholder findFirstById(int stakeHolderId);
}
