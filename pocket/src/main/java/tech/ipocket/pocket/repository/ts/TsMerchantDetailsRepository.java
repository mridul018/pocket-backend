package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsMerchantDetails;

public interface TsMerchantDetailsRepository extends CrudRepository<TsMerchantDetails, Integer> {
    TsMerchantDetails findFirstByShortCode(String shortCode);
}
