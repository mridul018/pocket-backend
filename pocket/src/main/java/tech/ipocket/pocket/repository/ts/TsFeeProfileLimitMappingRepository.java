package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeProfileLimitMapping;

public interface TsFeeProfileLimitMappingRepository extends CrudRepository<TsFeeProfileLimitMapping, Integer> {
    TsFeeProfileLimitMapping findFirstByFeeCodeAndServiceCodeAndFeeProfileCode(String feeCode, String serviceCode, String profileCode);

    TsFeeProfileLimitMapping findFirstByFeeProfileCodeAndServiceCode(String profileCode, String serviceCode);
}
