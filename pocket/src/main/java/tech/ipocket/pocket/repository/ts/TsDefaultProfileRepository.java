package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsDefaultProfile;

public interface TsDefaultProfileRepository extends CrudRepository<TsDefaultProfile, Integer> {
    TsDefaultProfile findFirstByGroupCode(String groupCode);
}
