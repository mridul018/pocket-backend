package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSession;

@Repository
public interface UserSessionRepository extends JpaRepository<UmUserSession, Integer> {

    //umUserInfoByUserId
    UmUserSession findByUmUserInfoByUserId(UmUserInfo userInfo);
}
