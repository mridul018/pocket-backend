package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsLimit;

import java.util.List;

public interface TsLimitRepository extends CrudRepository<TsLimit,Integer> {
    TsLimit findFirstByLimitCode(String limitCode);

    TsLimit findFirstByLimitName(String limitName);

    List<TsLimit> findAllByLimitCodeIn(List<String> codes);
}
