package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFee;
import tech.ipocket.pocket.entity.TsFeeProfile;
import tech.ipocket.pocket.entity.TsGl;

public interface TsGlRepository extends CrudRepository<TsGl, Integer> {
    TsGl findFirstByGlCode(String glCode);

    TsGl findFirstByGlName(String glName);
}
