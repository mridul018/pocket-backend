package tech.ipocket.pocket.repository.ts;


import tech.ipocket.pocket.utils.LogWriterUtility;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CustomQuery {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(CustomQuery.class);
    private EntityManager entityManager;

    public CustomQuery(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public BigDecimal getGlBalanceUsingGlCode(String glCode) {

        try {

            String queryString = "select coalesce( SUM(td.CREDIT_AMOUNT) , 0) as credit , coalesce( SUM(td.debit_card) , 0) as debit from TS_GL_TRANSACTION_DETAILS td\n" +
                    "    where td.GL_CODE=?1 ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, glCode);

            Object[] objectList = (Object[]) query.getSingleResult();

            BigDecimal creditAmount = ((BigDecimal) objectList[0]);
            BigDecimal debitAmount = ((BigDecimal) objectList[1]);

            BigDecimal balance = creditAmount.subtract(debitAmount);

            return balance == null ? BigDecimal.ZERO : balance;
        } catch (NoResultException e) {
            return BigDecimal.valueOf(0);
        }
    }

    public BigDecimal getGlAggregateBalanceUsingGlCodes(List<Long> glCodeList, String productCode, String requestid) {

        try {


            if (glCodeList == null || glCodeList.size() == 0) {
                logWriterUtility.trace(requestid, "Gl code list empty");
                return BigDecimal.ZERO;
            }

            List<Long> distinctGlCodes = glCodeList.stream().distinct().collect(Collectors.toList());

            String queryString = "select coalesce( SUM(td.CREDIT_AMOUNT) , 0) as credit , coalesce( SUM(td.DEBIT_AMOUNT) , 0) as debit from CFE_GL_TRANSACTION_DETAILS td\n" +
                    "    where td.GL_CODE IN (?1) AND td.PRODUCT_CODE=?2";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, distinctGlCodes);
            query.setParameter(2, productCode);

            List<Object[]> resultList = query.getResultList();

            BigDecimal netBalance = BigDecimal.ZERO;

            for (int index = 0; index < resultList.size(); index++) {
                Object[] bankDebitCreditBalance = resultList.get(index);

                BigDecimal creditAmount = ((BigDecimal) bankDebitCreditBalance[0]);
                BigDecimal debitAmount = ((BigDecimal) bankDebitCreditBalance[1]);

                BigDecimal netBalanceTemp = creditAmount.subtract(debitAmount);
                netBalance = netBalance.add(netBalanceTemp);
            }

            return netBalance;
        } catch (NoResultException e) {
            logWriterUtility.error(requestid, "Error :" + e.getMessage());
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getGlBalanceUsingGlCodeAndDateRange(Long glCode, String productCode, Date startDate, Date endDate) {

        try {

            String queryString;


            Query query;
            if (startDate == null) {
                queryString = "SELECT COALESCE( SUM(gltd.CREDIT_AMOUNT) , 0) as credit , COALESCE( SUM(gltd.DEBIT_AMOUNT) , 0) as debit \n" +
                        "from CFE_GL_TRANSACTION_DETAILS gltd " +
                        "left join CFE_TRANSACTION transaction on transaction.ID=gltd.transaction_id  " +
                        "where gltd.GL_CODE=?1 AND gltd.PRODUCT_CODE=?2 " +
                        "AND transaction.transaction_date <=?3 ";


                query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, glCode);
                query.setParameter(2, productCode);
                query.setParameter(3, endDate);

            } else {
                queryString = "SELECT COALESCE( SUM(gltd.CREDIT_AMOUNT) , 0) as credit , COALESCE( SUM(gltd.DEBIT_AMOUNT) , 0) as debit \n" +
                        "from CFE_GL_TRANSACTION_DETAILS gltd " +
                        "left join CFE_TRANSACTION transaction on transaction.ID=gltd.transaction_id  " +
                        "where gltd.GL_CODE=?1 AND gltd.PRODUCT_CODE=?2 AND transaction.transaction_date >=?3  " +
                        "AND transaction.transaction_date <=?4 ";


                query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, glCode);
                query.setParameter(2, productCode);
                query.setParameter(3, startDate);
                query.setParameter(4, endDate);
            }

            Object[] objectList = (Object[]) query.getSingleResult();

            BigDecimal creditAmount = ((BigDecimal) objectList[0]);
            BigDecimal debitAmount = ((BigDecimal) objectList[1]);


            BigDecimal netBalance = creditAmount.subtract(debitAmount);
            return netBalance;
        } catch (NoResultException e) {
            return BigDecimal.valueOf(0);
        }
    }

    /*public List<GlStatementListItemData> getTransactionsUsingGlCodeAndDateRange(Long glCode, String productCode, Date startDate,
                                                                                Date endDate, DmoneyService cfeServicesService) {

        try {

            String queryString = "SELECT transaction.TOKEN_NO, transaction.TRANSACTION_TYPE , " +
                    "transaction.transaction_date , gltd.CREDIT_AMOUNT , gltd.DEBIT_AMOUNT , transaction.ID " +
                    "from CFE_GL_TRANSACTION_DETAILS gltd " +
                    "left join CFE_TRANSACTION transaction ON gltd.transaction_id=transaction.ID " +
                    "where gltd.GL_CODE= ?1 AND gltd.PRODUCT_CODE=?2 AND (transaction.transaction_date >= ?3" +
                    " AND transaction.transaction_date <= ?4)  order by TRANSACTION .ID ASC ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, glCode);
            query.setParameter(2, productCode);
            query.setParameter(3, startDate);
            query.setParameter(4, endDate);

            logWriterUtility.trace("", "Query :" + queryString);
            logWriterUtility.trace("", "Data :" + startDate + " " + endDate);

            List<GlStatementListItemData> data = parseGlStatementListData(query.getResultList(), productCode, cfeServicesService);

            return data;
        } catch (NoResultException e) {
            return null;
        }

    }

    private List<Long> parseGlList(List<Object> resultList) {
        List<Long> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (int i = 0; i < resultList.size(); i++) {

            BigDecimal data = (BigDecimal) resultList.get(i);

            listItemData.add(data.longValue());
        }

        return listItemData;
    }

    private List<GlStatementListItemData> parseGlStatementListData(List<Object[]> resultList, String productCode, DmoneyService cfeServicesService) {
        List<GlStatementListItemData> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (Object[] data : resultList) {
            GlStatementListItemData itemData = new GlStatementListItemData(data, productCode, cfeServicesService);
            listItemData.add(itemData);
        }

        return listItemData;
    }

    public BigDecimal findBeforeTxOfDateRange(String mobileNumber, Long transactionId, String productCode, String requestId) {
        String queryString = "select id,AMOUNT,PAYER_MOBILE_NUMBER,PAYEE_MOBILE_NUMBER,FEE_AMOUNT,FEE_PAYER,transaction_date,transaction_status,transaction_type,WITHDRAWER_RUNNING_BALANCE,DEPOSITOR_RUNNING_BALANCE" +
                " from CFE_TRANSACTION transaction\n" +
                "where  transaction.id<?2 AND (transaction.PAYER_MOBILE_NUMBER=?1 or transaction.PAYEE_MOBILE_NUMBER=?1)  AND transaction.PRODUCT_CODE=?3\n" +
                "order by transaction.id desc OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";

        Query query = entityManager.createNativeQuery(queryString);
        query.setParameter(1, mobileNumber);
        query.setParameter(2, transactionId);
        query.setParameter(3, productCode);

        Object[] objectList;
        try {
            objectList = (Object[]) query.getSingleResult();
            if (objectList == null || objectList.length == 0) {
                logWriterUtility.trace(requestId, "No transaction found");
                return BigDecimal.valueOf(0);
            }

            CfeTransaction cfeTransaction = getTransaction(objectList);

            boolean isUserDepositor = mobileNumber.equals(cfeTransaction.getPayeeMobileNumber());
            if (isUserDepositor) {
                return cfeTransaction.getDepositorRunningBalance();
            } else {
                return cfeTransaction.getWithdrawerRunningBalance();
            }
        } catch (Exception e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            return new BigDecimal(0);
        }


    }

    public List<Long> getDistinctGlCodesUsingGlNameLike(String searchQuery, String productCode, String requestId) {

        try {

            String queryString = "SELECT DISTINCT GL_CODE from CFE_GL \n" +
                    "where GL_NAME LIKE ?1 AND PRODUCT_CODE=?2 ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, searchQuery);
            query.setParameter(2, productCode);

            List<Long> glCodes = parseGlList(query.getResultList());

            return glCodes;
        } catch (NoResultException e) {
            logWriterUtility.error(requestId, "No result found :" + e.getLocalizedMessage());
            return null;
        }

    }

    public List<String> getTransactionDetails(int minCount, java.sql.Date startDate, java.sql.Date endDate, String productCode, String requestId) {

        List<String> userList = new ArrayList<>();
        try {

            String searchQuery = "SELECT * FROM ( " +
                    "    SELECT tx.PAYER_MOBILE_NUMBER,COUNT(tx.PAYER_MOBILE_NUMBER) as count FROM CFE_TRANSACTION tx where tx.product_code=?1 AND tx.CREATED_DATE >= ?2 AND tx.CREATED_DATE <= ?3 GROUP BY PAYER_MOBILE_NUMBER\n" +
                    "    UNION ALL\n" +
                    "    SELECT tx.PAYEE_MOBILE_NUMBER ,COUNT(tx.PAYEE_MOBILE_NUMBER) as count FROM CFE_TRANSACTION tx where tx.product_code=?1 AND tx.CREATED_DATE >= ?2 AND tx.CREATED_DATE <= ?3 GROUP BY tx.PAYEE_MOBILE_NUMBER\n" +
                    "    \n" +
                    " ) tx ";

            Query query = entityManager.createNativeQuery(searchQuery);
            query.setParameter(1, productCode);
            query.setParameter(2, startDate);
            query.setParameter(3, endDate);

            userList = parseTxList(query.getResultList(), minCount);

        } catch (Exception e) {
            logWriterUtility.error(requestId, e);
        }

        return userList;
    }

    private List<String> parseTxList(List<Object[]> resultList, int minCount) {

        if (resultList == null || resultList.size() == 0) {
            return null;
        }

        HashMap<String, BigDecimal> dataSet = new HashMap<>();

        for (Object[] tempData : resultList) {

            if (tempData[0] == null) {
                continue;
            }

            String mobileNumber = (String) tempData[0];
            BigDecimal count = (BigDecimal) tempData[1];

            if (dataSet.size() == 0) {
                dataSet.put(mobileNumber, count);
                continue;
            }

            if (!dataSet.containsKey(mobileNumber)) {
                dataSet.put(mobileNumber, count);
            } else {
                BigDecimal prevValue = dataSet.get(mobileNumber);
                dataSet.put(mobileNumber, (prevValue.add(count)));
            }
        }

        List<String> finalResult = new ArrayList<>();

        dataSet.forEach((key, value) -> {
            if (value.intValue() >= minCount) {
                finalResult.add(key);
            }
        });

        return finalResult;
    }

    private CfeTransaction getTransaction(Object[] objectList) {
        try {
            CfeTransaction cfeTransaction = new CfeTransaction();

            BigDecimal id = (BigDecimal) objectList[0];

            cfeTransaction.setId(id.longValueExact());
            cfeTransaction.setAmount((BigDecimal) objectList[1]);
            cfeTransaction.setPayerMobileNumber((String) objectList[2]);
            cfeTransaction.setPayeeMobileNumber((String) objectList[3]);
            cfeTransaction.setFeeAmount((BigDecimal) objectList[4]);
            cfeTransaction.setFeePayer((String) objectList[5]);
            cfeTransaction.setTransactionDate((Date) objectList[6]);
            cfeTransaction.setTransactionStatus((String) objectList[7]);
            cfeTransaction.setTransactionType((String) objectList[8]);
            cfeTransaction.setWithdrawerRunningBalance((BigDecimal) objectList[9]);
            cfeTransaction.setDepositorRunningBalance((BigDecimal) objectList[10]);
            return cfeTransaction;
        } catch (Exception e) {
            return null;
        }
    }

    public String isDbConnected(String requestId) {

        try {

            String queryString = "select ?1 from dual ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, "hello");

            List<Object[]> resultList = query.getResultList();

            if (resultList == null || resultList.size() == 0) {
                return "disconnected";
            }

            return "connected";
        } catch (NoResultException e) {
            logWriterUtility.error(requestId, "No result found :" + e.getLocalizedMessage());
            return "disconnected";

        }

    }*/


}