package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSettings;

@Repository
public interface UserSettingsRepository extends JpaRepository<UmUserSettings, Integer> {
    UmUserSettings findFirstByUmUserInfoByUserId(UmUserInfo umUserInfo);
}
