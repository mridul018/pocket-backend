package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeProfile;

public interface TsFeeProfileRepository extends CrudRepository<TsFeeProfile, Integer> {
    TsFeeProfile findFirstByProfileCode(String profileCode);

    TsFeeProfile findFirstByProfileName(String profileName);
}
