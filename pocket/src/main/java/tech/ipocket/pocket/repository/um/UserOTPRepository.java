package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmOtp;

@Repository
public interface UserOTPRepository extends JpaRepository<UmOtp, Integer> {
    UmOtp findFirstByOtpTextAndOtpSecret(String otpText, String otpSecret);

    UmOtp findFirstByOtpSecretAndStatus(String otpSecret, String otpStatus);
}
