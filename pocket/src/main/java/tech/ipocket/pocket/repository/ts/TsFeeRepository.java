package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFee;

public interface TsFeeRepository extends CrudRepository<TsFee, Integer> {
    TsFee findFirstById(Integer feeId);
    TsFee findFirstByFeeCode(String feeCode);

    TsFee findFirstByFeeName(String feeName);
}
