package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsTransactionProfile;

public interface TsTransactionProfileRepository extends CrudRepository<TsTransactionProfile, Integer> {

    TsTransactionProfile findFirstByProfileCode(String profileCode);

    TsTransactionProfile findFirstByProfileName(String profileName);
}
