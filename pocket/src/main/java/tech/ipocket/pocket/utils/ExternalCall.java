package tech.ipocket.pocket.utils;

import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.response.BaseResponseObject;

import javax.validation.constraints.NotNull;

public class ExternalCall extends Thread {

    LogWriterUtility log = new LogWriterUtility(ExternalCall.class);


    private String to;
    private String subject;
    private String body;
    private String type;
    private RestTemplate restTemplate;
    private String requestId;
    private boolean callToExternal;

    public ExternalCall(String to, String subject, String body, String type, RestTemplate restTemplate, String requestId, boolean callToExternal) {
        this.to = to;
        this.subject = subject;
        this.body = body;
        this.type = type;
        this.restTemplate = restTemplate;
        this.requestId = requestId;
        this.callToExternal = callToExternal;
    }

    @Override
    public void run() {
        if(!callToExternal){
            return;
        }

        try{
            String url = "http://pocket-external/external/services";

            ExternalRequest request = new ExternalRequest();
            request.setTo(this.to);
            request.setBody(this.body);
            request.setSubject(this.subject);
            request.setType(this.type);

            BaseResponseObject responseObject = restTemplate.postForObject(url, request, BaseResponseObject.class);

            if(responseObject == null){
                log.error(this.requestId, "External service not executed");
            } else if (responseObject != null && responseObject.getError() != null) {
                log.error(this.requestId, responseObject.getError().getErrorMessage());
            } else {
                log.info(this.requestId, "External service is executed");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
