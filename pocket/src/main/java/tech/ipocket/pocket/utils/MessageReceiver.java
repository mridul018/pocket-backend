package tech.ipocket.pocket.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageReceiver {

    Locale locale = LocaleContextHolder.getLocale();

    @Autowired
    private MessageSource messageSource;


    public String getMessage(String errorCode) {
        return messageSource.getMessage(errorCode, null, "An unexpected error occurred", locale);
    }

    public String getLocalizedMessage(String messageKey, String... args) {

        if (messageKey == null || messageKey.length() <= 0) {
            messageKey = messageSource.getMessage(PocketErrorCode.UnspecifiedErrorOccured.getKeyString(), args, LocaleContextHolder.getLocale());
        } else {
            try {
                messageKey = messageSource.getMessage(messageKey, args, LocaleContextHolder.getLocale()); // ex("how.are.you")
            } catch (Exception e) {
                if (messageKey.length() != 0) {
                    messageKey = messageKey; // ex("How are you?")
                } else {
                    messageKey = messageSource.getMessage(PocketErrorCode.UnspecifiedErrorOccured.getKeyString(), null, LocaleContextHolder.getLocale()); // ex()
                }
            }
        }

        return messageKey;
    }

}
