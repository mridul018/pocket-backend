package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.notification.NotificationRequest;

import java.math.BigDecimal;

public class NotificationBuilder {

    public NotificationRequest buildNotificationRequest(BigDecimal transactionAmount, String receiverWallet,
                                                        String senderWalletNo, String transactionToken, String serviceCode,
                                                        String feePayer, BigDecimal feeAmount, String request, String response,
                                                        String requestId, String notes) {

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setTransactionAmount(transactionAmount);
        notificationRequest.setReceiverWallet(receiverWallet);
        notificationRequest.setSenderWalletNo(senderWalletNo);
        notificationRequest.setTransactionToken(transactionToken);

        notificationRequest.setFeeAmount(feeAmount);
        notificationRequest.setFeePayer(feePayer);

        notificationRequest.setServiceCode(serviceCode);
        notificationRequest.setNotes(notes);

        notificationRequest.setRequest(requestId);
        notificationRequest.setRequest(request);
        notificationRequest.setResponse(response);

        return notificationRequest;
    }

}
