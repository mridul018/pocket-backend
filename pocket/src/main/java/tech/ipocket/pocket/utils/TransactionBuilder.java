package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.TsGlRepository;
import tech.ipocket.pocket.request.ts.settlement.AdminCashInToCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.MasterDepositFromBankGlRequest;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionBuilder {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    public TsTransaction buildTransaction(TsClient receiverClient, MasterDepositFromBankGlRequest request,
                                          String serviceCode, FeeData feeData,
                                          String walletNo,
                                          TransactionAmountData transactionAmountData, String senderGlCode) {


        TsTransaction tsTransaction = new TsTransaction();
        tsTransaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        tsTransaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        tsTransaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        tsTransaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        tsTransaction.setFeeCode(feeData.getFeeCode());
        tsTransaction.setFeePayer(feeData.getFeePayer());

        tsTransaction.setCreatedDate(new Date());
        tsTransaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        tsTransaction.setIsDisputable(false);

        tsTransaction.setLogicalReceiver(walletNo);
        tsTransaction.setLogicalSender(request.getBankCode());

        tsTransaction.setSenderWallet(null);
        tsTransaction.setReceiverWallet(walletNo);

        tsTransaction.setSenderGl(senderGlCode);
        tsTransaction.setReceiverGl(null);

        tsTransaction.setPrevTransactionRef(null);
        tsTransaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
        tsTransaction.setTransactionType(serviceCode);
        tsTransaction.setTsClientBySenderClientId(null);
        tsTransaction.setTsClientByReceiverClientId(receiverClient);


        return tsTransaction;
    }

    public TsTransaction buildTransaction(TsClient senderClient, TsClient receiverClient, AdminCashInToCustomerRequest request,
                                          String serviceCode, FeeData feeData,
                                          TransactionAmountData transactionAmountData) {


        TsTransaction tsTransaction = new TsTransaction();
        tsTransaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        tsTransaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        tsTransaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        tsTransaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        tsTransaction.setFeeCode(feeData.getFeeCode());
        tsTransaction.setFeePayer(feeData.getFeePayer());

        tsTransaction.setCreatedDate(new Date());
        tsTransaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        tsTransaction.setIsDisputable(false);

        tsTransaction.setLogicalReceiver(receiverClient.getWalletNo());
        tsTransaction.setLogicalSender(senderClient.getWalletNo());

        tsTransaction.setSenderWallet(senderClient.getWalletNo());
        tsTransaction.setReceiverWallet(receiverClient.getWalletNo());

        tsTransaction.setSenderGl(null);
        tsTransaction.setReceiverGl(null);

        tsTransaction.setPrevTransactionRef(null);
        tsTransaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
        tsTransaction.setTransactionType(serviceCode);
        tsTransaction.setTsClientBySenderClientId(senderClient);
        tsTransaction.setTsClientByReceiverClientId(receiverClient);


        return tsTransaction;
    }

    public List<TsTransactionDetails> buildTransactionDetail(
            TsTransaction cfeTransaction,
            String receiverWalletRef, String senderWalletRef,
            TransactionAmountData transactionAmountData) {

        List<TsTransactionDetails> transactionDetailsList = new ArrayList<>();

        TsTransactionDetails tsReceiverTransactionDetails = new TsTransactionDetails();
        tsReceiverTransactionDetails.setTransactionAmount(transactionAmountData.getReceiverCreditAmount());
        tsReceiverTransactionDetails.setTransactionType(TsEnums.TransactionDrCrType.CREDIT.getTransactionDrCrType());
        tsReceiverTransactionDetails.setTransactionId(cfeTransaction.getId());
        tsReceiverTransactionDetails.setWalletReference(receiverWalletRef);
        transactionDetailsList.add(tsReceiverTransactionDetails);


        TsTransactionDetails tsSenderTransactionDetails = new TsTransactionDetails();
        tsSenderTransactionDetails.setTransactionAmount(transactionAmountData.getReceiverCreditAmount());
        tsSenderTransactionDetails.setTransactionType(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        tsSenderTransactionDetails.setTransactionId(cfeTransaction.getId());
        tsSenderTransactionDetails.setWalletReference(senderWalletRef);
        transactionDetailsList.add(tsSenderTransactionDetails);


        TsTransactionDetails tsFeeTransactionDetails = new TsTransactionDetails();
        tsFeeTransactionDetails.setTransactionAmount(transactionAmountData.getTotalFeeAmount());
        tsFeeTransactionDetails.setTransactionType(TsEnums.TransactionDrCrType.CREDIT.getTransactionDrCrType());
        tsFeeTransactionDetails.setTransactionId(cfeTransaction.getId());
        tsFeeTransactionDetails.setWalletReference("FEE");
        transactionDetailsList.add(tsFeeTransactionDetails);


        return transactionDetailsList;
    }

    public TsGlTransactionDetails buildGlTransactionDetailsItem(TsTransaction cfeTransaction, String glName,
                                                                BigDecimal debitAmount,
                                                                BigDecimal creditAmount, String requestId, TsGlRepository tsGlRepository) throws PocketException {

        TsGlTransactionDetails tsGlTransactionDetails = new TsGlTransactionDetails();
        tsGlTransactionDetails.setCreditAmount(creditAmount);
        tsGlTransactionDetails.setDebitCard(debitAmount);
        tsGlTransactionDetails.setTsTransactionByTransactionId(cfeTransaction);


        TsGl tsGl = tsGlRepository.findFirstByGlName(glName);

        if (tsGl == null) {
            logWriterUtility.error(requestId, "Gl not found :" + glName);
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }
        tsGlTransactionDetails.setGlCode(tsGl.getGlCode());


        return tsGlTransactionDetails;
    }

    public TsTransaction buildTransaction(TsClient clientSender,
                                          TsClient clientReceiver, String serviceCode, FeeData feeData,
                                          TransactionAmountData transactionAmountData) {
        TsTransaction tsTransaction = new TsTransaction();
        tsTransaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        tsTransaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        tsTransaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        tsTransaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        tsTransaction.setFeeCode(feeData.getFeeCode());
        tsTransaction.setFeePayer(feeData.getFeePayer());

        tsTransaction.setCreatedDate(DateUtil.currentDate());
        tsTransaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        tsTransaction.setIsDisputable(true);

        tsTransaction.setLogicalReceiver(clientReceiver.getWalletNo());
        tsTransaction.setLogicalSender(clientSender.getWalletNo());

        tsTransaction.setSenderWallet(clientSender.getWalletNo());
        tsTransaction.setReceiverWallet(clientReceiver.getWalletNo());

        tsTransaction.setSenderGl(null);
        tsTransaction.setReceiverGl(null);

        tsTransaction.setPrevTransactionRef(null);
        tsTransaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
        tsTransaction.setTransactionType(serviceCode);
        tsTransaction.setTsClientBySenderClientId(clientSender);
        tsTransaction.setTsClientByReceiverClientId(clientReceiver);


        return tsTransaction;
    }

    public TsTransactionDetails buildCfeTransactionDetailsItem(int transactionId, String payerType, String walletRef, BigDecimal amount) {


        TsTransactionDetails tsTransactionDetails = new TsTransactionDetails();
        tsTransactionDetails.setTransactionAmount(amount);
        tsTransactionDetails.setTransactionType(payerType);
        tsTransactionDetails.setTransactionId(transactionId);
        tsTransactionDetails.setWalletReference(walletRef);
        return tsTransactionDetails;
    }

    public TsGlTransactionDetails buildGlTransactionDetailsItem(TsTransaction tsTransaction, String glName,
                                                                BigDecimal debitAmount, BigDecimal creditAmount,
                                                                String requestId,
                                                                boolean isPostingAllowed, TsGlRepository tsGlRepository) throws PocketException {
        TsGlTransactionDetails tsGlTransactionDetails = new TsGlTransactionDetails();
        tsGlTransactionDetails.setCreditAmount(creditAmount);
        tsGlTransactionDetails.setDebitCard(debitAmount);
        tsGlTransactionDetails.setTsTransactionByTransactionId(tsTransaction);


        TsGl tsGl = tsGlRepository.findFirstByGlName(glName);

        if (tsGl == null) {
            logWriterUtility.error(requestId, "Gl not found :" + glName);
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }
        tsGlTransactionDetails.setGlCode(tsGl.getGlCode());


        return tsGlTransactionDetails;
    }
}
