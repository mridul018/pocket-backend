package tech.ipocket.pocket.utils;

public class PocketException extends Exception {
    private String errorCode;
    private String message;
    private String requestId;
    private Integer statusCode;
    private String[] args;

    public PocketException(String errorCode) {
        this.errorCode = errorCode;
    }

    public PocketException(PocketErrorCode invalidLoginCredential) {
        errorCode = "" + invalidLoginCredential.getErrorCode();
    }

    public PocketException(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.message = errorMessage;
    }

    public PocketException(PocketErrorCode errorCode, String errorMessage, String... args) {
        this.errorCode = "" + errorCode.getErrorCode();
        this.message = errorMessage;
        this.args = args;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String... args) {
        this.args = args;
    }
}
