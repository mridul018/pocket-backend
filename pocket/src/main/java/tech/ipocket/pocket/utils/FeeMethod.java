package tech.ipocket.pocket.utils;

public class FeeMethod {
    public static final String PERCENTAGE = "1";
    public static final String FIXED = "2";
    public static final String BOTH = "3";
}
