package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;

public class BaseEndpoint {

    protected void validateRequest(BaseRequestObject request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
        if (request.getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }

        request.validate(baseResponseObject);
        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }

}
