package tech.ipocket.pocket.utils;



import java.util.Base64;

public class SecuritySettings {
    //public static final String SECRET = Base64.getEncoder().encodeToString(MacProvider.generateKey(SignatureAlgorithm.HS512).getEncoded());
    //public static final String SECRET = "SecretKeyForJWTToken-DemoApplication";
    public static final long EXPIRATION_TIME = 60 * 60 * 1000; // 60 minutes
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

}
