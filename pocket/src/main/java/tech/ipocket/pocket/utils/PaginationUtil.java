package tech.ipocket.pocket.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class PaginationUtil {



    /*PageRequest pageRequest = PaginationUtil.makePageRequest(request.getPageId(), request.getNumberOfItemPerPage(),
            Sort.Direction.DESC, "id");*/

    public static PageRequest makePageRequest(int pageId, int pageSize, Sort.Direction direction, String... properties) {
        return pageSize > 0 ? PageRequest.of(pageId, pageSize, direction, properties) : null;
    }
}