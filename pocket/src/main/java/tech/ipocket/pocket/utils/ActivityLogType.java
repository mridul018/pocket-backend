package tech.ipocket.pocket.utils;

public enum ActivityLogType {
    CUSTOMER_REGISTRATION(1),
    MERCHANT_OR_AGENT_REGISTRATION(2),
    ADMIN_REGISTRATION(3),
    CUSTOMER_OTP_VERIFICATION_AFTER_REGISTRATION(4),
    CUSTOMER_LOGIN(5),
    MERCHANT_AGENT_LOGIN(6),
    ADMIN_LOGIN(7);

    private final Integer CODE;

    ActivityLogType(Integer CODE) {
        this.CODE = CODE;
    }

    public Integer getCODE() {
        return this.CODE;
    }

    public String getKeyString() {
        return String.valueOf(this.CODE);
    }
}
