package tech.ipocket.pocket.utils;

public class TransactionTypes {
    public static final String FUND_TRANSFER = "Fund_Transfer";
    public static final String MERCHANT_PAYMENT = "Merchant_Payment";
    public static final String TOP_UP = "Top_Up";
}
