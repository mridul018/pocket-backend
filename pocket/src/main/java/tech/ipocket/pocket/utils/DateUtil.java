package tech.ipocket.pocket.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date addDay(Date date, int numberOfDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numberOfDay);
        return cal.getTime();
    }

    public static java.sql.Date addDay(java.sql.Date date, int numberOfDay) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numberOfDay);
        return new java.sql.Date(cal.getTimeInMillis());
    }

    public static Date today() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        return today.getTime();
    }

    public static java.sql.Date dayBeforeStartOfDay(int day) {
        java.sql.Date date = java.sql.Date.valueOf(LocalDate.now().minusDays(1));
        return date;
    }

    public static java.sql.Date dayBeforeEndOfDay(int day) {
        java.sql.Date date = java.sql.Date.valueOf(LocalDate.now().minusDays(day - 1));
        return date;
    }

    public static java.sql.Date startOfDay(LocalDate reportDate) {
        java.sql.Date date = java.sql.Date.valueOf(reportDate);
        return date;
    }

    public static java.sql.Date endOfDay(LocalDate reportDate) {
        java.sql.Date date = java.sql.Date.valueOf(reportDate.plusDays(1));
        return date;
    }

    public static java.sql.Date startOfDay() {
        Long time = new java.sql.Date(System.currentTimeMillis()).getTime();
        return new java.sql.Date(time - time % (24 * 60 * 60 * 1000));
    }

    public static java.sql.Date endOfDay() {
        Long time = new java.sql.Date(System.currentTimeMillis()).getTime();
        java.sql.Date today = new java.sql.Date(time - time % (24 * 60 * 60 * 1000));
        return new java.sql.Date(today.getTime() + 24 * 60 * 60 * 1000);
    }

    public static java.sql.Date currentDate() {
        java.sql.Date currentDate = new java.sql.Date(System.currentTimeMillis());
        return currentDate;
    }

    public static String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");
        return (dateOnly.format(cal.getTime()));
    }

    public static java.sql.Date parseDate(String date) throws PocketException {

        if (date.length() != 10) {
            throw new PocketException(PocketErrorCode.DateFormatException);
        }
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH);

            LocalDate parsedDate = LocalDate.parse(date, formatter);

            return java.sql.Date.valueOf(parsedDate);
        } catch (Exception e) {
            throw new PocketException(PocketErrorCode.InvalidDateFormat);
        }
    }

    public static LocalDate parseDateToLocalDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        formatter = formatter.withLocale(Locale.ENGLISH);
        LocalDate dt = LocalDate.parse(date, formatter);
        return dt;
    }

    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String convertDate(Date date) {

        if (date == null) {
            return "";
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String parseDate(java.sql.Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String getCurrentDateTimeWithMonthName() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDateTimeForNotifiction() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public static java.sql.Date startOfCurrentMonth() {
        LocalDate startOfMonth = LocalDate.now().withDayOfMonth(1);
        return DateUtil.startOfDay(startOfMonth);
    }

    public static String parseToReportDate(java.sql.Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(date);
    }

}

