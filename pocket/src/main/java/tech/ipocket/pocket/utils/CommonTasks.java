package tech.ipocket.pocket.utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.GsonBuilder;

public class CommonTasks {
    private static LogWriterUtility logWriterUtility = new LogWriterUtility(CommonTasks.class);

    public static String getObjectToString(Object o) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(o);
    }

    public static void printObjectByRequestId(Object request, String requestId) {
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(request);
            logWriterUtility.trace(requestId, "Response : \n" + json);
        } catch (Exception e) {
            logWriterUtility.trace(requestId, "Data parsing failed.");
        }
    }
}