package tech.ipocket.pocket.utils;

public enum AccountStatus {
    INITIATION(1),
    VERIFIED(2),
    BLOCKED(3),
    HARD_BLOCKED(4),
    DELETED(5);

    private final Integer CODE;

    AccountStatus(Integer CODE) {
        this.CODE = CODE;
    }

    public Integer getCODE() {
        return this.CODE;
    }

    public String getKeyString() {
        return String.valueOf(CODE);
    }
}
