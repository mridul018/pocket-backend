package tech.ipocket.pocket.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogWriterUtility {

    Logger log;

    public LogWriterUtility(Class<?> clazz) {
        log = LogManager.getLogger(clazz);
    }

    public void trace(String requestId, String message) {
        log.trace("REQUESTID:" + requestId + ":" + message);
    }

    public void debug(String requestId, String message) {
        log.debug("REQUESTID:" + requestId + ":" + message);
    }

    public void error(String requestId, String message) {
        log.error("REQUESTID:" + requestId + ":" + message);
    }

    public void error(String requestId, Throwable t) {
        log.error("REQUESTID:" + requestId + ":");
        log.error(t);
    }

    public void info(String requestId, String message) {
        log.info("REQUESTID:" + requestId + ":" + message);
    }

    public void error(String requestId, Exception exception) {
        log.warn("REQUESTID:" + requestId + ":" + exception.getStackTrace());
    }

    public void errorWithAnalysis(String requestId, Exception exception) {

        if (exception == null)
            return;
        String message = "No Message on error";
        StackTraceElement[] stackTrace = exception.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            message = "";
            for (StackTraceElement e : stackTrace) {
                message += "\n" + e.toString();
            }
        }
        log.error("REQUESTID:" + requestId + ":" + message);


    }

    public void warn(String requestId, String message) {
        log.error(requestId, message);
    }

    public boolean isTraceEnabled() {
        return log.isTraceEnabled();
    }

}