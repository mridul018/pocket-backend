package tech.ipocket.pocket.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;

import java.util.concurrent.ExecutionException;


@Component
@ControllerAdvice
@RequestMapping(produces = "application/json")
public class GlobalExceptionHandler {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());


    @Autowired
    private MessageReceiver messageReceiver;
    private ObjectMapper mapper;

    @ExceptionHandler({Exception.class})
    public ResponseEntity<BaseResponseObject> genericExceptionBlankHandler(Exception throwable) {

        PocketException cfeException;
        if (throwable instanceof ExecutionException) {
            throwable = (Exception) throwable.getCause();
        } else if (throwable instanceof RuntimeException) {
            throwable = (Exception) throwable.getCause();
        } else if (throwable instanceof InterruptedException) {
            throwable = (Exception) throwable.getCause();
        }

        if (throwable instanceof PocketException) {
            cfeException = (PocketException) throwable;
        } else {
            cfeException = new PocketException(PocketErrorCode.UnspecifiedErrorOccured);
        }
        return prepareErrorResponse(cfeException);
    }

    @ExceptionHandler({PocketException.class})
    public ResponseEntity<BaseResponseObject> pocketExceptionHandler(PocketException ex) {
        return prepareErrorResponse(ex);
    }

    private ResponseEntity<BaseResponseObject> prepareErrorResponse(PocketException pocketException) {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setData(null);
        baseResponseObject.setRequestId(pocketException.getRequestId());
        baseResponseObject.setStatus("" + ((pocketException.getStatusCode() == null || pocketException.getStatusCode() <= 0) ? 400 : pocketException.getStatusCode()));

        if (pocketException.getMessage() != null) {
            logWriterUtility.error(baseResponseObject.getRequestId(), pocketException.getMessage());
        }

        String localizedMessage = "";

        if (Integer.parseInt(pocketException.getErrorCode()) >= 10000 && Integer.parseInt(pocketException.getErrorCode()) < 70000) {
            localizedMessage = messageReceiver.getLocalizedMessage("" + pocketException.getErrorCode(),
                    pocketException.getArgs());
        } else {
            localizedMessage = pocketException.getMessage();
        }
        baseResponseObject.setError(new ErrorObject(pocketException.getErrorCode(), localizedMessage));

        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
        }

        try {
            logWriterUtility.trace(baseResponseObject.getRequestId(), "Response object: " + mapper.writeValueAsString(baseResponseObject));
        } catch (JsonProcessingException e) {
            logWriterUtility.error(pocketException.getRequestId(), e.getCause());
        }
        return new ResponseEntity<>(baseResponseObject,
                HttpStatus.BAD_REQUEST);
    }


}
