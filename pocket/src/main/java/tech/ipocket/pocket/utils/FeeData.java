package tech.ipocket.pocket.utils;

import java.math.BigDecimal;

public class FeeData {
    private Long id;
    private String feePayer;
    private BigDecimal feeAmount;
    private String feeCode;

    public FeeData() {
        feeAmount = BigDecimal.ZERO;
        feePayer = FeePayer.DEBIT;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }
}
