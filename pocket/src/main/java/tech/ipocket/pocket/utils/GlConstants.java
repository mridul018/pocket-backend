package tech.ipocket.pocket.utils;

public class GlConstants {
    public static final String VAT_Payable = "VAT_Payable";
    public static final String AIT_Payable = "AIT_Payable";
    public static final String QCASH = "QCASH";
    public static String Deposit_of_Master = "Deposit_of_Master";
    public static String Fee_payable_for_AdminCashIn = "Fee_payable_for_AdminCashIn";
    public static String Fee_payable_for_AdminCashOut = "Fee_payable_for_AdminCashOut";
    public static String Fee_payable_for_FundMovement = "Fee_payable_for_FundMovement";

    public static final String Deposit_of_Customer = "Deposit_of_Customer";

    public static String Deposit_of_Merchant = "Deposit_of_Merchant";
    public static String Deposit_of_Agent = "Deposit_of_Agent";
    //    public static String Balance_Of_SSL="Balance_Of_SSL";
    public static String Balance_Of_ = "Balance_Of_";


    public static final String Balance_of_TopUp_ToUpService = "Balance_of_TopUp_ToUpService";

    public static final String Fee_payable_for_TopUp = "Fee_payable_for_TopUp";
    public static final String Fee_payable_for_BillPay = "Fee_payable_for_BillPay";
    public static final String Fee_payable_for_Refill = "Fee_payable_for_Refill";
    public static final String Fee_payable_for_AddFund = "Fee_payable_for_AddFund";
    public static String Fee_payable_for_FundTransfer = "Fee_payable_for_FundTransfer";

    public static String Fee_payable_for_Merchant_Sale = "Fee_payable_for_Merchant_Sale";
    public static String Fee_payable_for_Cash_Out = "Fee_payable_for_Cash_Out";
    public static String Fee_payable_for_Cash_In = "Fee_payable_for_Cash_In";
    public static String Fee_payable_for_CashWithdrawal = "Fee_payable_for_CashWithdrawal";
}