package tech.ipocket.pocket.utils;

import sun.jvm.hotspot.jdi.SACoreAttachingConnector;

public enum TsEnums {
    ;

    public enum UserType {

        CUSTOMER("1001"),
        AGENT("1002"),
        MERCHANT("1003"),
        Master("2001");
        private final String userTypeString;

        UserType(String userTypeString) {
            this.userTypeString = userTypeString;
        }

        public String getUserTypeString() {
            return userTypeString;
        }
    }

    public enum Services {

        Fund_Transfer("1001"),
        Merchant_Payment("1002"),
        Top_Up("1003"),
        Master_Wallet_Deposit("1004"),
        AdminCashIn("1005"),
        Refill("1007"),
        CashIn("1009");
        private final String serviceCode;

        Services(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getServiceCode() {
            return serviceCode;
        }
    }


    public enum UserStatus {
        INITIALIZED("1"),
        ACTIVE("2"),
        DELETED("5");
        private final String userStatusType;

        UserStatus(String userStatusType) {
            this.userStatusType = userStatusType;
        }

        public String getUserStatusType() {
            return userStatusType;
        }
    }

    public enum EodStatus {
        INITIALIZE("0"),
        REVERSED("1"),
        COMPLETED("2");
        private final String eodStatus;

        EodStatus(String eodStatus) {
            this.eodStatus = eodStatus;
        }

        public String getEodStatus() {
            return eodStatus;
        }
    }

    public enum TransactionStatus {

        INITIALIZE("0"),
        REVERSED("1"),
        COMPLETED("2");
        private final String transactionStatus;

        TransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public String getTransactionStatus() {
            return transactionStatus;
        }
    }

    public enum TransactionDrCrType {

        DEBIT("DR"),
        CREDIT("CR");
        private final String transactionDrCrType;

        TransactionDrCrType(String transactionDrCrType) {
            this.transactionDrCrType = transactionDrCrType;
        }

        public String getTransactionDrCrType() {
            return transactionDrCrType;
        }
    }

}
