package tech.ipocket.pocket.utils;

public class ServiceCodeConstants {
    public static final String Fund_Transfer = "1001";
    public static final String Merchant_Payment = "1002";
    public static final String Top_Up = "1003";
    public static final String Master_Wallet_Deposit = "1004";
    public static final String Refill = "1007";
    public static final String AdminCashIn = "1005";
    public static final String CashIn = "1009";
}
