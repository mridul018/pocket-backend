package tech.ipocket.pocket.utils;

public enum UmEnums {
    ;


    public enum UserGroup {
        CUSTOMER("1001"),
        AGENT("1002"),
        MERCHANT("1003"),
        ADMIN("1004"),
        SUPER_ADMIN("1005");
        private final String groupCode;

        UserGroup(String groupCode) {
            this.groupCode = groupCode;
        }

        public String getGroupCode() {
            return groupCode;
        }
    }



    public enum UserStatus {
        Initiation(1),
        Verified(2),
        Blocked(3),
        HardBlocked(4),
        Deleted(5);
        private final int userStatusType;

        UserStatus(int userStatusType) {
            this.userStatusType = userStatusType;
        }

        public int getUserStatusType() {
            return userStatusType;
        }
    }

    public enum DeviceMapStatus {
        Active("1"),
        Deleted("2");
        private final String mapStatus;

        DeviceMapStatus(String mapStatus) {
            this.mapStatus = mapStatus;
        }

        public String getMapStatus() {
            return mapStatus;
        }
    }

    public enum OtpStatus {
        UnUsed("1"),
        Used("2");
        private final String otpStatus;

        OtpStatus(String otpStatus) {
            this.otpStatus = otpStatus;
        }

        public String getOtpStatus() {
            return otpStatus;
        }
    }

    public enum OtpTypes {
        FundTransafer("1001"),
        OtpForVerification("1002");
        private final String otpType;

        OtpTypes(String otpType) {
            this.otpType = otpType;
        }

        public String getOtpType() {
            return otpType;
        }
    }

    public enum ActivityLogType {
        DeviceAdd("1"),
        Registration("2"),
        LoginFailedTry("3"),
        LoginSuccessTry("4"),
        ForgotPinInitiate("5"),
        ForgotPinSuccess("6"),
        ResendOtp("7"),
        ChangePin("8");
        private final String activityLogType;

        ActivityLogType(String activityLogType) {
            this.activityLogType = activityLogType;
        }

        public String getActivityLogType() {
            return activityLogType;
        }
    }

    public enum ActivityLogStatus {
        Initialize("1"),
        Deleted("2");
        private final String activityLogStatus;

        ActivityLogStatus(String activityLogStatus) {
            this.activityLogStatus = activityLogStatus;
        }

        public String getActivityLogStatus() {
            return activityLogStatus;
        }
    }

    public enum DocumentTypes {
        ProfilePicture("1"),
        IdentificationNumber("2");
        private final String documentType;

        DocumentTypes(String documentType) {
            this.documentType = documentType;
        }

        public String getDocumentType() {
            return documentType;
        }
    }
}
