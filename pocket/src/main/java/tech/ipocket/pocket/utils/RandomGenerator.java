package tech.ipocket.pocket.utils;


import org.spongycastle.crypto.prng.VMPCRandomGenerator;
import org.spongycastle.util.encoders.Hex;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Random;

public class RandomGenerator {



    /*public static String generateTransactionReference() {
        RandomGenerator securityService = RandomGenerator.getInstance();
        return securityService.generateTokenAsString(6);
    }*/


    private static Object lockObject = new Object();
    private static RandomGenerator instance;
    private VMPCRandomGenerator localRandGenerator = new VMPCRandomGenerator();
    private static Random random = null;

    private RandomGenerator() {
        this.localRandGenerator.addSeedMaterial((new Date()).getTime());
        random = new Random();
    }

    public static RandomGenerator getInstance() {
        if (instance == null) {
            Object var0 = lockObject;
            synchronized (lockObject) {
                if (instance == null) {
                    instance = new RandomGenerator();
                }
            }
        }
        return instance;
    }

    private byte[] generateRandom(int length) {
        byte[] result = new byte[length];
        this.localRandGenerator.nextBytes(result);
        return result;
    }

    public String generatePIN(int numberOfDigits) {
        //return String.format("%0" + numberOfDigits + "d", (new Random()).nextInt((int) Math.pow(10.0D, (double) numberOfDigits)));
        return "3456";
    }

    private byte[] generateToken(int n) {
        if (n < 0) {
            n = Math.abs(n);
        }

        int bytesFromTime = n < 16 ? n / 2 : 8;
        byte[] result = new byte[n];
        getTimeStampBytes(result, 0, bytesFromTime);
        this.localRandGenerator.nextBytes(result, bytesFromTime, n - bytesFromTime);
        return result;
    }

    private static void getTimeStampBytes(byte[] dest, int offset, int length) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong((new Date()).getTime()).position(8 - length);
        buffer.get(dest, offset, length);
    }

    public String generateTokenAsString(int numberOfBytes) {
        byte[] tokenBytes = generateToken(numberOfBytes);
        return Hex.toHexString(tokenBytes).toUpperCase();

    }

    public String generateTransactionTokenAsString() {
        byte[] tokenBytes = generateToken(6);
        return Hex.toHexString(tokenBytes).toUpperCase();

    }


}