package tech.ipocket.pocket.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${external.config}")
    private boolean externalEnable;

    public boolean isExternalEnable() {
        return externalEnable;
    }

    public void setExternalEnable(boolean externalEnable) {
        this.externalEnable = externalEnable;
    }
}
