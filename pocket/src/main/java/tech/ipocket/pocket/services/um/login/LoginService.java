package tech.ipocket.pocket.services.um.login;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.um.ForgotPinStepOneRequest;
import tech.ipocket.pocket.request.um.ForgotPinStepTwoRequest;
import tech.ipocket.pocket.request.um.LoginRequest;
import tech.ipocket.pocket.request.um.LoginStepTwoRequest;
import tech.ipocket.pocket.response.um.ForgotPinStepOneResponse;
import tech.ipocket.pocket.response.um.LoginResponse;
import tech.ipocket.pocket.utils.PocketException;

import java.io.IOException;

public interface LoginService {
    Boolean isUserStatusAbleToLogin(UmUserInfo umUserInfo, LoginRequest loginRequest) throws PocketException;

    String validatePasswordAndGenerateToken(String password, UmUserInfo umUserInfo, LoginRequest loginRequest, Boolean isNewDevice) throws PocketException, IOException;

    Boolean checkIsNewDevice(UmUserInfo umUserInfo, LoginRequest loginRequest);

    LoginResponse generateLoginResponse(UmUserInfo umUserInfo, String requestId, Boolean isNewDevice, String sessionToken);

    LoginResponse doLoginWithOtp(LoginStepTwoRequest request) throws PocketException;

    ForgotPinStepOneResponse forgotPinStepOneRequest(ForgotPinStepOneRequest request) throws PocketException;

    boolean forgotPinStepTwoRequest(ForgotPinStepTwoRequest request) throws PocketException;
}
