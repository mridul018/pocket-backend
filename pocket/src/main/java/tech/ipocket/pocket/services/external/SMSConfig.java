package tech.ipocket.pocket.services.external;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SMSConfig {
    @Value("${spring.sms.username}")
    private String username;
    @Value("${spring.sms.password}")
    private String password;
    @Value("${spring.sms.url}")
    private String url;
    @Value("${spring.sms.from}")
    private String from;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
