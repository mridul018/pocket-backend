package tech.ipocket.pocket.services.ts.feeProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsFee;
import tech.ipocket.pocket.entity.TsFeeProfileLimitMapping;
import tech.ipocket.pocket.repository.ts.TsFeeProfileLimitMappingRepository;
import tech.ipocket.pocket.repository.ts.TsFeeRepository;
import tech.ipocket.pocket.utils.FeeData;
import tech.ipocket.pocket.utils.FeeMethod;
import tech.ipocket.pocket.utils.LogWriterUtility;

import java.math.BigDecimal;

@Service
public class FeeManager {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsFeeProfileLimitMappingRepository tsFeeProfileLimitMappingRepository;
    @Autowired
    private TsFeeRepository feeRepository;

    public FeeData calculateFee(TsClient client, String serviceCode, BigDecimal amount, String requestId) {
        FeeData feeData = new FeeData();


        TsFeeProfileLimitMapping mappings = tsFeeProfileLimitMappingRepository.findFirstByFeeProfileCodeAndServiceCode(client.getFeeProfileCode(), serviceCode);
        if (mappings == null) {
            logWriterUtility.error(requestId, "FeeProfile and Service map not found");
            return feeData;
        }

        TsFee feeProduct = feeRepository.findFirstByFeeCode(mappings.getFeeCode());

        if (feeProduct == null) {
            return feeData;
        }


        BigDecimal feeAmount;

        switch (feeProduct.getAppliedFeeMethod()) {
            case FeeMethod.PERCENTAGE:
                feeAmount = amount.multiply(feeProduct.getPercentageFee())
                        .divide(new BigDecimal(100));
                break;
            case FeeMethod.FIXED:
                feeAmount = feeProduct.getFixedFee();
                break;
            case FeeMethod.BOTH:
                feeAmount = feeProduct.getFixedFee()
                        .add((amount.multiply(feeProduct.getPercentageFee()))
                                .divide(new BigDecimal(100)));
                break;
            default:
                feeAmount = null;
        }

        feeData.setFeeAmount(feeAmount);
        feeData.setFeePayer(feeProduct.getFeePayer());
        feeData.setFeeCode(feeProduct.getFeeCode());

        return feeData;
    }
}