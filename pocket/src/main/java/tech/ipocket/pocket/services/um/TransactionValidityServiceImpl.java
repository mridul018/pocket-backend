package tech.ipocket.pocket.services.um;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSettings;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.um.UserSettingsRepository;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.external.TsServices;
import tech.ipocket.pocket.utils.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionValidityServiceImpl implements TransactionValidityService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private TsServices tsServices;


    @Override
    public BaseResponseObject umCheckTransactionValidity(TransactionValidityUmRequest request) throws PocketException {

        UmUserInfo senderInfo;
        UmUserSettings senderSettings;
        List<Integer> statusIn = new ArrayList<>();
        statusIn.add(UmEnums.UserStatus.Verified.getUserStatusType());

        switch (request.getType()) {
            case ServiceCodeConstants
                    .Fund_Transfer:

                // check sender
                //check receiver

                senderInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getSenderMobileNumber(), statusIn);

                if (senderInfo == null) {
                    logWriterUtility.error(request.getRequestId(), "Sender not found");
                    throw new PocketException(PocketErrorCode.SenderWalletNotFound);
                }

                UmUserInfo receiverInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getReceiverMobileNumber(), statusIn);

                if (receiverInfo == null) {
                    logWriterUtility.error(request.getRequestId(), "Receiver not found");
                    throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
                }

                // Check settings for isPrimary identification number is verified

                senderSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(senderInfo);
                if (senderSettings == null) {
                    logWriterUtility.error(request.getRequestId(), "Sender Settings not found");
                    throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
                }

                if (senderSettings.getPrimaryIdVerified() == null || !senderSettings.getPrimaryIdVerified()) {
                    logWriterUtility.error(request.getRequestId(), "Sender primary id is not verified.");
                    throw new PocketException(PocketErrorCode.SenderPrimaryIdNotVerified);
                }


                UmUserSettings receiverSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(receiverInfo);
                if (receiverSettings == null) {
                    logWriterUtility.error(request.getRequestId(), "Receiver Settings not found");
                    throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
                }

                if (receiverSettings.getPrimaryIdVerified() == null || !receiverSettings.getPrimaryIdVerified()) {
                    logWriterUtility.error(request.getRequestId(), "Receiver primary id is not verified.");
                    throw new PocketException(PocketErrorCode.ReceiverPrimaryIdNotVerified);
                }

                return tsServices.getTransactionValidity(request);

            case ServiceCodeConstants
                    .Top_Up:

                // check sender
                //check receiver

                senderInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getSenderMobileNumber(), statusIn);

                if (senderInfo == null) {
                    logWriterUtility.error(request.getRequestId(), "Sender not found");
                    throw new PocketException(PocketErrorCode.SenderWalletNotFound);
                }

                // Check settings for isPrimary identification number is verified

                senderSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(senderInfo);
                if (senderSettings == null) {
                    logWriterUtility.error(request.getRequestId(), "Sender Settings not found");
                    throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
                }

                if (senderSettings.getPrimaryIdVerified() == null || !senderSettings.getPrimaryIdVerified()) {
                    logWriterUtility.error(request.getRequestId(), "Sender primary id is not verified.");
                    throw new PocketException(PocketErrorCode.SenderPrimaryIdNotVerified);
                }

                return tsServices.getTransactionValidity(request);

            default:
                throw new PocketException(PocketErrorCode.UnsupportedService);

        }
    }
}
