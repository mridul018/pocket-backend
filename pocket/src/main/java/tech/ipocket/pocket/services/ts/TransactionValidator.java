package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
public class TransactionValidator {
    private LogWriterUtility loggerUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsLimitRepository limitRepository;
    @Autowired
    private TsTransactionProfileRepository tpRepository;

    @Autowired
    private TsTransactionProfileLimitMappingRepository profileLimitMappingRepository;
    @Autowired
    private TsTransactionRepository transactionRepository;


    public Boolean isSenderProfileValidForTransaction(TsClient clientSender, TsService service,
                                                      BigDecimal transactionAmount,
                                                      String requestId) throws PocketException {

        //balance validation
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(clientSender);

        if (clientBalance.getBalance().compareTo(transactionAmount) < 0) {
            loggerUtility.error(requestId, "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        // TODO: 2019-04-26 check balance after adding fee

        if (clientSender.getTransactionProfileCode() == null || clientSender.getTransactionProfileCode().trim().length() <= 0) {
            loggerUtility.error(requestId, "Sender transaction profile not found.");
            throw new PocketException(PocketErrorCode.SenderTransactionProfileNotFound);
        }

        TsTransactionProfileLimitMapping profileLimitMapping = profileLimitMappingRepository.findFirstByTxProfileCodeAndServiceCode(clientSender.getTransactionProfileCode(), service.getServiceCode());

        if (profileLimitMapping == null) {
            loggerUtility.error(requestId, "Sender does not support " + service.getServiceName() + " service.");
            throw new PocketException(PocketErrorCode.UnsupportedService, service.getServiceName(), service.getServiceName());
        }

        TsLimit senderLimit = limitRepository.findFirstByLimitCode(profileLimitMapping.getLimitCode());

        if (senderLimit.getMinAmount() < 0) {
            loggerUtility.error(requestId, "Min limit not satisfied for per transaction");
            throw new PocketException(PocketErrorCode.MinLimitFailed);
        }

        if (senderLimit.getMaxAmount() < 0) {
            loggerUtility.error(requestId, "Max limit not satisfied for per transaction");
            throw new PocketException(PocketErrorCode.MaxLimitFailed);
        }
        String serviceCode = service.getServiceCode();

        //daily limit validation
        Boolean isDailyLimitValid = validateDailyLimit(true, transactionAmount, clientSender, senderLimit, serviceCode, requestId);
        loggerUtility.trace(requestId, "isDailyLimitValid :" + isDailyLimitValid);
        if (!isDailyLimitValid) {
            loggerUtility.error(requestId, "Daily Limit validation failed");
            throw new PocketException(PocketErrorCode.DailyLimitValidationFailed);
        }


        //monthly limitValidation
        Boolean isMonthlyLimitValid = validateMonthlyLimit(true, transactionAmount, clientSender, senderLimit, serviceCode, requestId);
        loggerUtility.trace(requestId, "isMonthlyLimitValid :" + isMonthlyLimitValid);

        if (!isMonthlyLimitValid) {
            loggerUtility.error(requestId, "Monthly Limit validation failed");
            throw new PocketException(PocketErrorCode.MonthlyLimitValidationFailed);
        }

        loggerUtility.trace(requestId, "All limit check passed");
        return true;
    }


    public Boolean isReceiverProfileValidForTransaction(TsClient clientReceiver, TsService service,
                                                        String requestId) throws PocketException {

        if (clientReceiver.getTransactionProfileCode() == null || clientReceiver.getTransactionProfileCode().trim().length() <= 0) {
            loggerUtility.error(requestId, "Receiver transaction profile not found. ");
            throw new PocketException(PocketErrorCode.ReceiverTransactionProfileNotFound);
        }

        TsTransactionProfileLimitMapping profileLimitMapping = profileLimitMappingRepository.findFirstByTxProfileCodeAndServiceCode(clientReceiver.getTransactionProfileCode(), service.getServiceCode());

        if (profileLimitMapping == null) {
            loggerUtility.error(requestId, "Receiver does not support " + service.getServiceName() + " service.");
            throw new PocketException(PocketErrorCode.UnsupportedService, service.getServiceName(), service.getServiceName());
        }

        // TODO: 1/14/19 have to check receivers daily and monthly limit

        return true;
    }

    private Boolean validateDailyLimit(boolean isUserPayer, BigDecimal transactionAmount, TsClient clientSender, TsLimit senderLimit, String serviceCode, String requestId)
            throws PocketException {

        LimitValidationData dailyLimitValidation = getLimitValidationData(isUserPayer, serviceCode, 1L, clientSender);

        if (!dailyLimitValidation.getStatus().equals(PocketConstants.OK)) {
            loggerUtility.error(requestId, "Could not validate limit profile");
            throw new PocketException(PocketErrorCode.CouldNotValidateLimitProfile);
        } else {
            if (dailyLimitValidation.getCount() >= senderLimit.getDailyMaxNoOfTransaction()) {
                loggerUtility.error(requestId, "Daily maximum number of transaction exceeds");
                throw new PocketException(PocketErrorCode.DailyMaxTransactionExceeds);
            }

            if (dailyLimitValidation.getAmount().compareTo(new BigDecimal(senderLimit.getDailyMaxTransactionAmount())) >= 0) {
                loggerUtility.error(requestId, "Daily maximum amount of transaction exceeds");
                throw new PocketException(PocketErrorCode.DailyMaxTransactionAmountExceeds);
            }

            if (dailyLimitValidation.getAmount().add(transactionAmount).compareTo(new BigDecimal(senderLimit.getDailyMaxTransactionAmount())) >= 0) {
                loggerUtility.error(requestId, "This transaction will exceed the daily transaction limit");
                throw new PocketException(PocketErrorCode.DailyTransactionLimitExceeds);
            }
        }

        return true;
    }

    private Boolean validateMonthlyLimit(boolean isUserPayer, BigDecimal transactionAmount, TsClient clientSender, TsLimit senderLimit, String serviceCode, String requestId) throws PocketException {
        LimitValidationData monthlyLimitValidation = getLimitValidationData(isUserPayer, serviceCode, 30L, clientSender);

        if (!monthlyLimitValidation.getStatus().equals("200")) {
            loggerUtility.error(requestId, "Could not validate limit profile");
            throw new PocketException(PocketErrorCode.CouldNotValidateLimitProfile);
        } else if (monthlyLimitValidation.getCount() >= senderLimit.getMontlyMaxNoOfTransaction()) {
            loggerUtility.error(requestId, "Monthly maximum number of transaction exceeds");
            throw new PocketException(PocketErrorCode.MonthlyMaxTransactionExceeds);

        }

        if (monthlyLimitValidation.getAmount().doubleValue() >= senderLimit.getMonthlyMaxTransactionAmount()) {
            loggerUtility.error(requestId, "Monthly maximum amount of transaction exceeds");
            throw new PocketException(PocketErrorCode.MonthlyMaxTransactionAmountExceeds);
        }

        if (monthlyLimitValidation.getAmount().add(transactionAmount).compareTo(new BigDecimal(senderLimit.getMonthlyMaxTransactionAmount())) >= 0) {
            loggerUtility.error(requestId, "his transaction will exceed the monthly transaction limit");
            throw new PocketException(PocketErrorCode.MonthlyTransactionLimitExceeds);
        }
        return true;
    }


    public LimitValidationData getLimitValidationData(boolean isUserPayer, String serviceCode, Long timeFrame, TsClient client) {

        LimitValidationData limitValidationData = new LimitValidationData();
        Date endDate = Date.valueOf(LocalDate.now());
        Date startDate = Date.valueOf(LocalDate.now().minusDays(timeFrame));
        endDate = DateUtil.addDay(endDate, 1);

        List<TsTransaction> transactionList;

        if (isUserPayer) {
            transactionList = transactionRepository
                    .findByTransactionTypeAndTsClientBySenderClientIdAndCreatedDateBetween(serviceCode,
                            client, startDate, endDate);
        } else {
            transactionList = transactionRepository
                    .findByTransactionTypeAndTsClientByReceiverClientIdAndCreatedDateBetween(serviceCode,
                            client, startDate, endDate);
        }

        limitValidationData.setCount(transactionList.size());

        BigDecimal amount = BigDecimal.ZERO;

        if (transactionList.size() > 0) {
            for (TsTransaction transaction : transactionList) {
                amount = amount.add(transaction.getTransactionAmount());
            }
        }

        limitValidationData.setAmount(amount);
        limitValidationData.setStatus("200");

        return limitValidationData;
    }

}
