package tech.ipocket.pocket.services.um.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmUserDevice;
import tech.ipocket.pocket.entity.UmUserDeviceMapping;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserDeviceMapRepository;
import tech.ipocket.pocket.repository.um.UserDeviceRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.um.UserDeviceListUpdateRequest;
import tech.ipocket.pocket.response.um.UserDeviceListUpdateResponse;
import tech.ipocket.pocket.response.um.UserDeviceResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDeviceServiceImpl implements UserDeviceService {

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public ArrayList<UserDeviceResponse> getAllUserDevice(String loginId) throws PocketException {

        ArrayList<UserDeviceResponse> responses;
        List<UmUserDeviceMapping> userDeviceMappings = userDeviceMapRepository.findByUmUserInfoByUserId(userInfoRepository.findByLoginId(loginId));

        if (userDeviceMappings == null) {
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        responses = new ArrayList<>();

        userDeviceMappings.forEach(map -> {
            UmUserDevice device = map.getUmUserDeviceByDeviceId();

            responses.add(new UserDeviceResponse(
                    device.getId(), device.getHardwareSignature(), device.getDeviceName(),
                    device.getMetaData(), device.getCreatedDate(), map.getStatus()
            ));

        });

        return responses;
    }

    @Override
    public UserDeviceListUpdateResponse updateUserDevice(UserDeviceListUpdateRequest request) throws PocketException {

        UserDeviceListUpdateResponse response = new UserDeviceListUpdateResponse();

        UmUserInfo userInfo = new UmUserInfo();
        userInfo.setId(request.getUserId());

        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setId(request.getDeviceId());

        UmUserDeviceMapping mapping = userDeviceMapRepository
                .findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(
                        userInfo, userDevice, request.getStatus()
                );
        if (mapping == null) {
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        mapping.setStatus(request.getStatus());
        userDeviceMapRepository.save(mapping);

        response.setUpdateCompleted(true);

        return response;
    }
}
