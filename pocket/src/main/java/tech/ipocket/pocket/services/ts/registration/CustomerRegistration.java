package tech.ipocket.pocket.services.ts.registration;

import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;

public class CustomerRegistration extends AbstractRegistration {


    @Override
    public void setData(WalletCreateRequest walletCreateRequest) {
        this.setMobileNumber(walletCreateRequest.getMobileNumber());
        this.setFullName(walletCreateRequest.getFullName());
        this.setGroupCode(walletCreateRequest.getGroupCode());
        this.setUserStatus(walletCreateRequest.getUserStatus());
        this.setRequestId(walletCreateRequest.getRequestId());
    }
}
