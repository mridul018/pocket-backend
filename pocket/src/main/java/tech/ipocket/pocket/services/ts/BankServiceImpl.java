package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.TsBanks;
import tech.ipocket.pocket.entity.TsGl;
import tech.ipocket.pocket.repository.ts.TsBankRepository;
import tech.ipocket.pocket.repository.ts.TsGlRepository;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.CreateBankRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.List;

@Service
public class BankServiceImpl implements BankService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsBankRepository tsBankRepository;
    @Autowired
    private TsGlRepository tsGlRepository;


    @Transactional(rollbackFor = {PocketException.class, Exception.class})
    @Override
    public BaseResponseObject createBank(CreateBankRequest request) throws PocketException {

        TsBanks tsBanks = tsBankRepository.findFirstByBankName(request.getBankName());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank name already exists");
            throw new PocketException(PocketErrorCode.BankAlreadyExists);
        }

        tsBanks = tsBankRepository.findFirstByGlCode(request.getBankGlCode());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank gl already exists");
            throw new PocketException(PocketErrorCode.BankGlCodeExists);
        }

        tsBanks = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank code already exists");
            throw new PocketException(PocketErrorCode.BankCodeExists);
        }

        TsGl tsGl = tsGlRepository.findFirstByGlCode(request.getBankGlCode());
        if (tsGl != null) {
            logWriterUtility.error(request.getRequestId(), "gl code already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        tsGl = new TsGl();
        tsGl.setSystemDefault(true);
        tsGl.setParentGlCode("40000000");
        tsGl.setGlCode(request.getBankGlCode());
        tsGl.setParentGl(false);
        tsGl.setGlName("GlCodeOfBank_" + request.getBankCode());

        tsGlRepository.save(tsGl);

        tsBanks = new TsBanks();
        tsBanks.setBankName(request.getBankName());
        tsBanks.setBankCode(request.getBankCode());
        tsBanks.setGlCode(request.getBankGlCode());
        tsBanks.setRoutingNo(request.getBankRoutingNumber());


        tsBankRepository.save(tsBanks);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getBanks(EmptyRequest request) {

        List<TsBanks> banks = (List<TsBanks>) tsBankRepository.findAll();
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(banks);
        return baseResponseObject;
    }
}
