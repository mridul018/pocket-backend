package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsClientBalance;
import tech.ipocket.pocket.entity.TsTransactionDetails;
import tech.ipocket.pocket.repository.ts.TsClientBalanceRepository;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionDetailsRepository;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.BalanceCheckResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.utils.*;

import javax.persistence.LockModeType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;
    @Autowired
    private TsTransactionDetailsRepository transactionDetailRepository;

    @Override
    public BaseResponseObject checkBalance(String walletId, String requestId) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(walletId, clientStatus);

        if (client == null) {
            logWriterUtility.error(requestId, "Wallet not found");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
        if (clientBalance == null) {
            logWriterUtility.error(requestId, "Inconsistent data on clientBalance table");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        BalanceCheckResponse balanceCheckResponse = new BalanceCheckResponse();
        balanceCheckResponse.setBalance(clientBalance.getBalance());
        balanceCheckResponse.setDate(new Date().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(balanceCheckResponse);

        return baseResponseObject;
    }

    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    public BigDecimal updateClientCurrentBalance(TsClient client, String requestId) {

        String walletRef = client.getWalletNo();

        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
        List<TsTransactionDetails> debitTxList = transactionDetailRepository.findAllByWalletReferenceAndTransactionTypeIgnoreCase(walletRef, TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());

        List<TsTransactionDetails> creditTxList = transactionDetailRepository.findAllByWalletReferenceAndTransactionTypeIgnoreCase(walletRef, TsEnums.TransactionDrCrType.CREDIT.getTransactionDrCrType());

        BigDecimal debitAmount = sumTransactionAmount(debitTxList);

        BigDecimal creditAmount = sumTransactionAmount(creditTxList);

        BigDecimal finalBalance = creditAmount.subtract(debitAmount);

        clientBalance.setBalance(finalBalance);
        clientBalanceRepository.save(clientBalance);

        return finalBalance;
    }

    @Override
    public BaseResponseObject updateClientStatus(String mobileNumber, String newStatus, String requestId) throws PocketException {
        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(mobileNumber, clientStatus);

        if (client == null) {
            logWriterUtility.error(requestId, "Wallet not found");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        client.setUserStatus(newStatus);
        client.setVerificationDate(new Date());

        clientRepository.save(client);

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    private BigDecimal sumTransactionAmount(List<TsTransactionDetails> transactionDetailList) {

        BigDecimal amount = new BigDecimal(0);

        if (transactionDetailList == null || transactionDetailList.size() == 0) {
            return amount;
        }

        for (TsTransactionDetails transactionDetail : transactionDetailList) {
            amount = amount.add(transactionDetail.getTransactionAmount());
        }

        return amount;
    }

}
