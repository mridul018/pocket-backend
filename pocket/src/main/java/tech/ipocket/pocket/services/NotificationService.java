package tech.ipocket.pocket.services;

import tech.ipocket.pocket.request.notification.NotificationRequest;

public interface NotificationService {
    void sendNotification(NotificationRequest notificationRequest);
}
