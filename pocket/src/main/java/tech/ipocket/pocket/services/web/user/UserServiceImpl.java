package tech.ipocket.pocket.services.web.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.UmDocuments;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSettings;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.um.UserDetailsRepository;
import tech.ipocket.pocket.repository.um.UserDocumentRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.um.UserSettingsRepository;
import tech.ipocket.pocket.request.um.ChangeCredentialRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.web.UserDetailsWithWalletResponse;
import tech.ipocket.pocket.response.web.UserListResponse;
import tech.ipocket.pocket.services.external.ExternalService;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private TsClientRepository clientRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private UserDocumentRepository userDocumentRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CommonService commonService;
    @Autowired
    private ExternalService externalService;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private AppConfiguration appConfiguration;



    @Override
    public BaseResponseObject getUserList(UserListRequest userListRequest) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        Page<UmUserInfo> userInfos=null;

        if(userListRequest.getGroupCode()==null){
            userInfos = userInfoRepository.findAll(PageRequest.of(userListRequest.getPage(),
                    userListRequest.getSize()));
        }else{
            userInfos = userInfoRepository.findAllByGroupCode(userListRequest.getGroupCode(),
                    PageRequest.of(userListRequest.getPage(), userListRequest.getSize()));
        }

        if (userInfos == null) {
            response.setError(new ErrorObject(PocketErrorCode.USER_NOT_FOUND));
            return response;
        }

        ArrayList<UserListResponse> userListResponses = new ArrayList<>();
        userInfos.forEach(userInfo -> {

            UserListResponse listResponse=new UserListResponse();
            listResponse.setId(userInfo.getId());
            listResponse.setGroupCode(userInfo.getGroupCode());
            listResponse.setFullName(userInfo.getFullName());
            listResponse.setLoginId(userInfo.getLoginId());
            listResponse.setPicture(""+userInfo.getPhotoId());
            listResponse.setAccountStatus(""+userInfo.getAccountStatus());

            userListResponses.add(listResponse);
        });

        response.setData(userListResponses);
        response.setError(null);
        response.setStatus(PocketConstants.OK);

        return response;
    }

    @Override
    public BaseResponseObject searchUser(UserSearchRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        List<UmUserInfo> userInfos;

        if (request.getSearchContent().matches("[0-9]")) {
            userInfos = userInfoRepository.findByLoginIdContaining(request.getSearchContent());
        } else {
            userInfos = userInfoRepository.findByFullNameContaining(request.getSearchContent());
        }

        if (userInfos == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        ArrayList<UserListResponse> listResponses = new ArrayList<>();
        userInfos.forEach(userInfo -> {

            UserListResponse listResponse=new UserListResponse();
            listResponse.setId(userInfo.getId());
            listResponse.setFullName(userInfo.getFullName());
            listResponse.setLoginId(userInfo.getLoginId());
            listResponse.setGroupCode(userInfo.getGroupCode());
            listResponse.setPicture(""+userInfo.getPhotoId());
            listResponse.setAccountStatus(""+userInfo.getAccountStatus());
            listResponses.add(listResponse);
        });

        response.setError(null);
        response.setData(listResponses);
        response.setStatus(PocketConstants.OK);

        return response;
    }

    @Override
    public BaseResponseObject editUser(UserEditRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        userInfo.setFullName(request.getFullName());

        userInfoRepository.save(userInfo);

        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setData("User edit done");
        response.setRequestId(request.getRequestId());

        return response;
    }

    @Override
    public BaseResponseObject pinReset(UserPinPasswordResetRequest request) throws PocketException {

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        String newPin = RandomGenerator.getInstance().generatePIN(4);
        userInfo.setPassword(Base64.getEncoder().encodeToString(newPin.getBytes()));

        userInfo.setPassword(newPin);
        userInfoRepository.save(userInfo);

        String msg = "Dear user, Your new PIN number is : " + newPin;

        new ExternalCall(request.getLoginId(), "SMS", msg, ExternalType.SMS, restTemplate,request.getRequestId(),appConfiguration.isExternalEnable()).start();

        BaseResponseObject response = new BaseResponseObject();
        response.setError(new ErrorObject(PocketErrorCode.INVALID_LOGIN_CREDENTIAL));
        response.setData(new SuccessBoolResponse(true));
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());
        return response;
    }

    @Override
    public BaseResponseObject localUnlockWallet(UserLockUnlockWalletRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        TsClient client = clientRepository.findByWalletNo(request.getWalletNumber());
        if (client == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (client.getUserStatus().equals(AccountStatus.HARD_BLOCKED.getKeyString())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (client.getUserStatus().equals(AccountStatus.BLOCKED.getKeyString())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (client.getUserStatus().equals(AccountStatus.DELETED.getKeyString())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        client.setUserStatus(request.getWalletStatus());
        clientRepository.save(client);

        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData("Wallet lock or unlock done");
        response.setError(null);


        return response;
    }

    @Override
    public BaseResponseObject verifyPrimaryIdInfo(UserPrimaryIdInfoVerificationRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getWalletId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(request.getWalletId());
        if (umUserInfo == null) {
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        UmUserSettings settings = userSettingsRepository.findFirstByUmUserInfoByUserId(userInfo);
        if (settings == null) {
            throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
        }

        // call ts for update wallet status
        UmUpdateClientStatusRequest updateClientStatusRequest=new UmUpdateClientStatusRequest();
        updateClientStatusRequest.setWalletId(userInfo.getMobileNumber());
        updateClientStatusRequest.setNewStatus(""+UmEnums.UserStatus.Verified.getUserStatusType());
        updateClientStatusRequest.setHardwareSignature(request.getHardwareSignature());
        updateClientStatusRequest.setSessionToken(request.getSessionToken());
        updateClientStatusRequest.setRequestId(request.getRequestId());

        Boolean isUpdated=externalService.callTxForUpdateClientStatus(updateClientStatusRequest);

        if(!isUpdated){
            logWriterUtility.error(request.getRequestId(),"Ts service status update failed");
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError);
        }

        umUserInfo.setAccountStatus(UmEnums.UserStatus.Verified.getUserStatusType());
        userInfoRepository.save(umUserInfo);

        UmUserDetails umUserDetails=userDetailsRepository.findFirstByUmUserInfoByUserId(umUserInfo);
        umUserDetails.setPrimaryIdVerificationDate(DateUtil.currentDate());
        userDetailsRepository.save(umUserDetails);

        settings.setPrimaryIdVerified(request.isVerifyPrimaryId());
        userSettingsRepository.save(settings);

        response.setError(null);
        response.setData(new SuccessBoolResponse(true));
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());
        return response;
    }

    @Override
    public BaseResponseObject userDetailsWithWallet(UserDetailsWithWalletRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        UserDetailsWithWalletResponse details = new UserDetailsWithWalletResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        TsClient client = clientRepository.findByWalletNo(request.getLoginId());
        if (client == null) {
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        Optional<UmDocuments> documents = userDocumentRepository.findById(userInfo.getPhotoId());
        if (!documents.isPresent()) {
            logWriterUtility.error(request.getRequestId(), "Image Document not found");
        } else {
            details.setPhotoUrl(documents.get().getDocumentPath());
        }

        if (client.getTsClientBalancesById() == null) {
            response.setError(new ErrorObject(PocketErrorCode.CUSTOMER_BALANCE_NOT_FOUND));
            return response;
        }

        details.setId(userInfo.getId());
        details.setAccountStatus(userInfo.getAccountStatus());
        details.setEmail(userInfo.getEmail());
        details.setFullName(userInfo.getFullName());
        details.setGroupCode(userInfo.getGroupCode());
        details.setFeeProfileCode(client.getFeeProfileCode());
        details.setTransactionProfileCode(client.getTransactionProfileCode());
        client.getTsClientBalancesById().forEach(balance -> {
            details.setAmount(balance.getBalance());
        });

        response.setError(null);
        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData(details);

        return response;
    }

    @Override
    public BaseResponseObject changeCredential(ChangeCredentialRequest request, TokenValidateUmResponse validateResponse) throws PocketException {

        UmUserInfo client = userInfoRepository.findByMobileNumber(validateResponse.getMobileNumber());
        if (client == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        // check old pin

        if(request.getNewCredential().equals(request.getOldCredential())){
            logWriterUtility.error(request.getRequestId(),"Both credential are same");
            throw new PocketException(PocketErrorCode.OldAndNewCredentialMustDifferent);
        }

        if (!client.getPassword().equals(Base64.getEncoder().encodeToString(request.getOldCredential().getBytes()))) {
            logWriterUtility.error(request.getRequestId(),"Old credential not matched");
            throw new PocketException(PocketErrorCode.OldCredentialNotMatched);
        }


        // if not matched then throw

        //update new PIN
        client.setPassword(Base64.getEncoder().encodeToString(request.getNewCredential().getBytes()));
        userInfoRepository.save(client);

        String message="Credential changed successfully";

        commonService.addInteractionLog(client.getLoginId(), message, request.getHardwareSignature(), request.getDeviceName(),
                UmEnums.ActivityLogType.ChangePin.getActivityLogType(), client);


        BaseResponseObject response = new BaseResponseObject(request.getRequestId());
        response.setError(null);
        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData(new SuccessBoolResponse(true));
        return response;
    }


}
