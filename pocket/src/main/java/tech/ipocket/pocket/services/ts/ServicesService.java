package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.request.ts.services.CreateUpdateServiceRequest;
import tech.ipocket.pocket.request.ts.services.GetServicesRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface ServicesService {

    BaseResponseObject getAllService(GetServicesRequest request) throws PocketException;
    BaseResponseObject createUpdateService(CreateUpdateServiceRequest request) throws PocketException;

}
