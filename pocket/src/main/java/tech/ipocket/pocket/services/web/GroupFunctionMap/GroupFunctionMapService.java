package tech.ipocket.pocket.services.web.GroupFunctionMap;

import tech.ipocket.pocket.request.um.GroupFunctionListMapRequest;
import tech.ipocket.pocket.request.um.GroupFunctionMapRequest;
import tech.ipocket.pocket.request.web.GroupFunctionMap;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.web.GroupFunctionMapListResponse;
import tech.ipocket.pocket.response.web.GroupFunctionMapResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface GroupFunctionMapService {
    GroupFunctionMapResponse addUpdateDeleteGroupFunctionMap(GroupFunctionMapRequest request) throws PocketException;

    GroupFunctionMapListResponse allGroupFunctionMapList() throws PocketException;

    BaseResponseObject mapGroupWithFunctions(GroupFunctionListMapRequest request) throws PocketException;
}
