package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.request.ts.services.CreateUpdateServiceRequest;
import tech.ipocket.pocket.request.ts.services.GetServicesRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.List;

@Service
public class ServicesServiceImpl implements ServicesService {
    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private TsServiceRepository tsServiceRepository;

    @Override
    public BaseResponseObject getAllService(GetServicesRequest request) throws PocketException {

        List<TsService> serviceList = tsServiceRepository.findAllByStatus("1");

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(serviceList);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject createUpdateService(CreateUpdateServiceRequest request) throws PocketException {

        if(request.getId()>0){
            return updateService(request);
        }

        // check service exists

        TsService tsService=tsServiceRepository.findFirstByServiceCode(request.getServiceCode());

        if(tsService!=null){
            logWriterUtility.error(request.getRequestId(),
                    "Service code exists :"+request.getServiceCode());
            throw new PocketException(PocketErrorCode.ServiceCodeExists);
        }

        tsService=tsServiceRepository.findFirstByServiceName(request.getServiceName());

        if(tsService!=null){
            logWriterUtility.error(request.getRequestId(),
                    "Service name exists :"+request.getServiceName());
            throw new PocketException(PocketErrorCode.ServiceNameExists);
        }

        tsService=new TsService();
        tsService.setServiceCode(request.getServiceCode());
        tsService.setServiceName(request.getServiceName());
        tsService.setDescription(request.getDescription());
        tsService.setStatus("1");

        tsServiceRepository.save(tsService);

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(tsService);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }

    private BaseResponseObject updateService(CreateUpdateServiceRequest request) throws PocketException {
        TsService tsService=tsServiceRepository.findFirstById(request.getId());

        if(tsService==null){
            logWriterUtility.error(request.getRequestId(),
                    "Service not found :"+request.getId());
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        if(!request.getDescription().equals(tsService.getDescription())){
            tsService.setDescription(request.getDescription());
        }

        tsServiceRepository.save(tsService);

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(tsService);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }
}
