package tech.ipocket.pocket.services.um.otp;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.concurrent.ExecutionException;

public abstract class AbstractOtp {
    static LogWriterUtility logWriterUtility = new LogWriterUtility(AbstractOtp.class);

    CommonOtpService commonOtpService;
    OtpService otpService;

    public AbstractOtp(CommonOtpService commonOtpService, OtpService otpService) {
        this.commonOtpService = commonOtpService;
        this.otpService = otpService;
    }

    public abstract BaseResponseObject sendOtp(SendOtpRequest sendOtpRequest, Integer userCredentialId) throws PocketException, ExecutionException, InterruptedException;

    protected UmUserInfo checkIsValidSender(Integer credentialId, String requestID) throws PocketException {
        return commonOtpService.checkIsValidSender(credentialId, requestID);
    }

    protected  void checkIsValidReceiver(String receiverMobileNumber, String requestId) throws PocketException {
        commonOtpService.checkIsValidReceiver(receiverMobileNumber, requestId);
    }

    protected void checkIsValidReceiverAndIsValidMpos(String receiverMobileNumber, String requestId) throws PocketException {
        commonOtpService.checkIsValidReceiver(receiverMobileNumber, requestId);
    }

    protected void checkIsSenderBalanceSufficient(BalanceCheckRequest balanceInqueryRequest) throws PocketException {
        commonOtpService.checkIsSenderBalanceSufficient(balanceInqueryRequest);
    }

    protected boolean callTxForTransactionVisibility(TransactionValidityUmRequest visibilityCheckRequest) throws PocketException {

        logWriterUtility.trace(visibilityCheckRequest.getRequestId(), "Calling Transaction Service for check Transaction Visibility");

        if (!commonOtpService.callTxForTransactionVisibility(visibilityCheckRequest)) {
            logWriterUtility.trace(visibilityCheckRequest.getRequestId(), "Returning because Transaction not possible for this accounts.");
            throw new PocketException(PocketErrorCode.TransactionNotPossibleForAccounts);
        }
        logWriterUtility.trace(visibilityCheckRequest.getRequestId(), "Transaction possible among these accounts.");

        return true;
    }

    protected void sendOtpNotification(String mobileNumber, String email, UmUserInfo userCredential, String otp,  String requestId) {
        commonOtpService.sendOtpNotification(mobileNumber, email, userCredential, otp, requestId);
    }
}