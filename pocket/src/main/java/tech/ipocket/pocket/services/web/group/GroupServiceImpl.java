package tech.ipocket.pocket.services.web.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.web.GroupAndFunctionMapResponse;
import tech.ipocket.pocket.response.web.GroupCreateOrUpdateResponse;
import tech.ipocket.pocket.response.web.UserFunctionResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private UserGroupFunctionMapRepository functionMapRepository;

    @Autowired
    private UserFunctionRepository userFunctionRepository;

    @Override
    public GroupCreateOrUpdateResponse createOrUpdateOrDeleteGroup(GroupRequest request) throws PocketException {
        GroupCreateOrUpdateResponse response = new GroupCreateOrUpdateResponse();

        UmUserGroup userGroup = userGroupRepository.findByGroupCode(request.getGroupCode());
        if (userGroup != null) {
            response.setError(new ErrorObject(PocketErrorCode.GROUP_ALREADY_EXISTS));
            return response;
        }

        if (userGroup == null) userGroup = new UmUserGroup();

        userGroup.setGroupName(request.getGroupName());
        userGroup.setGroupCode(request.getGroupCode());
        userGroup.setDescription(request.getDescription());
        userGroup.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userGroup.setGroupStatus(request.getGroupStatus());

        UmUserGroup newGroup = userGroupRepository.saveAndFlush(userGroup);

        response.setGroupId(newGroup.getId());
        response.setMessage("Group create successfully done");
        return response;
    }

    @Override
    public List<GroupAndFunctionMapResponse> getAllGroupsWithFunctions() throws PocketException {

        List<UmUserGroup> groupList = userGroupRepository.findAll();

        if (groupList == null || groupList.size() == 0) {
            return null;
        }

        List<GroupAndFunctionMapResponse> finalResponse = new ArrayList<>();

        for (UmUserGroup group : groupList) {

            GroupAndFunctionMapResponse listItem=new GroupAndFunctionMapResponse();
            listItem.setGroupCode(group.getGroupCode());
            listItem.setGroupName(group.getGroupName());

            List<UmGroupFunctionMapping> mappings = functionMapRepository.findAllByGroupCodeAndStatus(group.getGroupCode(), "1");

            List<UserFunctionResponse> functionResponses=new ArrayList<>();
            if (mappings != null && mappings.size() > 0) {

                List<String> functionCodesList = mappings.stream()
                        .map(UmGroupFunctionMapping::getFunctionCode)
                        .collect(Collectors.toList());
                List<UmUserFunction> functions = userFunctionRepository.findAllByFunctionCodeIn(functionCodesList);

                functionResponses=new ArrayList<>();

                for (UmUserFunction function:functions) {
                    UserFunctionResponse item=new UserFunctionResponse();
                    item.setId(function.getId());
                    item.setFunctionName(function.getFunctionName());
                    item.setFunctionCode(function.getFunctionCode());
                    functionResponses.add(item);
                }
            }
            listItem.setFunctions(functionResponses);

            finalResponse.add(listItem);
        }
        return finalResponse;
    }
}
