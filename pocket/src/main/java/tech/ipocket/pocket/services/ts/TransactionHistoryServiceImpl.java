package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.ITsTransaction;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionRepository;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionHistoryRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponse;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponseRoot;
import tech.ipocket.pocket.utils.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsServiceRepository tsServiceRepository;

    @Override
    public BaseResponseObject getTransactionHistory(GetTransactionHistoryRequest request) throws PocketException {

        List<ITsTransaction> transactions;


    PageRequest pageRequest = PaginationUtil.makePageRequest(request.getPageId(), request.getNumberOfItemPerPage(),
            Sort.Direction.DESC, "id");


        if(request.getFromDate()==null || request.getToDate()==null){
            if (request.getTransactionTypes() == null
                    || request.getTransactionTypes().size() == 0) {
                transactions = tsTransactionRepository.findAllByReceiverWalletOrSenderWallet(request.getWalletNo(), request.getWalletNo(),pageRequest);
            } else {
                transactions = tsTransactionRepository.findAllByTransactionTypeInAndReceiverWalletOrSenderWallet(request.getTransactionTypes(),
                        request.getWalletNo(), request.getWalletNo(),pageRequest);
            }
        }else{

            Date fromDate=DateUtil.parseDate(request.getFromDate());

            Date toDate= DateUtil.parseDate(request.getToDate());
            toDate=DateUtil.addDay(toDate,1);

            if (request.getTransactionTypes() == null
                    || request.getTransactionTypes().size() == 0) {
                transactions = tsTransactionRepository.findAllByReceiverWalletOrSenderWalletDateBetween(request.getWalletNo(),
                        request.getWalletNo(),fromDate,toDate,pageRequest);
            } else {
                transactions = tsTransactionRepository.findAllByTransactionTypeInAndReceiverWalletOrSenderWalletDateBetween(request.getTransactionTypes(),
                        request.getWalletNo(), request.getWalletNo(),fromDate,toDate,pageRequest);
            }
        }

        List<TransactionHistoryResponse> historyResponseList=new ArrayList<>();
        TransactionHistoryResponse historyResponseItem=null;

        for (ITsTransaction iTsTransaction:transactions) {
            historyResponseItem=new TransactionHistoryResponse();

            historyResponseItem.setTransactionType(iTsTransaction.getTransactionType());

            TsService tsService=tsServiceRepository.findFirstByServiceCode(iTsTransaction.getTransactionType());
            if(tsService!=null){
                historyResponseItem.setTransactionType(tsService.getDescription());
            }

            historyResponseItem.setId(iTsTransaction.getId());
            historyResponseItem.setCreatedDate(iTsTransaction.getCreatedDate().toString());
            historyResponseItem.setDisputable(iTsTransaction.getIsDisputable());
            historyResponseItem.setFeeAmount(iTsTransaction.getFeeAmount().doubleValue());
            historyResponseItem.setFeeCode(iTsTransaction.getFeeCode());
            historyResponseItem.setFeePayer(iTsTransaction.getFeePayer());
            historyResponseItem.setTransactionAmount(iTsTransaction.getTransactionAmount().doubleValue());
            historyResponseItem.setSenderDebitAmount(iTsTransaction.getSenderDebitAmount().doubleValue());
            historyResponseItem.setReceiverCreditAmount(iTsTransaction.getReceiverCreditAmount().doubleValue());
            historyResponseItem.setToken(iTsTransaction.getToken());
            historyResponseItem.setSenderWallet(iTsTransaction.getSenderWallet());
            historyResponseItem.setReceiverWallet(iTsTransaction.getReceiverWallet());
            historyResponseItem.setTransactionStatus(iTsTransaction.getTransactionStatus());
            historyResponseItem.setPrevTransactionRef(iTsTransaction.getPrevTransactionRef());
            historyResponseItem.setFeeCode(iTsTransaction.getFeeCode());
            historyResponseItem.setLogicalSender(iTsTransaction.getLogicalSender());
            historyResponseItem.setLogicalReceiver(iTsTransaction.getLogicalReceiver());
            historyResponseList.add(historyResponseItem);
        }

        TransactionHistoryResponseRoot responseRoot=new TransactionHistoryResponseRoot();
        responseRoot.setHistory(historyResponseList);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(responseRoot);

        return baseResponseObject;
    }
}
