package tech.ipocket.pocket.services.external;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.utils.ExternalType;
import tech.ipocket.pocket.utils.PocketException;

@Component
public class ExternalFactory implements ApplicationContextAware {
    private ApplicationContext context;

    @Autowired
    private ExternalService externalService;

    public ExternalResponse externalServices(ExternalRequest request) throws PocketException {

        switch (request.getType()) {
            case ExternalType.EMAIL:
                return externalService.sendEmail(request);
            case ExternalType.SMS:
                return externalService.sendSMS(request);
            case ExternalType.NOTIFICATION:
                return externalService.sendNotification(request);
        }


        return null;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
