package tech.ipocket.pocket.services.ts.feeProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.feeProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.feeProfile.FeeListItem;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class FeeProfileServiceImpl implements FeeProfileService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsFeeProfileRepository tsFeeProfileRepository;

    @Autowired
    private TsFeeRepository tsFeeRepository;

    @Autowired
    private TsFeeStakeholderRepository tsFeeStakeholderRepository;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private TsFeeFeeStakeholderMapRepository tsFeeFeeStakeholderMapRepository;

    @Autowired
    private TsStakeHolderRepository tsStakeHolderRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsFeeProfileLimitMappingRepository tsFeeProfileLimitMappingRepository;


    @Override
    public BaseResponseObject createFeeProfile(CreateFeeProfileRequest request) throws PocketException {

        // check by fee code
        //check by fee name

        TsFeeProfile tsFeeProfile = tsFeeProfileRepository.findFirstByProfileCode(request.getProfileCode());
        if (tsFeeProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Fee profile code already exists");
            throw new PocketException(PocketErrorCode.FeeProfileCodeAlreadyExists);
        }

        tsFeeProfile = tsFeeProfileRepository.findFirstByProfileName(request.getProfileName());
        if (tsFeeProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Fee profile name already exists");
            throw new PocketException(PocketErrorCode.FeeProfileNameAlreadyExists);
        }

        tsFeeProfile = new TsFeeProfile();
        tsFeeProfile.setProfileCode(request.getProfileCode());
        tsFeeProfile.setProfileName(request.getProfileName());
        tsFeeProfile.setCreatedDate(new Date());
        tsFeeProfileRepository.save(tsFeeProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getFeeProfile(GetFeeProfileRequest request) {

        List<TsFeeProfile> feeProfileList = (List<TsFeeProfile>) tsFeeProfileRepository.findAll();

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(feeProfileList);

        return baseResponseObject;
    }

    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    @Override
    public BaseResponseObject createFee(CreateFeeRequest request) throws PocketException {
        // check by fee code
        //check by fee name

        TsFee tsFee = tsFeeRepository.findFirstByFeeCode(request.getFeeCode());
        if (tsFee != null) {
            logWriterUtility.error(request.getRequestId(), "Fee code already exists");
            throw new PocketException(PocketErrorCode.FeeCodeAlreadyExists);
        }

        tsFee = tsFeeRepository.findFirstByFeeName(request.getFeeName());
        if (tsFee != null) {
            logWriterUtility.error(request.getRequestId(), "Fee name already exists");
            throw new PocketException(PocketErrorCode.FeeNameAlreadyExists);
        }

        // check stakeholder

        List<TsFeeStakeholder> tsFeeStakeholders = new ArrayList<>();
        double totalPercentage = 0;


        for (FeeStakeHolderItem feeStakeHolderItem : request.getStakeHolders()
        ) {

            TsFeeStakeholder tsFeeStakeholder = tsFeeStakeholderRepository.findFirstById(feeStakeHolderItem.getStakeHolderId());
            if (tsFeeStakeholder == null) {
                logWriterUtility.error(request.getRequestId(), "Fee stakeholder not found");
                throw new PocketException(PocketErrorCode.StakeHolderNotFound);
            }
            totalPercentage += feeStakeHolderItem.getPercentage();
            tsFeeStakeholders.add(tsFeeStakeholder);
        }

        if(totalPercentage!=100){
            logWriterUtility.error(request.getRequestId(),"Total stakeholder percentage must be equals 100");
            throw new PocketException(PocketErrorCode.StakeHolderPercentageMustEquals100);
        }

        tsFee = new TsFee();
        tsFee.setFeeCode(request.getFeeCode());
        tsFee.setFeeName(request.getFeeName());
        tsFee.setFixedFee(request.getFixedFee());
        tsFee.setPercentageFee(request.getPercentageFee());
        tsFee.setBoth(request.getBoth());
        tsFee.setFeePayer(request.getFeePayer());
        tsFee.setAppliedFeeMethod(request.getAppliedFeeMethod());
        tsFee.setCreatedDate(new Date());
        tsFeeRepository.save(tsFee);

        // Insert the fee-feestakeholder mapping table

        List<TsFeeFeeStakeholderMap> tsFeeFeeStakeholderMaps = new ArrayList<>();


        for (TsFeeStakeholder tsFeeStakeholder : tsFeeStakeholders) {

            TsFeeFeeStakeholderMap tsFeeFeeStakeholderMap = new TsFeeFeeStakeholderMap();
            tsFeeFeeStakeholderMap.setStakeholderId(tsFeeStakeholder.getId());
            tsFeeFeeStakeholderMap.setFeeProductCode(tsFee.getFeeCode());
            tsFeeFeeStakeholderMap.setStakeholderPercentage(getStakeHolderPercentage(tsFeeStakeholder.getGlCode(), request.getStakeHolders()));
            tsFeeFeeStakeholderMap.setCreatedDate(new Date());
            tsFeeFeeStakeholderMaps.add(tsFeeFeeStakeholderMap);
        }

        tsFeeFeeStakeholderMapRepository.saveAll(tsFeeFeeStakeholderMaps);


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getFee(GetFeeRequest request) throws PocketException {
        List<TsFee> feeList = (List<TsFee>) tsFeeRepository.findAll();

        List<FeeListItem> feeListItems = new ArrayList<>();

        for (TsFee tempFee : feeList) {
            FeeListItem feeListItem = new FeeListItem();
            feeListItem.setFeeName(tempFee.getFeeName());
            feeListItem.setFeeCode(tempFee.getFeeCode());
            feeListItem.setFeePayer(tempFee.getFeePayer());
            feeListItem.setFixedFee(tempFee.getFixedFee());
            feeListItem.setPercentageFee(tempFee.getPercentageFee());
            feeListItem.setAppliedFeeMethod(tempFee.getAppliedFeeMethod());
            feeListItem.setBoth(tempFee.getBoth());


            List<FeeStakeHolderItem> stakeHolderItems = new ArrayList<>();


            List<TsFeeFeeStakeholderMap> stakeholderMaps = tsFeeFeeStakeholderMapRepository.findAllByFeeProductCode(tempFee.getFeeCode());

            for (TsFeeFeeStakeholderMap tempMap : stakeholderMaps) {

                TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository
                        .findFirstById(tempMap.getStakeholderId());

                FeeStakeHolderItem feeStakeHolderItem = new FeeStakeHolderItem();
                feeStakeHolderItem.setGlCode(tsFeeStakeholder.getGlCode());
                feeStakeHolderItem.setStakeholderName(tsFeeStakeholder.getStakeholderName());
                feeStakeHolderItem.setPercentage(tempMap.getStakeholderPercentage());
                stakeHolderItems.add(feeStakeHolderItem);
            }
            feeListItem.setStakeHolders(stakeHolderItems);

            feeListItems.add(feeListItem);
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(feeListItems);

        return baseResponseObject;

    }

    @Override
    public BaseResponseObject createStakeHolder(CreateStakeHolder request) throws PocketException {

        // check stakeholder name
        // check gl code

        TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository.findFirstByStakeholderName(request.getStakeholderName());
        if (tsFeeStakeholder != null) {
            logWriterUtility.error(request.getRequestId(), "Stakeholder already exists");
            throw new PocketException(PocketErrorCode.StakeHolderAlreadyExists);
        }

        tsFeeStakeholder = tsStakeHolderRepository.findFirstByGlCode(request.getGlCode());
        if (tsFeeStakeholder != null) {
            logWriterUtility.error(request.getRequestId(), "Stakeholder gl already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        TsGl tsGl = tsGlRepository.findFirstByGlCode(request.getGlCode());
        if (tsGl != null) {
            logWriterUtility.error(request.getRequestId(), "Gl already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        tsFeeStakeholder = new TsFeeStakeholder();
        tsFeeStakeholder.setStakeholderName(request.getStakeholderName());
        tsFeeStakeholder.setGlCode(request.getGlCode());
        tsFeeStakeholder.setCreatedDate(new Date());
        tsStakeHolderRepository.save(tsFeeStakeholder);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getStakeHolder(GetStakeHolderRequest request) {
        List<TsFeeStakeholder> stakeholders = (List<TsFeeStakeholder>) tsStakeHolderRepository.findAll();
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(stakeholders);

        return baseResponseObject;
    }

    /*@Override
    public BaseResponseObject createStakeHolderFeeProductMapping(CreateStakeHolderFeeMapRequest request) throws PocketException {

        //check fee exists
        // check stakeholder exists


        TsFee tsFee = tsFeeRepository.findFirstById(request.getFeeId());

        if (tsFee == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee id");
            throw new PocketException(PocketErrorCode.InvalidFee);
        }

        List<TsFeeFeeStakeholderMap> mapList = new ArrayList<>();

        for (FeeStakeHolderItem item : request.getStakeHolderItems()) {
            TsFeeStakeholder tsStakeHolder = tsStakeHolderRepository.findFirstById(item.getStakeHolderId());
            if (tsStakeHolder == null) {
                logWriterUtility.error(request.getRequestId(), "Invalid stakeholder id");
                throw new PocketException(PocketErrorCode.InvalidStakeholder);
            }

            TsFeeFeeStakeholderMap tsFeeFeeStakeholderMap = new TsFeeFeeStakeholderMap();
            tsFeeFeeStakeholderMap.setStakeholderPercentage(item.getPercentage());
            tsFeeFeeStakeholderMap.setCreatedDate(new Date());
            tsFeeFeeStakeholderMap.setFeeProductCode(tsFee.getFeeCode());
            tsFeeFeeStakeholderMap.setStakeholderId(tsStakeHolder.getId());
            mapList.add(tsFeeFeeStakeholderMap);

        }
        tsFeeFeeStakeholderMapRepository.saveAll(mapList);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }*/

    @Override
    public BaseResponseObject createFeeFeeProfileServiceMap(CreateFeeProfileLimitMapRequest request) throws PocketException {

        TsFee tsFee = tsFeeRepository.findFirstByFeeCode(request.getFeeCode());

        if (tsFee == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee id");
            throw new PocketException(PocketErrorCode.InvalidFee);
        }

        TsFeeProfile tsFeeProfile = tsFeeProfileRepository.findFirstByProfileCode(request.getFeeProfileCode());

        if (tsFeeProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee profile code");
            throw new PocketException(PocketErrorCode.InvalidFeeProfileCode);
        }


        TsService tsService = tsServiceRepository.findFirstByServiceCode(request.getServiceCode());

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee profile id");
            throw new PocketException(PocketErrorCode.InvalidServiceCode);
        }


        TsFeeProfileLimitMapping tsFeeProfileLimitMapping = tsFeeProfileLimitMappingRepository.findFirstByFeeCodeAndServiceCodeAndFeeProfileCode(tsFee.getFeeCode(), tsService.getServiceCode(), tsFeeProfile.getProfileCode());

        if (tsFeeProfileLimitMapping != null) {
            logWriterUtility.error(request.getRequestId(), "Map already exists");
            throw new PocketException(PocketErrorCode.MapAlreadyExists);
        }

        tsFeeProfileLimitMapping=new TsFeeProfileLimitMapping();

        tsFeeProfileLimitMapping.setFeeCode(tsFee.getFeeCode());
        tsFeeProfileLimitMapping.setServiceCode(tsService.getServiceCode());
        tsFeeProfileLimitMapping.setFeeProfileCode(tsFeeProfile.getProfileCode());
        tsFeeProfileLimitMapping.setCreatedDate(new Date());

        tsFeeProfileLimitMappingRepository.save(tsFeeProfileLimitMapping);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    private double getStakeHolderPercentage(String glCode, List<FeeStakeHolderItem> stakeHolders) {

        for (FeeStakeHolderItem item : stakeHolders) {
            if (item.getGlCode().equals(glCode)) {
                return item.getPercentage();
            }
        }

        return 0;
    }
}
