package tech.ipocket.pocket.services.um.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.request.um.OtpVerificationRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.utils.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private UserGroupRepository userGroupRepository;
    @Autowired
    private UserOTPRepository otpRepository;
    @Autowired
    private UserDeviceRepository userDeviceRepository;
    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserActivityLogRepository activityLogRepository;
    @Autowired
    private UserSettingsRepository userSettingsRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private Environment environment;
    @Autowired
    private AppConfiguration appConfiguration;


    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse customerRegistration(CustomerRegistration customerRegistration) throws PocketException {

        validateCustomerData(customerRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findByMobileNumber(customerRegistration.getMobileNumber());
        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCode(customerRegistration.getGroupCode());
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // set user information
        if (userInfo == null)
            userInfo = new UmUserInfo();
        userInfo.setFullName(customerRegistration.getFullName());
        userInfo.setMobileNumber(customerRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(customerRegistration.getPassword()));
        userInfo.setGroupCode(customerRegistration.getGroupCode());
        userInfo.setLoginId(customerRegistration.getMobileNumber());
        userInfo.setAccountStatus(AccountStatus.INITIATION.getCODE());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setPrimaryIdNumber(customerRegistration.getPrimaryIdNumber());
        userDetails.setPrimaryIdIssueDate(customerRegistration.getPrimaryIdIssueDate());
        userDetails.setPrimaryIdExpiryDate(customerRegistration.getPrimaryIdExpiryDate());
        userDetails.setPrimaryIdType(customerRegistration.getPrimaryIdType());
        userDetails.setNationality(customerRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetailsRepository.save(userDetails);

        // save documents

        // set for otp
        String otpText = RandomGenerator.getInstance().generatePIN(4);
        String secret = RandomGenerator.getInstance().generateTokenAsString(12);
        UmOtp otp = new UmOtp();
        otp.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        otp.setOtpSecret(secret);
        otp.setOtpText(otpText);
        otp.setUmUserInfoByUserId(newUserInfo);
        otp.setStatus(String.valueOf(AccountStatus.INITIATION.getCODE()));
        otp.setLoginId(newUserInfo.getMobileNumber());
        UmOtp newOtp = otpRepository.save(otp);

        // save user device info
        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(customerRegistration.getDeviceName());
        userDevice.setHardwareSignature(customerRegistration.getHardwareSignature());
        userDevice.setMetaData(customerRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);


        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(customerRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_REGISTRATION.getKeyString());
        log.setMetaData(customerRegistration.getMetaData());
        log.setMobileNumber(customerRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);

        CompletableFuture.runAsync(() -> {
            String msg = "Your PIN number is : " + newOtp.getOtpText();
            new ExternalCall(customerRegistration.getMobileNumber(),
                    "", msg, ExternalType.SMS, restTemplate, customerRegistration.getRequestId(),
                    appConfiguration.isExternalEnable())
                    .start();
        });

        // send response
        registrationResponse.setDeviceId(newDevice.getId());
        registrationResponse.setOtpId(newOtp.getId());
        registrationResponse.setOtpSecret(secret);
        registrationResponse.setRegistrationDate(new Date());

        return registrationResponse;
    }


    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse merchantAgentRegistration(MerchantAgentRegistration merchantAgentRegistration) throws PocketException {

        validateAgentMerchantData(merchantAgentRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findByMobileNumber(merchantAgentRegistration.getMobileNumber());
        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCode(merchantAgentRegistration.getGroupCode());
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // TS for create wallet
        WalletCreateRequest walletCreateRequest = new WalletCreateRequest();
        walletCreateRequest.setFullName(merchantAgentRegistration.getFullName());
        walletCreateRequest.setGroupCode(merchantAgentRegistration.getGroupCode());
        walletCreateRequest.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        walletCreateRequest.setUserStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        walletCreateRequest.setHardwareSignature(merchantAgentRegistration.getHardwareSignature());
        walletCreateRequest.setRequestId(merchantAgentRegistration.getRequestId());


        if (!WalletClass.walletCreate(walletCreateRequest,environment,restTemplate)) {
            throw new PocketException(PocketErrorCode.WALLET_CREATE_EXCEPTION);
        }


        // insert user info
        if (userInfo == null)
            userInfo = new UmUserInfo();
        userInfo.setFullName(merchantAgentRegistration.getFullName());
        userInfo.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(merchantAgentRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(merchantAgentRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(merchantAgentRegistration.getGroupCode());
        userInfo.setLoginId(merchantAgentRegistration.getMobileNumber());
        userInfo.setAccountStatus(UmEnums.UserStatus.Verified.getUserStatusType());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setPrimaryIdNumber(merchantAgentRegistration.getPrimaryIdNumber());
        userDetails.setPrimaryIdIssueDate(merchantAgentRegistration.getPrimaryIdIssueDate());
        userDetails.setPrimaryIdExpiryDate(merchantAgentRegistration.getPrimaryIdExpiryDate());
        userDetails.setPrimaryIdType(merchantAgentRegistration.getPrimaryIdType());
        userDetails.setNationality(merchantAgentRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetailsRepository.save(userDetails);

        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.TRUE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
//        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setPrimaryIdVerified(Boolean.TRUE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);

        // save user device info
        /*UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(merchantAgentRegistration.getDeviceName());
        userDevice.setHardwareSignature(merchantAgentRegistration.getHardwareSignature());
        userDevice.setMetaData(merchantAgentRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(newUserInfo);
        mapping.setUmUserDeviceByDeviceId(newDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);*/

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(merchantAgentRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.MERCHANT_OR_AGENT_REGISTRATION.getKeyString());
        log.setMetaData(merchantAgentRegistration.getMetaData());
        log.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);


        String msg = "Your registration is complete. Please login with default" +
                "mobile number and password";
        new ExternalCall(merchantAgentRegistration.getMobileNumber(),
                "", msg, ExternalType.SMS, restTemplate, merchantAgentRegistration.getRequestId(),
                appConfiguration.isExternalEnable())
                .start();

        msg = "Hi, " + merchantAgentRegistration.getFullName() + "," +
                "Congratulation, you account has been create successfully.\nPlease " +
                "login with default mobile number and password";
        new ExternalCall(merchantAgentRegistration.getEmail(), "Registration Confirmation",
                msg, ExternalType.EMAIL, restTemplate, merchantAgentRegistration.getRequestId(),
                appConfiguration.isExternalEnable())
                .start();


        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        return registrationResponse;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse adminRegistration(AdminRegistration adminRegistration) throws PocketException {

        validateAdminData(adminRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findFirstByEmail(adminRegistration.getEmail());

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCode(adminRegistration.getGroupCode());
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // insert user info
        if (userInfo == null)
            userInfo = new UmUserInfo();

        userInfo.setFullName(adminRegistration.getFullName());
        userInfo.setMobileNumber(adminRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(adminRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(adminRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(adminRegistration.getGroupCode());
        userInfo.setLoginId(adminRegistration.getEmail());
        userInfo.setAccountStatus(AccountStatus.VERIFIED.getCODE());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setNationality(adminRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetailsRepository.save(userDetails);


        // save user device info
        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(adminRegistration.getDeviceName());
        userDevice.setHardwareSignature(adminRegistration.getHardwareSignature());
        userDevice.setMetaData(adminRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(newUserInfo);
        mapping.setUmUserDeviceByDeviceId(newDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);

        // user settings
        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.FALSE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);


        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(adminRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.ADMIN_REGISTRATION.getKeyString());
        log.setMobileNumber(adminRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);

        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        CompletableFuture.runAsync(() -> {
            String msg = "Hi, " + adminRegistration.getFullName() + "," +
                    "Congratulation, you account has been create successfully.\nPlease " +
                    "login with default email and password";
            new ExternalCall(adminRegistration.getEmail(), "Registration Confirmation",
                    msg, ExternalType.EMAIL, restTemplate, adminRegistration.getRequestId(),
                    appConfiguration.isExternalEnable())
                    .start();
        });


        return registrationResponse;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public BaseResponseObject doVerifyUser(OtpVerificationRequest otpVerificationRequest) throws PocketException {

        // TODO: 2019-05-09 work for reversal with ts during failed in um

        UmOtp otp = otpRepository.findFirstByOtpTextAndOtpSecret(otpVerificationRequest.getOtpText(), otpVerificationRequest.getOtpSecret());
        if (otp == null) {
            throw new PocketException(PocketErrorCode.OtpNotMatched);
        }

        UmUserInfo umUserInfo = otp.getUmUserInfoByUserId();

        // create wallet
        // TS for create wallet
        WalletCreateRequest walletCreateRequest = new WalletCreateRequest();
        walletCreateRequest.setFullName(umUserInfo.getFullName());
        walletCreateRequest.setGroupCode(umUserInfo.getGroupCode());
        walletCreateRequest.setMobileNumber(umUserInfo.getMobileNumber());
//        walletCreateRequest.setUserStatus(String.valueOf(umUserInfo.getAccountStatus()));
        walletCreateRequest.setUserStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        walletCreateRequest.setHardwareSignature(otpVerificationRequest.getHardwareSignature());
        walletCreateRequest.setRequestId(otpVerificationRequest.getRequestId());


        if (!WalletClass.walletCreate(walletCreateRequest,environment, restTemplate)) {
            throw new PocketException(PocketErrorCode.WALLET_CREATE_EXCEPTION);
        }


        otp.setStatus(String.valueOf(AccountStatus.VERIFIED.getCODE()));
        otpRepository.save(otp);


        // todo have to replace code here
        // add user settings data
        UmUserSettings userSettings = new UmUserSettings();
        userSettings.setMobileNumberVerified(true);
//        userSettings.setPrimaryIdVerified(false);
        userSettings.setPrimaryIdVerified(true);
        userSettings.setUmUserInfoByUserId(umUserInfo);
        userSettingsRepository.save(userSettings);

        // update user info data status = verify
        umUserInfo.setAccountStatus(AccountStatus.VERIFIED.getCODE());
        userInfoRepository.save(umUserInfo);

        // check device already exists or not
        UmUserDevice userDevice = userDeviceRepository.findFirstByHardwareSignature(otpVerificationRequest.getHardwareSignature());
        if (userDevice == null) {
            logWriterUtility.error(otpVerificationRequest.getRequestId(), "Device not found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(umUserInfo);
        mapping.setUmUserDeviceByDeviceId(userDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(otpVerificationRequest.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_OTP_VERIFICATION_AFTER_REGISTRATION.getKeyString());
        log.setMobileNumber(umUserInfo.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(umUserInfo);

        activityLogRepository.save(log);

        String msg= "Your account activation done";
        new ExternalCall(umUserInfo.getMobileNumber(), "SMS", msg, ExternalType.SMS, restTemplate, otpVerificationRequest.getRequestId(),
                appConfiguration.isExternalEnable())
                .start();
        msg = "Hi " + umUserInfo.getFullName() + ",\nYour account is create successfully. Now you can login with your default " +
                "mobile number and pin";
        new ExternalCall(umUserInfo.getEmail(), "Account Create", msg, ExternalType.EMAIL, restTemplate, otpVerificationRequest.getRequestId(),
                appConfiguration.isExternalEnable())
                .start();

        BaseResponseObject baseResponseObject = new BaseResponseObject(otpVerificationRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(otpVerificationRequest.getRequestId());
        baseResponseObject.setStatus("200");
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    private void validateCustomerData(CustomerRegistration customerRegistration) throws PocketException {
        check(customerRegistration.getMobileNumber(), customerRegistration.getPrimaryIdNumber(), customerRegistration.getPrimaryIdType(), customerRegistration.getPrimaryIdIssueDate(), customerRegistration.getPrimaryIdExpiryDate(), customerRegistration.getNationality());
    }

    private void validateAgentMerchantData(MerchantAgentRegistration merchantAgentRegistration) throws PocketException {
        check(merchantAgentRegistration.getMobileNumber(), merchantAgentRegistration.getPrimaryIdNumber(), merchantAgentRegistration.getPrimaryIdType(), merchantAgentRegistration.getPrimaryIdIssueDate(), merchantAgentRegistration.getPrimaryIdExpiryDate(), merchantAgentRegistration.getNationality());
    }

    private void check(String mobileNumber, String primaryIdNumber, String primaryIdType, java.sql.Date primaryIdIssueDate, java.sql.Date primaryIdExpiryDate, String nationality) throws PocketException {
        if (mobileNumber == null || mobileNumber.equals("")) {
            throw new PocketException(PocketErrorCode.MobileNumberRequired);
        }
        if (primaryIdNumber == null || primaryIdNumber.equals("")) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberRequired);
        }

        if (primaryIdType == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberTypeRequired);
        }

        if (primaryIdIssueDate == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberIssueDateRequired);
        }
        if (primaryIdExpiryDate == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberExpiryDateRequired);
        }
        if (nationality == null || nationality.equals("")) {
            throw new PocketException(PocketErrorCode.NationalityRequired);
        }
    }


    private void validateAdminData(AdminRegistration adminRegistration) throws PocketException {
        if (adminRegistration.getEmail() == null || adminRegistration.getEmail().equals("")) {
            throw new PocketException(PocketErrorCode.EmailRequired);
        }
        if (adminRegistration.getNationality() == null || adminRegistration.getNationality().equals("")) {
            throw new PocketException(PocketErrorCode.NationalityRequired);
        }

    }
}
