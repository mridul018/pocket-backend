package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.CreateBankRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface BankService {
    BaseResponseObject createBank(CreateBankRequest request) throws PocketException;

    BaseResponseObject getBanks(EmptyRequest request);
}
