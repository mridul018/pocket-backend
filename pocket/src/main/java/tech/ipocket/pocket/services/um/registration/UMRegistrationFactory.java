package tech.ipocket.pocket.services.um.registration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.utils.GroupCodes;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@Component
public class UMRegistrationFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public RegistrationResponse doRegistration(RegistrationRequest registrationRequest, String type) throws PocketException {
       switch (type) {
            case GroupCodes.CUSTOMER:
                CustomerRegistration customerRegistration = new CustomerRegistration();
                customerRegistration.setData(registrationRequest);
                RegistrationService registrationService = applicationContext.getBean(RegistrationServiceImpl.class);
                return registrationService.customerRegistration(customerRegistration);
            case GroupCodes.AGENT:
                return registration(registrationRequest);
            case GroupCodes.MERCHANT:
                return registration(registrationRequest);
            case GroupCodes.ADMIN:
            case GroupCodes.SUPER_ADMIN:
                AdminRegistration adminRegistration = new AdminRegistration();
                adminRegistration.setData(registrationRequest);
                RegistrationService adminRegistrationService = applicationContext.getBean(RegistrationServiceImpl.class);
                return adminRegistrationService.adminRegistration(adminRegistration);

            default:
                throw new PocketException(PocketErrorCode.UnSupportedUserType);
        }
    }

    private RegistrationResponse registration(RegistrationRequest request) throws PocketException {
        MerchantAgentRegistration merchantAgentRegistration = new MerchantAgentRegistration();
        merchantAgentRegistration.setData(request);
        RegistrationService registrationService = applicationContext.getBean(RegistrationServiceImpl.class);
        return registrationService.merchantAgentRegistration(merchantAgentRegistration);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
