package tech.ipocket.pocket.services.um;

import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface TransactionValidityService {
    BaseResponseObject umCheckTransactionValidity(TransactionValidityUmRequest request) throws PocketException;
}
