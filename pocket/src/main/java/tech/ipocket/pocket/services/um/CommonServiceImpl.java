package tech.ipocket.pocket.services.um;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmActivityLog;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserActivityLogRepository;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.UmEnums;

import java.util.Date;

@Service
public class CommonServiceImpl implements CommonService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserActivityLogRepository activityLogRepository;

    @Override
    public void addInteractionLog(String loginId, String message,
                                  String hardwareSignature, String deviceName, String logType,
                                  UmUserInfo userInfo) {
        UmActivityLog umActivityLog = new UmActivityLog();
        umActivityLog.setCreatedDate(new Date());
        umActivityLog.setLogType(logType);
        umActivityLog.setDeviceInfo(hardwareSignature);
        umActivityLog.setMobileNumber(loginId);
        umActivityLog.setMetaData(message);
        umActivityLog.setStatus(UmEnums.ActivityLogStatus.Initialize.getActivityLogStatus());
        umActivityLog.setUmUserInfoByUserId(userInfo);

        activityLogRepository.save(umActivityLog);

    }
}
