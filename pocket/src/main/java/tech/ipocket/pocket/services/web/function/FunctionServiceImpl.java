package tech.ipocket.pocket.services.web.function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.web.FunctionRequest;
import tech.ipocket.pocket.response.web.FunctionListResponse;
import tech.ipocket.pocket.response.web.FunctionResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private UserFunctionRepository userFunctionRepository;

    @Autowired
    private UserGroupFunctionMapRepository userGroupFunctionMapRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Override
    public FunctionResponse addUpdateDeleteFunction(FunctionRequest request) throws PocketException {
        FunctionResponse response = new FunctionResponse();

        UmUserFunction userFunction = userFunctionRepository
                .findByFunctionCode(request.getFunctionCode());
        if (userFunction == null) {
            userFunction = new UmUserFunction();
            userFunction.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            userFunction.setDescription(request.getDescription());
            userFunction.setFunctionCode(request.getFunctionCode());
            userFunction.setFunctionName(request.getFunctionName());
            userFunction.setFunctionStatus(request.getFunctionStatus());
        } else {
            userFunction.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            userFunction.setDescription(request.getDescription());
            userFunction.setFunctionCode(request.getFunctionCode());
            userFunction.setFunctionName(request.getFunctionName());
            userFunction.setFunctionStatus(request.getFunctionStatus());
        }

        UmUserFunction newUserFunction = userFunctionRepository.saveAndFlush(userFunction);
        if (userFunction != null && newUserFunction.getId() > 0) {
            response.setFunctionId(newUserFunction.getId());
            response.setMessage("Function operation successfully done");
        } else {
            response.setFunctionId(0);
            response.setMessage("Function operation failed");
        }


        return response;
    }

    @Override
    public FunctionListResponse listOfAllFunctions() throws PocketException {
        FunctionListResponse response = null;

        List<UmUserFunction> all = userFunctionRepository.findAll();
        if (all != null) {
            response = new FunctionListResponse();
            response.setFunctions(all);
        }

        return response;
    }

    @Override
    public FunctionListResponse getFunctionsByGroupCode(String groupCode) throws PocketException {
        FunctionListResponse response;
        List<UmGroupFunctionMapping> groupFunctionMappings = userGroupFunctionMapRepository
                .findAllByGroupCodeAndStatus(groupCode, "1");

        if (groupFunctionMappings == null || groupFunctionMappings.size() == 0) {
            throw new PocketException(PocketErrorCode.NoFunctionAssignedForThisGroup);
        }

        List<String> functionCodesList = groupFunctionMappings.stream()
                .map(UmGroupFunctionMapping::getFunctionCode)
                .collect(Collectors.toList());

        List<UmUserFunction> functionList = userFunctionRepository.findAllByFunctionCodeIn(functionCodesList);

        response = new FunctionListResponse();
        response.setFunctions(functionList);
        return response;
    }
}
