package tech.ipocket.pocket.services.um;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.DocumentItem;
import tech.ipocket.pocket.request.um.GetUserInfoRequest;
import tech.ipocket.pocket.request.um.UpdateDocumentRequest;
import tech.ipocket.pocket.request.um.UpdateFcmKeyRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.DeviceListItem;
import tech.ipocket.pocket.response.um.DeviceListResponseRoot;
import tech.ipocket.pocket.response.um.UserDetailsResponseResponse;
import tech.ipocket.pocket.utils.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserDeviceRepository userDeviceRepository;

    @Autowired
    private UserDocumentRepository userDocumentRepository;

    @Autowired
    private Environment environment;
    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Override
    public TokenValidateUmResponse getUserInfo(GetUserInfoRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getMobileNumber(), null);

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        TokenValidateUmResponse validateResponse = new TokenValidateUmResponse();
        validateResponse.setGroupCode(umUserInfo.getGroupCode());
        validateResponse.setMobileNumber(umUserInfo.getMobileNumber());
        validateResponse.setName(umUserInfo.getFullName());
        validateResponse.setPhotoId("" + umUserInfo.getPhotoId());
        return validateResponse;
    }

    @Override
    public BaseResponseObject getUserDevices(TokenValidateUmResponse validateResponse, EmptyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumber(validateResponse.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        List<String> statusIn = new ArrayList<>();
        statusIn.add(UmEnums.DeviceMapStatus.Active.getMapStatus());
        List<UmUserDeviceMapping> maps = userDeviceMapRepository.findAllByUmUserInfoByUserIdAndStatusIn(umUserInfo, statusIn);

        if (maps == null || maps.size() == 0) {
            return baseResponseObject;
        }

        DeviceListResponseRoot listResponseRoot = new DeviceListResponseRoot();

        maps.forEach(map -> {
            DeviceListItem deviceListItem = new DeviceListItem();
            deviceListItem.setDeviceName(map.getUmUserDeviceByDeviceId().getDeviceName());
            deviceListItem.setHardwareSignature(map.getUmUserDeviceByDeviceId().getHardwareSignature());
            listResponseRoot.getDevices().add(deviceListItem);
        });
        baseResponseObject.setData(listResponseRoot);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateFcmKey(TokenValidateUmResponse validateResponse, UpdateFcmKeyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumber(validateResponse.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        UmUserDevice umUserDevice = userDeviceRepository.findFirstByHardwareSignature(request.getHardwareSignature());
        if (umUserDevice == null) {
            logWriterUtility.error(request.getRequestId(), "User device found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        UmUserDeviceMapping userDeviceMap = userDeviceMapRepository.findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(umUserInfo, umUserDevice, UmEnums.DeviceMapStatus.Active.getMapStatus());

        if (userDeviceMap == null) {
            logWriterUtility.error(request.getRequestId(), "User device found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        userDeviceMap.setFcmKey(request.getFcmKey());
        userDeviceMapRepository.save(userDeviceMap);

        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateUserDocuments(TokenValidateUmResponse validateResponse, UpdateDocumentRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumber(request.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        for (DocumentItem documentItem : request.getDocumentList()) {

            UmDocuments umDocuments = new UmDocuments();

            if (documentItem.getDocumentType().equals(UmEnums.DocumentTypes.ProfilePicture.getDocumentType())) {
                String documentPath = saveDocumentsToDisk(documentItem.getDocumentAsString(), ".png", request.getRequestId());
                umDocuments.setCreatedDate(new Date());
                umDocuments.setDocumentType(documentItem.getDocumentType());
                umDocuments.setStatus("1");
                umDocuments.setUmUserInfoByUserId(umUserInfo);
                umDocuments.setDocumentPath(documentPath);
                userDocumentRepository.save(umDocuments);

                umUserInfo.setPhotoId(umDocuments.getId());
                userInfoRepository.save(umUserInfo);
            }
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getUserDetails(GetUserInfoRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumber(request.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        UserDetailsResponseResponse response=new UserDetailsResponseResponse();

        response.setUserId(umUserInfo.getId());
        response.setFullName(umUserInfo.getFullName());
        response.setMobileNumber(umUserInfo.getMobileNumber());
        response.setGroupCode(umUserInfo.getGroupCode());

        UmDocuments umDocuments=userDocumentRepository.findLastByUmUserInfoByUserIdAndDocumentType(umUserInfo,UmEnums.DocumentTypes.ProfilePicture.getDocumentType());
        if(umDocuments!=null){
            response.setPhotoUrl(umDocuments.getDocumentPath());
        }

        UmUserSettings umUserSettings=userSettingsRepository.findFirstByUmUserInfoByUserId(umUserInfo);
        if(umUserSettings!=null){
            response.setPrimaryIdVerified(umUserSettings.getPrimaryIdVerified());
            response.setMobileNumberVerified(umUserSettings.getMobileNumberVerified());
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(response);
        return baseResponseObject;
    }

    private String saveDocumentsToDisk(String document, String extension, String requestId) throws PocketException{
        String target = environment.getProperty("file_base_path");
        String fileName =  System.currentTimeMillis() + "." + extension;

        String fileBasePath = environment.getProperty("file_base_path");

        File file = new File(target);
        if (!file.exists()) {
            System.out.print("No Directory");
            file.mkdir();
            System.out.print("Folder created");
        } else {
            logWriterUtility.debug(requestId, " Exists :" + target + "\".");
        }
        String documentPath = "" + new Date().getTime() + "." + extension;

        byte[] imageByte = Base64.decodeBase64(document);

        String directory = target + "/" + fileName;

        logWriterUtility.debug(requestId, "Saving File :" + directory);

        try {
            // TODO: 7/24/18 Have to check null value
            new FileOutputStream(directory).write(imageByte);
        } catch (IOException e) {
            logWriterUtility.error(requestId, e);
            throw new PocketException(PocketErrorCode.IOException, e.getMessage());
        }

        return fileName;
    }
}
