package tech.ipocket.pocket.services.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.*;

import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class ExternalServiceImpl implements ExternalService {

    LogWriterUtility logWriterUtility = new LogWriterUtility(ExternalServiceImpl.class);

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private SMSConfig smsConfig;

    private ScheduledExecutorService scheduledExecutorService;

    @Autowired
    private Environment environment;

    @Override
    public ExternalResponse sendEmail(ExternalRequest request) throws PocketException {

        ExternalResponse response = new ExternalResponse();

        /*JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setPort(emailConfig.getPort());
        mailSender.setHost(emailConfig.getHost());
        mailSender.setUsername(emailConfig.getName());
        mailSender.setPassword(emailConfig.getPassword());
        mailSender.setJavaMailProperties(getProperties());
        mailSender.setSession(getSession());

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(emailConfig.getName());
        mailMessage.setTo(request.getTo());
        mailMessage.setSubject(request.getSubject());
        mailMessage.setText(request.getBody());

        try {
            mailSender.send(mailMessage);
            response.setError(null);
            response.setAbleToSend(true);
            response.setData("Email send successfully");
            response.setStatus(PocketConstants.OK);
        }catch (Exception ex){
            response.setError(new ErrorObject(PocketErrorCode.EMAIL_NOT_SEND));
            response.setAbleToSend(false);
            response.setData("Email not able to send");
            response.setStatus(PocketConstants.ERROR);
        }*/

        return response;

    }


    @Override
    public ExternalResponse sendSMS(ExternalRequest request) throws PocketException {

        ExternalResponse response = new ExternalResponse();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(smsConfig.getUsername(), smsConfig.getPassword());


        SmsSendRequest smsSendRequest = new SmsSendRequest();
        smsSendRequest.setFrom(smsConfig.getFrom());
        smsSendRequest.setTo(request.getTo());
        smsSendRequest.setText(request.getBody());



        String resp= (String) new RestCallerTask()
                .callToRestService(HttpMethod.POST,smsConfig.getUrl(),
                        smsSendRequest,String.class,request.getRequestId());

        if (resp != null) {
            response.setError(null);
            response.setAbleToSend(true);
            response.setData("SMS send successfully");
            response.setStatus(PocketConstants.OK);
        } else {
            response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
            response.setAbleToSend(false);
            response.setData("SMS not send");
            response.setStatus(PocketConstants.ERROR);
        }


        return response;
    }

    @Override
    public ExternalResponse sendNotification(ExternalRequest request) throws PocketException {

        return null;
    }

    @Override
    public TransactionSummaryViewResponse callTxForTransactionVisibility(TransactionValidityRequest transactionSummaryRequest) throws PocketException {

        String url=environment.getProperty("GET_TX_VALIDITY");

        TransactionSummaryViewResponse response= null;
        try {
            response = (TransactionSummaryViewResponse) new RestCallerTask()
                    .callToRestService(HttpMethod.POST,url,transactionSummaryRequest,
                            TransactionSummaryViewResponse.class,transactionSummaryRequest.getRequestId());
            if(response==null){
                logWriterUtility.error(transactionSummaryRequest.getRequestId(),"Response null from TS");
                return null;
            }
            return response;
        } catch (PocketException e) {
            logWriterUtility.error(transactionSummaryRequest.getRequestId(),e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean callTxForUpdateClientStatus(UmUpdateClientStatusRequest updateClientStatusRequest) throws PocketException {

        String url=environment.getProperty("TS_UPDATE_CLIENT_STATUS");

        SuccessBoolResponse successBoolResponse= null;
        try {
            successBoolResponse = (SuccessBoolResponse) new RestCallerTask()
                    .callToRestService(HttpMethod.POST,url,updateClientStatusRequest,
                    SuccessBoolResponse.class,updateClientStatusRequest.getRequestId());
            if(successBoolResponse==null){
                logWriterUtility.error(updateClientStatusRequest.getRequestId(),"Response null from TS");
                return false;
            }
            return successBoolResponse.isResult();
        } catch (PocketException e) {
            logWriterUtility.error(updateClientStatusRequest.getRequestId(),e.getMessage());
            return false;
        }
    }
}
