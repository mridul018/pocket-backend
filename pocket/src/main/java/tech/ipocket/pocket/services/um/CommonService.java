package tech.ipocket.pocket.services.um;

import tech.ipocket.pocket.entity.UmUserInfo;

public interface CommonService {
    void addInteractionLog(String loginId, String message, String hardwareSignature, String deviceName,
                           String logType, UmUserInfo userInfo);
}
