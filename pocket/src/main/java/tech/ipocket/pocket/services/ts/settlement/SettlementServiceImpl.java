package tech.ipocket.pocket.services.ts.settlement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.settlement.AdminCashInToCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.MasterDepositFromBankGlRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.utils.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class SettlementServiceImpl implements SettlementService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TsBankRepository tsBankRepository;
    @Autowired
    private TsGlRepository tsGlRepository;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private FeeManager feeManager;
    @Autowired
    private AccountService clientService;

    @Autowired
    private TransactionValidator transactionValidator;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;

    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    @Override
    public BaseResponseObject depositToMasterFromBankGl(MasterDepositFromBankGlRequest request) throws PocketException {
        // Load CfeClient for Master

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.Master_Wallet_Deposit);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        // Check sender
        TsBanks cfeBankDetail = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (cfeBankDetail == null) {
            logWriterUtility.error(request.getRequestId(), "TsBanks not found .");
            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        TsGl senderGl = tsGlRepository.findFirstByGlCode(cfeBankDetail.getGlCode());


        if (senderGl == null) {
            logWriterUtility.error(request.getRequestId(), "senderGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }


        //calculate fee
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                request.getAmount(), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                request.getAmount());

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeBankDetail.getGlCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_FundMovement,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        // TODO: 2019-04-22 send notification

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    @Override
    public BaseResponseObject adminCashInToCustomer(AdminCashInToCustomerRequest request) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                request.getReceiverWallet(), statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");


        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.AdminCashIn);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }


        //calculate fee
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                request.getAmount(), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                request.getAmount());

        // TODO: 2019-04-23 balance check

        //balance validation
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }


        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData);
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Customer,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_FundMovement,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        // TODO: 2019-04-22 send notification

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

}
