package tech.ipocket.pocket.services.external;

import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface ExternalService {

    ExternalResponse sendEmail(ExternalRequest request) throws PocketException;

    ExternalResponse sendSMS(ExternalRequest request) throws PocketException;

    ExternalResponse sendNotification(ExternalRequest request) throws PocketException;


    TransactionSummaryViewResponse callTxForTransactionVisibility(TransactionValidityRequest transactionSummaryRequest) throws PocketException;

    Boolean callTxForUpdateClientStatus(UmUpdateClientStatusRequest updateClientStatusRequest) throws PocketException;
}
