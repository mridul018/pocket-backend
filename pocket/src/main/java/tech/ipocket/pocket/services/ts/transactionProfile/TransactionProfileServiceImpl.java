package tech.ipocket.pocket.services.ts.transactionProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsLimit;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.entity.TsTransactionProfile;
import tech.ipocket.pocket.entity.TsTransactionProfileLimitMapping;
import tech.ipocket.pocket.repository.ts.TsLimitRepository;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionProfileLimitMappingRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionProfileRepository;
import tech.ipocket.pocket.request.ts.transactionProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.transactionProfile.TransactionProfileServiceLimitMapItem;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransactionProfileServiceImpl implements TransactionProfileService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsLimitRepository tsLimitRepository;

    @Autowired
    private TsTransactionProfileRepository tsTransactionProfileRepository;

    @Autowired
    private TsTransactionProfileLimitMappingRepository tsTransactionProfileLimitMappingRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;

    @Override
    public BaseResponseObject createLimit(CreateLimitRequest request) throws PocketException {


        TsLimit tsLimit = tsLimitRepository.findFirstByLimitCode(request.getLimitCode());
        if (tsLimit != null) {
            logWriterUtility.error(request.getRequestId(), "Limit code already exists");
            throw new PocketException(PocketErrorCode.LimitCodeAlreadyExists);
        }

        tsLimit = tsLimitRepository.findFirstByLimitName(request.getLimitName());
        if (tsLimit != null) {
            logWriterUtility.error(request.getRequestId(), "Limit name already exists");
            throw new PocketException(PocketErrorCode.LimitNameAlreadyExists);
        }

        tsLimit = new TsLimit();
        tsLimit.setLimitName(request.getLimitName());
        tsLimit.setLimitCode(request.getLimitCode());
        tsLimit.setMinAmount(request.getMinAmount());
        tsLimit.setMaxAmount(request.getMaxAmount());

        tsLimit.setDailyMaxNoOfTransaction(request.getDailyMaxNoOfTransaction());
        tsLimit.setDailyMaxTransactionAmount(request.getDailyMaxTransactionAmount());

        tsLimit.setMontlyMaxNoOfTransaction(request.getMontlyMaxNoOfTransaction());
        tsLimit.setMonthlyMaxTransactionAmount(request.getMonthlyMaxTransactionAmount());

        tsLimitRepository.save(tsLimit);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getLimit(GetLimitRequest request) throws PocketException {

        List<TsLimit> limits=null;
        if(request.getLimitCode()!=null){

            List<String> codes=new ArrayList<>();
            codes.add(request.getLimitCode());

            limits = tsLimitRepository.findAllByLimitCodeIn(codes);
        }else{
            limits = (List<TsLimit>) tsLimitRepository.findAll();
        }
        List<TsLimit> tsLimits = (List<TsLimit>) tsLimitRepository.findAll();

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(tsLimits);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject createTransactionProfile(CreateTransactionProfileRequest request) throws PocketException {

        TsTransactionProfile tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileCode(request.getProfileCode());

        if (tsTransactionProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile code exis");
            throw new PocketException(PocketErrorCode.TransactionProfileCodeExists);
        }

        tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileName(request.getProfileName());

        if (tsTransactionProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile name exis");
            throw new PocketException(PocketErrorCode.TransactionProfileNameExists);
        }

        tsTransactionProfile = new TsTransactionProfile();
        tsTransactionProfile.setProfileCode(request.getProfileCode());
        tsTransactionProfile.setProfileName(request.getProfileName());
        tsTransactionProfile.setCreatedDate(new Date());

        tsTransactionProfileRepository.save(tsTransactionProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTransactionProfile(GetTransactionProfileRequest request) {
        List<TsTransactionProfile> transactionProfiles = (List<TsTransactionProfile>) tsTransactionProfileRepository.findAll();

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(transactionProfiles);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject mapTransactionProfileServiceLimit(CreateTransactionProfileLimitServiceMapRequest request) throws PocketException {

        TsTransactionProfile tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileCode(request.getTxProfileCode());

        if (tsTransactionProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile code invalid");
            throw new PocketException(PocketErrorCode.InvalidTxProfileCode);
        }


        TsLimit tsLimit = tsLimitRepository.
                findFirstByLimitCode(request.getLimitCode());

        if (tsLimit == null) {
            logWriterUtility.error(request.getRequestId(), "Limit code invalid");
            throw new PocketException(PocketErrorCode.InvalidLimitCode);
        }


        TsService tsService = tsServiceRepository.
                findFirstByServiceCode(request.getServiceCode());

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service code invalid");
            throw new PocketException(PocketErrorCode.InvalidServiceCode);
        }

        TsTransactionProfileLimitMapping tsTransactionProfileLimitMapping =
                tsTransactionProfileLimitMappingRepository.findFirstByTxProfileCodeAndServiceCodeAndLimitCode(request.getTxProfileCode(), request.getServiceCode(), request.getLimitCode());

        if (tsTransactionProfileLimitMapping != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile,limit and service map already exists");
            throw new PocketException(PocketErrorCode.TxProfileServiceAndLimitMapExists);
        }

        tsTransactionProfileLimitMapping = new TsTransactionProfileLimitMapping();
        tsTransactionProfileLimitMapping.setLimitCode(request.getLimitCode());
        tsTransactionProfileLimitMapping.setServiceCode(request.getServiceCode());
        tsTransactionProfileLimitMapping.setTxProfileCode(request.getTxProfileCode());
        tsTransactionProfileLimitMapping.setCreatedDate(new Date());

        tsTransactionProfileLimitMappingRepository.save(tsTransactionProfileLimitMapping);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getServiceLimitByTxProfile(GetServiceLimitByTxProfileRequest request) throws PocketException {

        // check tx profile code

        TsTransactionProfile transactionProfile = tsTransactionProfileRepository.findFirstByProfileCode(request.getTxProfileCode());
        if (transactionProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid Tx profile code :" + request.getTxProfileCode());
            throw new PocketException(PocketErrorCode.InvalidTxProfileCode);
        }

        List<TsTransactionProfileLimitMapping> tsTransactionProfileLimitMapping = tsTransactionProfileLimitMappingRepository
                .findAllByTxProfileCode(request.getTxProfileCode());

        List<TransactionProfileServiceLimitMapItem> mapItems = new ArrayList<>();

        if (tsTransactionProfileLimitMapping != null && tsTransactionProfileLimitMapping.size() > 0) {

            for (TsTransactionProfileLimitMapping map : tsTransactionProfileLimitMapping) {
                TransactionProfileServiceLimitMapItem temp = new TransactionProfileServiceLimitMapItem();
                temp.setLimitCode(map.getLimitCode());

                TsLimit tsLimit = tsLimitRepository.findFirstByLimitCode(map.getLimitCode());
                if (tsLimit != null) {
                    temp.setLimitName(tsLimit.getLimitName());
                }

                TsService tsService = tsServiceRepository.findFirstByServiceCode(map.getServiceCode());
                temp.setServiceCode(map.getServiceCode());
                if (tsService != null) {
                    temp.setServiceName(tsService.getServiceName());
                }
                mapItems.add(temp);
            }
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(mapItems);

        return baseResponseObject;
    }
}
