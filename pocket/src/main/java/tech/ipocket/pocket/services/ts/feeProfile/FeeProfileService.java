package tech.ipocket.pocket.services.ts.feeProfile;

import tech.ipocket.pocket.request.ts.feeProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface FeeProfileService {
    BaseResponseObject createFeeProfile(CreateFeeProfileRequest request) throws PocketException;

    BaseResponseObject getFeeProfile(GetFeeProfileRequest request);

    BaseResponseObject createFee(CreateFeeRequest request) throws PocketException;

    BaseResponseObject getFee(GetFeeRequest request) throws PocketException;

    BaseResponseObject createStakeHolder(CreateStakeHolder request) throws PocketException;

    BaseResponseObject getStakeHolder(GetStakeHolderRequest request);

//    BaseResponseObject createStakeHolderFeeProductMapping(CreateStakeHolderFeeMapRequest request) throws PocketException;

    BaseResponseObject createFeeFeeProfileServiceMap(CreateFeeProfileLimitMapRequest request) throws PocketException;
}
