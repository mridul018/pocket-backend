package tech.ipocket.pocket.services.security;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface RequestValidator {
    TsSessionObject validateSession(BaseRequestObject baseRequestObject) throws PocketException;
    TsSessionObject validateSessionWithCredential(SessionCheckWithCredentialRequest baseRequestObject) throws PocketException;
}
