package tech.ipocket.pocket.services.ts.transactionValidity;

import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsMerchantDetails;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsMerchantDetailsRepository;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.response.ts.transactionValidity.TransactionValidityResponse;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTransactionValidity {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(AbstractTransactionValidity.class);

    private TsClientRepository clientRepository;
    private TsServiceRepository serviceRepository;
    private FeeManager feeManager;
    private TransactionValidator validator;
    private TsMerchantDetailsRepository merchantDetailsRepository;

    private TsClient senderCfeClient, receiverCfeClient;
    private TsService cfeService;

    public void setDependency(TsClientRepository clientRepository,
                              TsServiceRepository serviceRepository,
                              FeeManager feeManager, TransactionValidator validator,
                              TsMerchantDetailsRepository merchantDetailsRepository) {
        this.clientRepository = clientRepository;
        this.serviceRepository = serviceRepository;
        this.feeManager = feeManager;
        this.validator = validator;
        this.merchantDetailsRepository = merchantDetailsRepository;
    }

    public abstract TransactionValidityResponse getTransactionValidity(TransactionValidityRequest transactionValidityRequest) throws PocketException;

    Boolean validateRequestAccountsForFundTransfer(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), clientStatus);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }

    Boolean validateRequestAccountsForCashIn(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        if(!senderCfeClient.getGroupCode().equals(TsEnums.UserType.AGENT)){
            logWriterUtility.error(request.getRequestId(),"Sender must be agent");
            throw new PocketException(PocketErrorCode.SenderMustBeAgent);
        }

        receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), clientStatus);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }


        if(!receiverCfeClient.getGroupCode().equals(TsEnums.UserType.CUSTOMER)){
            logWriterUtility.error(request.getRequestId(),"Receiver must be customer");
            throw new PocketException(PocketErrorCode.ReceiverMustBeCustomer);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }

    Boolean validateRequestAccountsForTopUp(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletIsRequired);
        }

        TsMerchantDetails merchantDetails = merchantDetailsRepository.findFirstByShortCode("SSL");
        if (merchantDetails == null) {
            logWriterUtility.error(request.getRequestId(), "TopUp Merchant client not found");
            throw new PocketException(PocketErrorCode.MerchantNotFound);
        }

        receiverCfeClient = merchantDetails.getTsClientByClientId();
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }

    TransactionValidityResponse getTransactionValidityResponse(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        BigDecimal transactionAmount = new BigDecimal(transactionValidityRequest.getTransferAmount());

        FeeData feeData = feeManager.calculateFee(senderCfeClient, cfeService.getServiceCode(), transactionAmount,
                transactionValidityRequest.getRequestId());

        if (feeData == null) {
            logWriterUtility.error(transactionValidityRequest.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
            transactionAmount = transactionAmount.add(feeData.getFeeAmount());
        }

        validator.isSenderProfileValidForTransaction(senderCfeClient, cfeService, transactionAmount, transactionValidityRequest.getRequestId());
        validator.isReceiverProfileValidForTransaction(receiverCfeClient, cfeService, transactionValidityRequest.getRequestId());

        TransactionValidityResponse validityResponse = new TransactionValidityResponse();
        validityResponse.setFee(feeData.getFeeAmount());
        validityResponse.setPayer(getFeePayer(feeData.getFeePayer(), transactionValidityRequest.getType(), transactionValidityRequest.getRequestId()));
        return validityResponse;
    }

    public String getFeePayer(String genericPayer, String transactionType, String requestId) {
        logWriterUtility.trace(requestId, "Payer :" + genericPayer + " transactionType :" + transactionType);
        switch (transactionType) {
            case ServiceCodeConstants.Fund_Transfer:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.Merchant_Payment:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.Top_Up:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.CashIn:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            default:
                return null;
        }

    }

    public TsClient getSenderCfeClient() {
        return senderCfeClient;
    }

    public TsClient getReceiverCfeClient() {
        return receiverCfeClient;
    }
}