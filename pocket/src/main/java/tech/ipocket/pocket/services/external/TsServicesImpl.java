package tech.ipocket.pocket.services.external;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.TransactionValidityUmResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.RestCallerTask;

@Service
public class TsServicesImpl implements TsServices {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private Environment environment;

    @Override
    public BaseResponseObject getTransactionValidity(TransactionValidityUmRequest request) throws PocketException {

        String url = environment.getProperty("ts.baseUrl") + "v1/mobile/getTransactionValidity";

        BaseResponseObject baseResponseObject= (BaseResponseObject) new RestCallerTask()
                .callToRestService(HttpMethod.POST,url,request,BaseResponseObject.class,request.getRequestId());
        return baseResponseObject;
    }

    @Override
    public boolean callTxForValidatingWalletBalance(BalanceCheckRequest balanceInqueryRequest) {
        // TODO: 2019-05-06 code here for balance check
        return false;
    }

    @Override
    public TransactionValidityUmResponse callTxForTransactionVisibility(TransactionValidityUmRequest request) throws PocketException {
        String url=environment.getProperty("ts.baseUrl")+"v1/mobile/getTransactionValidity";

        TransactionValidityUmResponse response= (TransactionValidityUmResponse) new RestCallerTask()
                .callToRestService(HttpMethod.POST,url,request,TransactionValidityUmResponse.class,request.getRequestId());
        return response;
    }
}
