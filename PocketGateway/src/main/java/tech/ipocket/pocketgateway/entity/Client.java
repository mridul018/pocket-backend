package tech.ipocket.pocketgateway.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "client", schema = "PocketAuth")
public class Client {
    private Integer id;
    private String clientId;
    private String clientName;
    private String clientPassword;
    private Boolean clientValid;
    private Timestamp createAt;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "client_id", nullable = false, length = 10)
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "client_name", nullable = false, length = 100)
    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Basic
    @Column(name = "client_password", nullable = false, length = -1)
    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    @Basic
    @Column(name = "client_valid", nullable = false)
    public Boolean getClientValid() {
        return clientValid;
    }

    public void setClientValid(Boolean clientValid) {
        this.clientValid = clientValid;
    }

    @Basic
    @Column(name = "createAt", nullable = true)
    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                Objects.equals(clientId, client.clientId) &&
                Objects.equals(clientName, client.clientName) &&
                Objects.equals(clientPassword, client.clientPassword) &&
                Objects.equals(clientValid, client.clientValid) &&
                Objects.equals(createAt, client.createAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, clientName, clientPassword, clientValid, createAt);
    }
}
