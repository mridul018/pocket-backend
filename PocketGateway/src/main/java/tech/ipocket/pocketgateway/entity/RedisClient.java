package tech.ipocket.pocketgateway.entity;

import java.io.Serializable;

public class RedisClient implements Serializable {
    private String clientId;
    private String clientName;
    private String clientPassword;

    public RedisClient() {
    }

    public RedisClient(String clientId, String clientName, String clientPassword) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.clientPassword = clientPassword;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }
}
