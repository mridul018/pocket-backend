package tech.ipocket.pocketgateway.entity;

import java.io.Serializable;

public class RedisClientRouteMapping implements Serializable {
    private int id;
    private String clientId;
    private String routeId;

    public RedisClientRouteMapping() {
    }

    public RedisClientRouteMapping(int id, String clientId, String routeId) {
        this.id = id;
        this.clientId = clientId;
        this.routeId = routeId;
    }

    public RedisClientRouteMapping(String clientId, String routeId) {
        this.clientId = clientId;
        this.routeId = routeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }
}
