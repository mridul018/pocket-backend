package tech.ipocket.pocketgateway.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "route", schema = "PocketAuth")
public class Route {
    private Integer id;
    private String routeId;
    private String routePath;
    private String routeServiceId;
    private String routeUrl;
    private String routeContextPath;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "route_id", nullable = false, length = 4)
    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "route_path", nullable = false, length = 45)
    public String getRoutePath() {
        return routePath;
    }

    public void setRoutePath(String routePath) {
        this.routePath = routePath;
    }

    @Basic
    @Column(name = "route_serviceId", nullable = true, length = 45)
    public String getRouteServiceId() {
        return routeServiceId;
    }

    public void setRouteServiceId(String routeServiceId) {
        this.routeServiceId = routeServiceId;
    }

    @Basic
    @Column(name = "route_url", nullable = false, length = 15)
    public String getRouteUrl() {
        return routeUrl;
    }

    public void setRouteUrl(String routeUrl) {
        this.routeUrl = routeUrl;
    }

    @Basic
    @Column(name = "route_context_path", nullable = true, length = 45)
    public String getRouteContextPath() {
        return routeContextPath;
    }

    public void setRouteContextPath(String routeContextPath) {
        this.routeContextPath = routeContextPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(id, route.id) &&
                Objects.equals(routeId, route.routeId) &&
                Objects.equals(routePath, route.routePath) &&
                Objects.equals(routeServiceId, route.routeServiceId) &&
                Objects.equals(routeUrl, route.routeUrl) &&
                Objects.equals(routeContextPath, route.routeContextPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, routeId, routePath, routeServiceId, routeUrl, routeContextPath);
    }
}
