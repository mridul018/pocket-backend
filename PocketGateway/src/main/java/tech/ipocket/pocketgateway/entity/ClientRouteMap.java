package tech.ipocket.pocketgateway.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "client_route_map", schema = "PocketAuth")
public class ClientRouteMap {
    private Integer id;
    private String clientId;
    private String routeId;
    private Boolean status;
    private Boolean authRequire;
    private Timestamp createAt;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "client_id", nullable = false, length = 10)
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "route_id", nullable = false, length = 4)
    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "auth_require", nullable = true)
    public Boolean getAuthRequire() {
        return authRequire;
    }

    public void setAuthRequire(Boolean authRequire) {
        this.authRequire = authRequire;
    }

    @Basic
    @Column(name = "createAt", nullable = true)
    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientRouteMap map = (ClientRouteMap) o;
        return Objects.equals(id, map.id) &&
                Objects.equals(clientId, map.clientId) &&
                Objects.equals(routeId, map.routeId) &&
                Objects.equals(status, map.status) &&
                Objects.equals(authRequire, map.authRequire) &&
                Objects.equals(createAt, map.createAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, routeId, status, authRequire, createAt);
    }
}
