package tech.ipocket.pocketgateway.request;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthTokenRequest {
    @NotNull(message = "client name cannot be null or empty")
    @Size(max = 30, message = "client name too large, max length is 30")
    private String clientName;

    @NotNull(message = "client password cannot be null or empty")
    @Size(max = 200, message = "client password too large, max length 200")
    private String clientPassword;
}
