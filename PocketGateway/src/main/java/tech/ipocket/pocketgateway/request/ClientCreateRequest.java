package tech.ipocket.pocketgateway.request;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientCreateRequest {
    @NotNull(message = "client id cannot be null")
    @Size(max = 4, message = "client id too long, max length 4")
    private String clientId;

    @NotNull(message = "client name cannot be null")
    @Size(max = 100, message = "client name too large, max length 100")
    private String clientName;

    @NotNull(message = "client password cannot be null")
    @Size(max = 100, message = "Password too long, max length 100")
    private String clientPassword;

    @NotNull(message = "client valid or not, it must be set (1 or 0)")
    private Boolean clientValid;


}
