package tech.ipocket.pocketgateway.request;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientRouteMappingRequest {
    private int id;

    @NotNull(message = "client id not null or empty")
    @Size(max = 4, message = "client id too large, max length 4")
    private String clientId;

    @NotNull(message = "route id not null or empty")
    @Size(max = 4, message = "route id too large, max length 4")
    private String routeId;

    @NotNull(message = "mapping status cannot be null or empty")
    private boolean status;

    @Nullable
    private boolean authRequire;

}
