package tech.ipocket.pocketgateway.request;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RouteAddRequest {

    private Integer id;

    @NotNull(message = "route id cannot be null or empty")
    @Size(max = 4, message = "route id too large, max length 4")
    private String routeId;

    @NotNull(message = "path cannot be null or empty")
    private String routePath;

    @Nullable
    private String routeServiceId;

    @NotNull(message = "url cannot be null or empty")
    private String routeUrl;

    @Nullable
    private String routeContextPath;
}
