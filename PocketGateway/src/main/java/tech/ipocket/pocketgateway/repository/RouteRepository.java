package tech.ipocket.pocketgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketgateway.entity.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Integer> {

}
