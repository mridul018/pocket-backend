package tech.ipocket.pocketgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketgateway.entity.ClientRouteMap;

@Repository
public interface ClientRouteMappingRepository extends JpaRepository<ClientRouteMap, Integer> {
}
