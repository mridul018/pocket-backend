package tech.ipocket.pocketgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocketgateway.entity.RedisRoute;
import tech.ipocket.pocketgateway.request.AuthTokenRequest;
import tech.ipocket.pocketgateway.request.ClientCreateRequest;
import tech.ipocket.pocketgateway.request.ClientRouteMappingRequest;
import tech.ipocket.pocketgateway.request.RouteAddRequest;
import tech.ipocket.pocketgateway.response.BaseResponseObject;
import tech.ipocket.pocketgateway.services.RedisRouteService;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Map;


@RestController
public class HomeController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RedisRouteService redisRouteService;


    @PostMapping(value = "/token")
    public ResponseEntity<String> getToken(@Valid @RequestBody AuthTokenRequest request){
        return new ResponseEntity<>(restTemplate.postForObject("http://pocket-auth/auth/login",
                request, String.class), HttpStatus.OK);
    }

    @PostMapping(value = "/createRoute")
    public ResponseEntity<BaseResponseObject> createRoute(@Valid @RequestBody RouteAddRequest request){

        BaseResponseObject responseObject = restTemplate.postForObject("http://pocket-auth/auth/addRoute",
                request, BaseResponseObject.class);
        if(responseObject.getError()==null){
            redisRouteService.setZuulRoute();
        }
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/createClient")
    public ResponseEntity<BaseResponseObject> createClient(@Valid @RequestBody ClientCreateRequest request){
        return new ResponseEntity<>(restTemplate.postForObject("http://pocket-auth/auth/createClient",
                request, BaseResponseObject.class), HttpStatus.OK);
    }

    @PostMapping(value = "/createMapping")
    public ResponseEntity<BaseResponseObject> createMapping(@Valid @RequestBody ClientRouteMappingRequest request){
        return new ResponseEntity<>(restTemplate.postForObject("http://pocket-auth/auth/addNewMapping",
                request, BaseResponseObject.class), HttpStatus.OK);

    }
}
