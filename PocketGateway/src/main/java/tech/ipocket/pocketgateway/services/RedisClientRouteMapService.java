package tech.ipocket.pocketgateway.services;

import tech.ipocket.pocketgateway.entity.RedisClientRouteMapping;

import java.util.List;
import java.util.Map;

public interface RedisClientRouteMapService {

    Map<Object, Object> findAll();
}
