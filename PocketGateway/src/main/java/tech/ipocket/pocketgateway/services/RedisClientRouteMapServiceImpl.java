package tech.ipocket.pocketgateway.services;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketgateway.entity.RedisClientRouteMapping;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class RedisClientRouteMapServiceImpl implements RedisClientRouteMapService {

    private static final String KEY="MAPPING";
    private RedisTemplate<String, RedisClientRouteMapping> redisTemplate;
    private HashOperations hashOperations;

    public RedisClientRouteMapServiceImpl(RedisTemplate<String, RedisClientRouteMapping> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @Override
    public Map<Object, Object> findAll() {
        try {
            return this.redisTemplate.boundHashOps(KEY).entries();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
