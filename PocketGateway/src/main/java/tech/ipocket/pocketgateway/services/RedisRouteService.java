package tech.ipocket.pocketgateway.services;

import tech.ipocket.pocketgateway.entity.RedisRoute;

import java.util.Map;

public interface RedisRouteService {

    Map<Object, Object> findAll();
    RedisRoute findByRouteID(String routeId);
    void setZuulRoute();
}
