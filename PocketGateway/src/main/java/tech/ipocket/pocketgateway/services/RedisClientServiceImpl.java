package tech.ipocket.pocketgateway.services;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketgateway.entity.RedisClient;

import java.util.Map;

@Repository
public class RedisClientServiceImpl implements RedisClientService {

    private static final String KEY="CLIENT";
    private RedisTemplate<String, RedisClient> redisTemplate;
    private HashOperations hashOperations;

    public RedisClientServiceImpl(RedisTemplate<String, RedisClient> redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @Override
    public Map<Object, Object> finaAll() {
        try {
            return this.redisTemplate.boundHashOps(KEY).entries();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public RedisClient findByClientName(String clientName) {
        try {
            return (RedisClient) this.redisTemplate.boundHashOps(KEY).get(clientName);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
