package tech.ipocket.pocketgateway.services;

import tech.ipocket.pocketgateway.entity.RedisClient;

import java.util.Map;

public interface RedisClientService {
    Map<Object, Object> finaAll();
    RedisClient findByClientName(String clientName);

}
