package tech.ipocket.pocketgateway.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocketgateway.entity.RedisRoute;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

@Repository
public class RedisRouteServiceImpl implements RedisRouteService {

    private static final String KEY="ROUTE";

    private ZuulProperties zuulProperties;
    private ZuulHandlerMapping zuulHandlerMapping;
    private RedisTemplate<String, RedisRoute> redisTemplate;
    private HashOperations hashOperations;

    public RedisRouteServiceImpl(ZuulProperties zuulProperties, ZuulHandlerMapping zuulHandlerMapping, RedisTemplate<String, RedisRoute> redisTemplate) {
        this.zuulProperties = zuulProperties;
        this.zuulHandlerMapping = zuulHandlerMapping;
        this.redisTemplate = redisTemplate;
        this.hashOperations = this.redisTemplate.opsForHash();
    }

    @PostConstruct
    public void initialization(){
        setZuulRoute();
    }

    @Override
    public Map<Object, Object> findAll() {
        try {
            return this.redisTemplate.boundHashOps(KEY).entries();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public RedisRoute findByRouteID(String routeId) {
        try {
            return (RedisRoute) this.redisTemplate.boundHashOps(KEY).get(routeId);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        return  null;
    }

    @Override
    public void setZuulRoute() {
        try {
            Map<Object, Object> dynamicRouteMap = findAll();
            if(dynamicRouteMap != null && dynamicRouteMap.size()>0){
                dynamicRouteMap.forEach((key, redisRoute) -> {
                    if(zuulProperties.getRoutes() != null && zuulProperties.getRoutes().size()>0){
                        zuulProperties.getRoutes().remove(key);
                    }
                    RedisRoute route = null;
                    try {
                        route = new ObjectMapper().readValue(dynamicRouteMap.get(key).toString(), RedisRoute.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    zuulProperties.getRoutes().put(route.getRequestId(),new ZuulProperties.ZuulRoute(
                            route.getRequestId(),route.getPath(),null,
                            route.getUrl()+"/"+route.getContextPath()+"/",true,false,
                            new HashSet<>()
                    ));
                    zuulHandlerMapping.setDirty(true);
                });
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

}
