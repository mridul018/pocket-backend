package tech.ipocket.pocketgateway.response;


public class ErrorObject {
    private String errorMessage;
    private int errorCode;

    public ErrorObject(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorObject(int errorCode, String errorMessage) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
