package tech.ipocket.pocketgateway.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import tech.ipocket.pocketgateway.config.JwtConfig;
import tech.ipocket.pocketgateway.response.BaseResponseObject;
import tech.ipocket.pocketgateway.response.ErrorObject;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthorizationToken extends OncePerRequestFilter {

    private JwtConfig jwtConfig;

    public JwtAuthorizationToken(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        String header = httpServletRequest.getHeader(jwtConfig.getHeader());
        if(header==null||!header.startsWith(jwtConfig.getPrefix())){
            filterChain.doFilter(httpServletRequest,httpServletResponse);
            return;
        }

        String token = header.replace(jwtConfig.getPrefix(),"").trim();

        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(getSecretKey())
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.get("username",String.class);
            String password = claims.get("password",String.class);

            UsernamePasswordAuthenticationToken auth =
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password,
                            Collections.emptyList());

            SecurityContextHolder.getContext().setAuthentication(auth);

            filterChain.doFilter(httpServletRequest, httpServletResponse);

        }catch (Exception ex){
            SecurityContextHolder.clearContext();

            BaseResponseObject responseObject = new BaseResponseObject();

            responseObject.setData(null);

            if(ex.getMessage().contains("time")){
                responseObject.setStatus(String.valueOf(HttpServletResponse.SC_REQUEST_TIMEOUT));
                responseObject.setError(new ErrorObject(
                        HttpServletResponse.SC_REQUEST_TIMEOUT,ex.getMessage()));
            }else if(ex.getMessage().contains("locally")){
                responseObject.setStatus(String.valueOf(HttpServletResponse.SC_UNAUTHORIZED));
                responseObject.setError(new ErrorObject(
                        HttpServletResponse.SC_UNAUTHORIZED,ex.getMessage()));
            }
            responseObject.setRequestId(null);

            httpServletResponse.setContentType("application/json");
            httpServletResponse.getWriter().write(new ObjectMapper().writeValueAsString(responseObject));

        }

    }

    private String getSecretKey() {
        /*byte[] encoded = MacProvider.generateKey(SignatureAlgorithm.HS512).getEncoded();
        System.out.println(Base64.getEncoder().encodeToString(encoded));
        return Base64.getEncoder().encodeToString(encoded);*/

        return jwtConfig.getKey();
    }
}
