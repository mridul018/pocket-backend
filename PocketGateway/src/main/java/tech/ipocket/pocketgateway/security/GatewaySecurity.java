package tech.ipocket.pocketgateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.SecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tech.ipocket.pocketgateway.config.JwtConfig;

@EnableWebSecurity
public class GatewaySecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .httpBasic().disable()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .addFilterAfter(new JwtAuthorizationToken(jwtConfig),UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                    .antMatchers("/token","/createRoute","/createClient","/createMapping",
                            "/mapping","/**/v2/api-docs","/configuration/**","/swagger*/**","/webjars/**").permitAll()
                .anyRequest().authenticated();
    }
}
