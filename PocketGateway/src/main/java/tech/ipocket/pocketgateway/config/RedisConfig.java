package tech.ipocket.pocketgateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import tech.ipocket.pocketgateway.entity.RedisClient;
import tech.ipocket.pocketgateway.entity.RedisClientRouteMapping;
import tech.ipocket.pocketgateway.entity.RedisRoute;

@Configuration
public class RedisConfig {

    @Bean
    public JedisConnectionFactory jedisConnectionFactory(){
        return new JedisConnectionFactory();
    }


    @Bean
    public RedisTemplate<String, RedisRoute> redisRouteTemplate(){
        RedisTemplate<String, RedisRoute> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());

        template.setDefaultSerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));

        template.setEnableTransactionSupport(false);

        return template;
    }

    @Bean
    public RedisTemplate<String, RedisClient> redisClientTemplate(){
        RedisTemplate<String, RedisClient> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());

        template.setDefaultSerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));

        template.setEnableTransactionSupport(false);

        return template;
    }

    @Bean
    public RedisTemplate<String, RedisClientRouteMapping> redisClientRouteMappingTemplate(){
        RedisTemplate<String, RedisClientRouteMapping> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());


        template.setDefaultSerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));

        template.setEnableTransactionSupport(false);

        return template;
    }


}
