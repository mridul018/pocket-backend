package tech.ipocket.pocketgateway.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.http.HttpStatus;
import tech.ipocket.pocketgateway.entity.RedisClient;
import tech.ipocket.pocketgateway.entity.RedisClientRouteMapping;
import tech.ipocket.pocketgateway.entity.RedisRoute;
import tech.ipocket.pocketgateway.response.BaseResponseObject;
import tech.ipocket.pocketgateway.response.ErrorObject;
import tech.ipocket.pocketgateway.services.RedisClientRouteMapService;
import tech.ipocket.pocketgateway.services.RedisClientService;
import tech.ipocket.pocketgateway.services.RedisRouteService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ZuulRouteFilter extends ZuulFilter {

    @Autowired
    private RedisClientService redisClientService;
    @Autowired
    private RedisClientRouteMapService redisClientRouteMapService;
    @Autowired
    private RedisRouteService redisRouteService;
    @Autowired
    private JwtConfig jwtConfig;

    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();

        if(request.getRequestURI().contains("api-docs")){
            System.out.println("Found api-docs, so it must be ignored");
            return false;
        }
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();

        String url = request.getRequestURI();
        System.out.println("Route url "+ url);
        String token = request.getHeader(jwtConfig.getHeader());

        // find user from token
        if(token==null||!token.startsWith(jwtConfig.getPrefix())){
            return new ZuulException("Token not found",401,"User Toke not found");
        }

        token = token.replace(jwtConfig.getPrefix(),"").trim();

        try{
            Claims claims = Jwts.parser()
                    .setSigningKey(jwtConfig.getKey())
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.get("username",String.class);

            System.out.println("API consume user : "+ username);
            String[] urlSplit = url.split("[/]");
            System.out.println("Route path : "+ urlSplit[2]);
            if(!isValidURL(urlSplit[2],username)){

                System.out.println("API consumer is not matched");

                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setStatus("400");
                responseObject.setError(new ErrorObject(HttpStatus.FORBIDDEN.value(),"Invalid api consumer"));

                requestContext.setSendZuulResponse(false);
                requestContext.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
                requestContext.setResponseBody(new ObjectMapper().writeValueAsString(responseObject));
            }

        }catch (Exception ex){
            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setStatus("400");
            responseObject.setError(new ErrorObject(HttpStatus.FORBIDDEN.value(),"Token not valid"));

            requestContext.setSendZuulResponse(false);
            requestContext.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
            try {
                requestContext.setResponseBody(new ObjectMapper().writeValueAsString(responseObject));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private boolean isValidURL(String urlSplit, String username) {
        int count=0;
        Collection<String> routeIds = new ArrayList<>();
        if(urlSplit.toLowerCase().equals(username.toLowerCase())){
            System.out.println("same header or url");
            return Boolean.TRUE;
        } else {

            RedisClient client = redisClientService.findByClientName(username);
            if(client==null){
                return Boolean.FALSE;
            }

            System.out.println("client found and its id: "+ client.getClientId());

            Map<Object, Object> allMapping = redisClientRouteMapService.findAll();

            allMapping.forEach((key, redisClientRouteMapping) -> {

                RedisClientRouteMapping mapping = null;
                try {
                    mapping = new ObjectMapper().readValue(allMapping.get(key).toString(), RedisClientRouteMapping.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(client.getClientId().equals(mapping.getClientId())){
                    System.out.println("mapping route id: "+mapping.getRouteId());
                    routeIds.add(mapping.getRouteId());
                }
            });

            for(String id : routeIds){
                RedisRoute route = redisRouteService.findByRouteID(id);
                if(route.getPath().equals("/"+urlSplit+"/**")){
                    count++;
                }
            }
            return count <= 0 ? Boolean.FALSE : Boolean.TRUE;
        }
    }
}
