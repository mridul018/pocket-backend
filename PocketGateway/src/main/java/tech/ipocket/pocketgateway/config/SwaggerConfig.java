package tech.ipocket.pocketgateway.config;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    private ZuulProperties zuulProperties;

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(ApiInfo.DEFAULT)
                .forCodeGeneration(true)
                //.securityContexts(Lists.newArrayList(securityContext()))
                //.securitySchemes(Lists.newArrayList(apiKey()))
                .useDefaultResponseMessages(false)

                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference("JWT", authorizationScopes));
    }

    @Primary
    @Bean
    public SwaggerResourcesProvider swaggerResourcesProvider(){
        return () -> {
            List<SwaggerResource> resources = new ArrayList<>();

            if(resources.size()>0){
                resources.clear();
            }

            resources.add(swaggerResource("default","/v2/api-docs","2.0"));
            zuulProperties.getRoutes().forEach((s, zuulRoute) -> {
                resources.add(swaggerResource(
                        zuulRoute.getPath().replace("/**",""),
                        zuulRoute.getPath().replace("**","v2/api-docs"),
                        "2.0"));
            });



            // add default swagger
            //resources.add(swaggerResource("pocket","/v2/api-docs","2.0"));
            //resources.add(swaggerResource("pocket-external","/ext/v2/api-docs","2.0"));
            //resources.add(swaggerResource("pocket-core-mobile","/mobile/v2/api-docs","2.0"));
            //resources.add(swaggerResource("pocket-core-web","/web/v2/api-docs","2.0"));




            return resources;
        };
    }

    // http://localhost:9001/pocket/mobile/core/v2/api-docs
    // http://localhost:9001/pocket/ext/external/v2/api-docs

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
